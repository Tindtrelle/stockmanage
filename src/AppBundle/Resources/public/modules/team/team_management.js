/**
 * TEAM MANAGEMENT
 */

function openCreateMemberModal() {
    // Reset values
    $("#create-folder-modal .modal-title").html("Create Folder");
    $(".input-m-id").val("");
    $(".input-m-name").val("");
    $(".input-m-role").val("");
    $(".input-m-custom-id").val("");
    $(".input-m-email").val("");

    $("#update-member-modal").modal('show');
}

function openEditMemberModal(id) {
    var parent_row = "tr[data-member-id='"+id+"']";
    var m_name = $(parent_row+" .member-name").text();
    var m_custom_id = $(parent_row+" .member-custom-id").text();
    var m_role = $(parent_row+" .member-role").text();
    var email = $(parent_row+" .member-email").text();
    // Set values
    $("#update-member-modal .modal-title").html("Update Member");
    $(".input-m-id").val(id);
    $(".input-m-name").val(m_name);
    $(".input-m-role").val(m_role);
    $(".input-m-custom-id").val(m_custom_id);
    $(".input-m-email").val(email);

    $("#update-member-modal").modal('show');
}

function submitUpdateMember(event,form) {
    event.preventDefault();
    var url = "/team/ajax";
    var data = getFormResults(form);
    data.action = "member-create";
    startLoading();

    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "json",
        method: "POST"
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

var selected_all_member = false;
function selectAllMembers() {
    if(selected_all_member) {
        $("input.member-select").prop('checked', false);
        selected_all_member = false;
    } else {
        $("input.member-select").prop('checked', true);
        selected_all_member = true;
    }
}

/**
 * Delete selected members
 *
 */
function deleteMembers() {
    var r = confirm("Are you sure that you want to delete?");
    if (r == false) {
        return false;
    }
    var members_to_delete = [];
    $("input.member-select").each(function(index) {
        if($(this).is(':checked')) {
            members_to_delete.push($(this).attr("data-member-id"));
        }
    });
    if(members_to_delete.length == 0) {
        return;
    }

    var url = "/team/ajax";
    var form_data = new FormData();
    form_data.append("members",members_to_delete);
    form_data.append("action","members-delete");
    startLoading();
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}