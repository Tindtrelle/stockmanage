var order_by = "";
var selected_all_files = false;
function selectAllFolders() {
    if(selected_all_files) {
        $("input.file-select").prop('checked', false);
        selected_all_files = false;
    } else {
        $("input.file-select").prop('checked', true);
        selected_all_files = true;
    }
    // update background
    $("input.file-select").each(function(index) {
        var elem_file_row = $('tr[data-file-id='+$(this).attr("data-file-id")+']');
        if($(this).is(':checked')) {
            elem_file_row.addClass("selected");
        } else {
            elem_file_row.removeClass("selected");
        }
    });
    showInfoPopup();
}

function deselectAll() {
    $("input.file-select").prop('checked', false);
    $("input.file-select").each(function(index) {
        var elem_file_row = $('tr[data-file-id='+$(this).attr("data-file-id")+']');
        elem_file_row.removeClass("selected");
    });
    showInfoPopup();
}

/**
 * Show top right info popup
 */
function showInfoPopup() {
    var info_popup = $(".page-notifications");
    var total_files = 0;
    var selected_files = 0;
    $("input.file-select").each(function(index) {
        total_files++;
        if($(this).is(':checked')) {
            selected_files++;
        }
    });

    if(selected_files) {
        info_popup.removeClass("hidden");
        $("p.notification-content").html(selected_files+"/"+total_files+" images selected<br><a class='deselect-link' onclick='deselectAll()'>Deselect all</a>");
    } else {
        info_popup.addClass("hidden");
    }
}

function downloadFiles(view) {
    var files_to_download = [];
    if(view == 'list') {
        $("input.file-select").each(function(index) {
            if($(this).is(':checked')) {
                files_to_download.push($(this).attr("data-file-id"));
            }
        });
    } else if(view == 'grid') {
        $(".img-container").each(function (index) {
            if ($(this).hasClass("selected")) {
                files_to_download.push($(this).attr("data-file-id"));
            }
        });
    } else {
        return;
    }

    if(files_to_download.length == 0) {
        return;
    }
    window.open(window.location.origin+'/file/download?id='+files_to_download, '_blank');
}

/**
 * Delete files
 */
function deleteFiles(view) {
    var r = confirm("Are you sure that you want to delete?");
    if (r == false) {
        return false;
    }
    var files_to_delete = [];
    if(view == 'list') {
        $("input.file-select").each(function(index) {
            if($(this).is(':checked')) {
                files_to_delete.push($(this).attr("data-file-id"));
            }
        });
    } else if(view == 'grid') {
        $(".img-container").each(function (index) {
            if ($(this).hasClass("selected")) {
                files_to_delete.push($(this).attr("data-file-id"));
            }
        });
    } else {
        return;
    }

    if(files_to_delete.length == 0) {
        return;
    }

    var url = "/folders/ajax";
    var form_data = new FormData();
    form_data.append("files",files_to_delete);
    form_data.append("action","files-delete");
    startLoading();
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

/**
 * Search files
 */
var fileTypingTimer;
var $input = $('#input-file-search');
$input.on('keyup', function () {
    clearTimeout(fileTypingTimer);
    fileTypingTimer = setTimeout(
        searchFilesRequest,700);
});
$input.on('keydown', function () {
    clearTimeout(fileTypingTimer);
});

function searchFilesRequest() {
    var url = window.location.pathname;
    $("#current-page").val(1);
    var queryData = getQueryData();
    var data = queryData['data'];
    var query = queryData['query'];

    window.history.pushState("object or string", "Title", url+query);
    updateFilterIcon();
    $.ajax({
        url: window.location.origin+url+query,
        data: data,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        if(window.location.pathname == "/metadata/grid") { // grid view
            $('.files-grid').html(response);
            initDragable();
        } else {
            $('.files-list').html(response); // list view
        }
        countAllFilesKeywords();
    });
}

/**
 * Change order
 * @param elem
 * @param orderBy
 */
function updateOrderBy(elem,orderBy) {
    $("li.order-option").removeClass("active");
    $(elem).addClass("active");
    if(orderBy == "custom") {
        order_by = "";
    } else {
        order_by = orderBy;
    }
    searchFilesRequest();
}

function getQueryData() {
    var response = {};

    var data = [];
    var query = "?";

    var page = $("#current-page").val();
    if(page != "") {
        data.push("page="+page);
    }

    var search_name = $("input.search").val();
    if(search_name!= "") {
        data.push("name="+search_name);
    }
    // PHOTOGRAPHERS CHECKBOX
    var photographersArr = [];
    var photographersLength = $("input.filter-photographer").length;
    $("input.filter-photographer").each(function() {
        if($(this).is(":checked")) {
            photographersArr.push($(this).attr("data-id"));
        }
    });
    var photographers = photographersArr.join(",");
    if(photographers != "" && photographersLength != photographersArr.length  ) {
        data.push('p='+photographers);
    }
    // RETOUCHERS CHECKBOX
    var retouchersArr = [];
    var retouchersLength = $("input.filter-retoucher").length;
    $("input.filter-retoucher").each(function() {
        if($(this).is(":checked")) {
            retouchersArr.push($(this).attr("data-id"));
        }
    });
    var retouchers = retouchersArr.join(",");
    if(retouchers != "" && retouchersLength != retouchersArr.length ) {
        data.push('r='+retouchers);
    }
    // Collection
    var collectionArr = [];
    $("input.filter-collection").each(function() {
        if($(this).is(":checked")) {
            collectionArr.push($(this).attr("data-name"));
        }
    });
    var collection = collectionArr.join(",");
    if(collection != "") {
        data.push('collection='+collection);
    }
    // Date from - to
    var dateFrom = $("input#filter-date-from").val();
    if(dateFrom != "01/01/2011")  {
        data.push('date_from='+dateFrom);
    }
    var dateTo = $("input#filter-date-to").val();
    var now = new Date();
    var nowString = formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear();

    if(dateTo != nowString) {
        data.push('date_to=' + dateTo);
    }

    if(order_by != "") {
        data.push('order_by=' + order_by);
    }

    if($("#select-search-by").val() == "meta") {
        data.push('search_by=meta');
    } else {
        data.push('search_by=name');
    }
    var selectedFolder = $('#autocomplete-value').val();
    //var selectedFolder = $('#select-folder-search').find(":selected").val();
    if(selectedFolder){
        data.push('folder_id='+selectedFolder);
    }

    query += data.join('&');

    response['data'] = data;
    response['query'] = query;

    return response;
}
/* PAGINATION */
function changePage(page) {
    $("#current-page").val(page);
    var url = window.location.pathname;
    var queryData = getQueryData();
    var data = queryData['data'];
    var query = queryData['query'];

    window.history.pushState("object or string", "Title", url+query);
    updateFilterIcon();
    $.ajax({
        url: window.location.origin+url+query,
        data: data,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        if(window.location.pathname == "/metadata/grid") { // grid view
            $('.files-grid').html(response);
            initDragable();
        } else {
            $('.files-list').html(response); // list view
        }
    });
}

function clearFilters() {
    $("input.filter-photographer").each(function() {
        $(this).prop("checked",true)
    });
    $("input.filter-retoucher").each(function() {
        $(this).prop("checked",true)
    });
    $("input.filter-collection").each(function() {
        $(this).prop("checked",true)
    });
    $("input#filter-date-from").val("01/01/2011");
    var now = new Date();
    $("input#filter-date-to").val(formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear());

    updateFilterIcon();
    var url = window.location.pathname;
    window.history.pushState("object or string", "Title", url);
    $.ajax({
        url: window.location.origin+url,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        if(window.location.pathname == "/metadata/grid") { // grid view
            $('.files-grid').html(response);
        } else {
            $('.files-list').html(response); // list view
        }
    });
}


function updateFilterIcon() {
    var elem = $(".filter-icon");
    var active = false;
    $("input.filter-photographer").each(function() {
        if(!$(this).is(":checked")) {
            active = true;
        }
    });
    $("input.filter-retoucher").each(function() {
        if(!$(this).is(":checked")) {
            active = true;
        }
    });
    if($("input#filter-date-from").val() != "01/01/2011") {
        active = true;
    }
    var now = new Date();
    var date_string = formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear();
    if($("input#filter-date-to").val() != date_string) {
        active = true;
    }
    if(active) {
        elem.addClass("active");
        elem.attr('src', '/bundles/app/images/filtersActive.png');
    } else {
        elem.removeClass("active");
        elem.attr('src', '/bundles/app/images/filters.png');
    }
}


/**
 * On image click, on select all
 * @param id
 */
function selectFile(id) {
    var elem_file_row = $('tr[data-file-id='+id+']');
    var checkbox_file = $('input[data-file-id='+id+']');

    // IF SHIFT PRESSED
    if(shifted) {
        var selectedFiles = $("tr.item.selected");
        if(selectedFiles.length < 1) {
            return;
        }
        var firstSelectedIndex = parseInt($(selectedFiles[0]).attr("data-index"));
        var lastSelectedIndex = parseInt($(elem_file_row).attr("data-index"));
        $("tr.item").each(function(index) { // TR
            var fileIndex = parseInt($(this).attr("data-index"));
            if(fileIndex >= firstSelectedIndex  &&  fileIndex <= lastSelectedIndex) {
                $(this).addClass("selected")
            }
        });
        $("input.file-select").each(function(index) { // INPUT
            var fileIndex = parseInt($(this).attr("data-index"));
            if(fileIndex >= firstSelectedIndex  &&  fileIndex <= lastSelectedIndex) {
                $(this).prop('checked', true);
            }
        });
        showInfoPopup();
        return;
    }

    //IF CTRL PRESSED
    if(!ctrled) {
        $("tr.item").each(function(index) {
            $(this).removeClass("selected")
        });
        $("input.file-select").each(function(index) {
            $(this).prop('checked', false);
        });
    }

    if(elem_file_row.hasClass("selected")) {
        elem_file_row.removeClass("selected");
    } else {
        elem_file_row.addClass("selected");
    }

    if(checkbox_file.is(":checked")) {
        checkbox_file.prop('checked', false);
    } else {
        checkbox_file.prop('checked', true);
    }
    showInfoPopup();
}
/**
 * SAVING METADATA FLOW  - START
 */
// var currentFileEditing = "";
// $("textarea.meta-input").on('click',function(){
//     if(currentFileEditing == "") {
//         currentFileEditing = $(this).attr("data-file-id");
//         return;
//     }
//
//     if(currentFileEditing == $(this).attr("data-file-id")) {
//     } else {
//         saveFileMetadata(currentFileEditing);
//     }
//     currentFileEditing = $(this).attr("data-file-id");
// });
//
// $('body').on('click', function (event) {
//     if(!$(event.target).hasClass("meta-input")) {
//         if(currentFileEditing != "") {
//             saveFileMetadata(currentFileEditing);
//         }
//     }
// });

var metaTypingTimer;
var metaInput = $('textarea.meta-input');
metaInput.on('keyup', function () {
    var currentFileEditing = $(this).attr("data-file-id");
    clearTimeout(metaTypingTimer);
    metaTypingTimer = setTimeout(function() {
            saveFileMetadata(currentFileEditing)
    },500);
});
metaInput.on('keydown', function () {
    clearTimeout(metaTypingTimer);
});


function saveFileMetadata(file_id)  {
    var fileMetadata = {};
    var title = $("#file_"+file_id+"_title").val();
    var description = $("#file_"+file_id+"_description").val();
    var keywords = $("#file_"+file_id+"_keywords").val();
    fileMetadata['id'] = file_id;
    fileMetadata['title'] = title;
    fileMetadata['description'] = description;
    fileMetadata['keywords'] = keywords;
    updateMetadata(fileMetadata);
}

/**
 * SAVING METADATA FLOW  - END
 */

/**
 * Update files metadata
 */
function updateMetadata(fileMetadata) {
    var url = "/metadata/ajax";
    var form_data = new FormData();
    $.each(fileMetadata, function( index, value ) {
        form_data.append(index,value);
    });
    form_data.append("action","metadata-update");
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        /*if(response.data.keywords){
            $('#file_'+fileMetadata['id']+'_keywords').val(response.data.keywords)
        }*/
        if(response.status) {
            //location.reload();
            //showPageNotification(response.message);
            $("div.img-content[data-file-id='"+fileMetadata['id']+"']").find(".handle").removeClass("hidden");

        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

/**
 * Keywords count start
 */
function countKeywords(elem) {
    var keywordsValue = $(elem).val();
    var keywordsArr = keywordsValue.split(",");
    if(keywordsArr.length == 1 && keywordsArr[0] == "") {
        $(elem).next(".keywords-count").html(0);
    } else {
        $(elem).next(".keywords-count").html(keywordsArr.length);
    }
}
function countAllFilesKeywords() {
    $("textarea.keywords-input").each(function( index ) {
        var keywordsValue = $(this).val();
        var fileId = $(this).attr("data-file-id");
        /*$(this).val(keywordsValue.replace(/,/g, ', '));*/
        var keywordsArr = keywordsValue.split(",");
        if(keywordsArr.length == 1 && keywordsArr[0] == "") {
            $(".keywords-count[data-file-id='"+fileId+"']").html(0);
        } else {
            $(".keywords-count[data-file-id='"+fileId+"']").html(keywordsArr.length);}
    });
}
countAllFilesKeywords();

/**
 * Keywords count end
 */



/**
 * Grid view
 */

var selected_all = false;
function gridSelectAllFiles() {
    if(selected_all) {
        $(".img-container").removeClass("selected");
        selected_all = false;
    } else {
        $(".img-container").addClass("selected");
        selected_all = true;
    }
    gridShowInfoPopup();
    getFilesMeta();
    gridCountKeyWords();
}

function gridDeselectAllFiles() {
    $(".img-container").removeClass("selected");
    gridShowInfoPopup();
    getFilesMeta();
    gridCountKeyWords();
}
/**
 * Select single files
 * @param elem
 */
function gridSelectSingleFile(elem) {

    // IF SHIFT PRESSED
    if(shifted) {
        var selectedFiles = $(".img-container.selected");
        if(selectedFiles.length < 1) {
            return;
        }
        var firstSelectedIndex = parseInt($(selectedFiles[0]).attr("data-index"));
        var lastSelectedIndex = parseInt($(elem).attr("data-index"));
        if(lastSelectedIndex < firstSelectedIndex) {
            var tmpIndex = firstSelectedIndex;
            firstSelectedIndex = lastSelectedIndex;
            lastSelectedIndex = tmpIndex;
        }
        $(".img-container").each(function(index) {
            var fileIndex = parseInt($(this).attr("data-index"));
            if(fileIndex >= firstSelectedIndex  &&  fileIndex <= lastSelectedIndex) {
                $(this).addClass("selected")
            }
        });
        gridShowInfoPopup();
        getFilesMeta();
        gridCountKeyWords();
        return;
    }

    //IF CTRL PRESSED
    if(!ctrled) {
        $(".img-container").each(function(index) {
            $(this).removeClass("selected")
        });
    }

    if($(elem).hasClass("selected")) {
        $(elem).removeClass("selected")
    } else {
        $(elem).addClass("selected")
    }
    gridShowInfoPopup();
    getFilesMeta();
    gridCountKeyWords();
}
/**
 * Show top right info popup
 */
function gridShowInfoPopup() {
    var info_popup = $(".page-notifications");
    var total_files = 0;
    var selected_files = 0;
    $(".img-container").each(function(index) {
        total_files++;
        if($(this).hasClass("selected")) {
            selected_files++;
        }
    });

    if(selected_files) {
        info_popup.removeClass("hidden");
        $("p.notification-content").html(selected_files+"/"+total_files+" images selected<br><a class='deselect-link' onclick='gridDeselectAllFiles()'>Deselect all</a>");
    } else {
        info_popup.addClass("hidden");
    }
}

/**
 * Getting selected files metadata
 */
function getFilesMeta() {
    // Get selected files ids
    var selectedFilesIds = [];
    $(".img-container").each(function (index) {
        if ($(this).hasClass("selected")) {
            selectedFilesIds.push($(this).attr("data-file-id"));
        }
    });
    // Get selected files
    var selectedFilesMeta = [];
    files.forEach(function(item,index) {
        if(selectedFilesIds.indexOf(item.id.toString()) > -1) {
            selectedFilesMeta.push(item);
        }
    });
    // Create Metadata Object
    var metadata = { "title":"", "description":"", "keywords":"" };
    selectedFilesMeta.forEach(function(item,index) {
        if(index == 0) {
            metadata.title = item.title;
            metadata.description = item.description;
            metadata.keywords = item.keywords;
        } else {
            if(metadata.title != item.title) {
                metadata.title = "< mixed >"
            }
            if(metadata.description != item.description) {
                metadata.description = "< mixed >"
            }
        }
    });
    if($("#grid-common-keywords").is(':checked')) {
        metadata.keywords = mergeCommonKeywords(selectedFilesMeta);
    } else{
        metadata.keywords = mergeKeywords(selectedFilesMeta);
    }

    // Set metadata
    $("#title-input").val(metadata.title);
    $("#description-input").val(metadata.description);
    $("#keywords-input").val(metadata.keywords);
    auto_grow($("#keywords-input"));
    gridCountKeyWords();
}

/**
 * When selecting more files, merge keywords, add * to different words
 * @param selectedItems
 * @returns {string}
 */
function mergeKeywords(selectedItems) {
    var keyWordsList = [];
    var resultList = [];

    if (selectedItems.length >1){

        selectedItems.forEach(function(item, index){
            keyWordsList = keyWordsList.concat(item.keywords.split(','));
        });

        var items = {};
        keyWordsList.forEach(function(singleKeyword, index){
            var name = singleKeyword.trim()
            if(name){
                if(items[name] != undefined){
                    items[name] = ++items[name];
                } else{
                    items[name] = 1;
                }
            }
        });
        for (var keyword in items){
            if(items[keyword]==selectedItems.length){
                resultList.push(keyword);
            } else{
                resultList.push(keyword+'*');
            }
        }
    } else{
        selectedItems.forEach(function(item, index){
            keyWordsList = keyWordsList.concat(item.keywords.split(','));
        });
        keyWordsList.forEach(function(word,i) {
            if(word){
                resultList.push(word);
            }
        })
    }
    return resultList.join(",");
}

function mergeCommonKeywords(selectedItems) {
    var duplicates = [];
    var count = {};
    var counter =0;
    var arr = [];

    var numberOfSelected = selectedItems.length;
    selectedItems.forEach(function(item, index){
        arr = arr.concat(item.keywords.split(','));
    });
    arr.forEach(function(i) { count[i] = (count[i]||0)+1;  });
    for(var i in count){
        if(count[i]==numberOfSelected){
            duplicates[counter] = i;
            counter++;
        }
    }
    return duplicates.join(", ");
}


function updateKeywordsAjax() {
    var gridMetaTypingTimer;
    var gridMetaInput = $('.grid-meta-input');
    gridMetaInput.bind('input propertychange', function () {
        clearTimeout(gridMetaTypingTimer);
        gridMetaTypingTimer = setTimeout(
            gridSaveMetadata, 700);
    });

    gridMetaInput.on('keydown', function () {
        clearTimeout(gridMetaTypingTimer);
    });
}


/**
 * Saving metadata to selected files
 */
function gridSaveMetadata() {
    var data = {};
    var files_to_update = [];
    var multiSelect = false;
    $(".img-container").each(function(index) {
        if($(this).hasClass("selected")) {
            files_to_update.push($(this).attr("data-file-id"));
        }
    });
    if(files_to_update.length < 1) {
        showPageNotification("No file(s) selected");
    }
    data['files'] = files_to_update;
    // TITLE
    var meta_title = $("#title-input").val();
    if(meta_title != "< mixed >") { // Not update if different title
        data['title'] = meta_title;
    }
    // DESCRIPTION
    var meta_description = $("#description-input").val();
    if(meta_description != "< mixed >") { // Not update if different description
        data['description'] = meta_description;
    }
    // KEYWORDS
    var keywords_arr = $("#keywords-input").val().split(",");
    var meta_keywords = [];
    keywords_arr.forEach(function(item,index) {
        if(item.indexOf("*") < 0) {
            meta_keywords.push(item);
        } else{
            multiSelect = true;
        }
    });
    data['keywords'] = meta_keywords;
    var url = "/metadata/ajax";
    var form_data = new FormData();
    form_data.append("action","grid-metadata-update");
    form_data.append("files",data['files']);
    form_data.append("title",data['title']);
    form_data.append("description",data['description']);
    form_data.append("keywords",data['keywords']);
    form_data.append("multi_select",multiSelect);

    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        if(!response.status){
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        } else{
            if(response.data.keywords && !multiSelect){
                data['keywords'] = response.data.keywords;
                gridUpdateFrontMetadata(data);
            } else if(multiSelect) {
                gridUpdateFrontMetadataKeywords(data);
            }
        }


    });
}

/**
 * Update selected files metadata on frontend
 * @param data
 */
function gridUpdateFrontMetadata(data) {
    var filesToUpdate = data['files'];
    files.forEach(function(item,index) {
        if(filesToUpdate.indexOf(String(item.id)) > -1) {
            files[index].title = data['title'];
            files[index].description = data['description'];
            if(typeof (data['keywords']) !== 'string'){
                files[index].keywords = data['keywords'].join(",");
            } else{
                files[index].keywords = data['keywords'];
            }
            //$('#keywords-input').val(data.keywords);
            $(".img-container[data-file-id='"+item.id+"']").find(".handle").removeClass("hidden");
        }
    })
}
/*
* Updating metadata when updating keywords when removing star
* */

function gridUpdateFrontMetadataKeywords(data){
    var filesToUpdate = data['files'];
    var array1 = [];
    var array2 = [];
    var array3 = [];
    files.forEach(function(item,index) {
        if(filesToUpdate.indexOf(String(item.id)) > -1) {
            if(typeof (data['keywords']) !== 'string'){
                array1 = files[index].keywords.split(',');
                array3 = arrayUnique(array1.concat(data['keywords']));
            } else{
                array1 = files[index].keywords.split(',');
                array2 = data['keywords'].split(',');
                array3 = arrayUnique(array1.concat(array2));
            }
            files[index].keywords = array3.join(",");
        }
    })
}


/*Function for returning only unqiue values from array*/
function arrayUnique(array) {
    for (var i = 0; i < array.length; i++) {
        array[i] = array[i].trim();
    }
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }
    return a;
}



/*DRAG AND DROP ORDER - START*/

var old_index;
var new_index;
/*Draggable files*/
function initDragable(){
    dragula([document.getElementById('draggable-container')], {
        revertOnSpill: true
    }).on('drag', function (el, container) {
        old_index = getNodeIndex(el, container);
        new_index = false;
    }).on('cancel', function () {
        old_index = false;
    }).on('drop', function (el, container) {
        new_index= getNodeIndex(el, container);
        replaceGetParameter('order_by', 'custom');
        setIndex(container);
        $("li.order-option").removeClass("active");
        $('#order_custom').addClass("active");
    });
    setIndex();
}
initDragable();
//Return index from element which is inside container
function getNodeIndex(el, container) {
    var divs = $(container).children("div");
    for (var i=0; i< divs.length; i++){
        if (el === divs[i]) {
            $(el).data('order',i);
            return i;
        }
    }
}
//Set new order numbers
function setIndex(container){
    if(!container){
        container = document.getElementById('draggable-container');
    }
    var orderFiles = [];
    var orderNumbers = [];

    var first_index;
    var last_index;
    var page = findGetParameter('page');
    var order_by = findGetParameter('order_by');
    console.log(order_by);
    if(order_by!='custom'){
        return;
    }
    console.log('index set');
    /*last_index not used atm, it will be used if customer decide to go with folder sorting*/
    if(page && page>1){
        first_index = (page-1)*50+1;
        last_index = (page-1)*50+50;
    } else{
        first_index = 1;
        last_index = 50;
    }

    var divs = $(container).children("div");

    for(var i=0;i<divs.length;i++){
        $(divs[i]).attr('data-order', first_index);
        orderFiles.push($(divs[i]).children(":first").data('file-id'));
        orderNumbers.push(first_index);
        first_index++;
    }

    var url = "/metadata/ajax";
    var form_data = new FormData();
    form_data.append("files",orderFiles);
    form_data.append("positions",orderNumbers);
    form_data.append("action","order-update");
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        if(response.status) {
            //location.reload();
            //showPageNotification(response.message);
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
    reIndex();
}

/**
 * Re-index after drag & drop
 */
function reIndex() {
    $(".img-container").each(function(index) {
        $(this).attr("data-index",index);
    });
}

/*DRAG AND DROP ORDER - END */
function gridCountKeyWords(elem) {
    var keywordsValue = $("#keywords-input").val();
    var keywordsArr = keywordsValue.split(",");
    if(keywordsArr.length == 1 && keywordsArr[0] == "") {
        $(".grid-keywords-count").html(0);
    } else {
        $(".grid-keywords-count").html(keywordsArr.length);}
}

/**
 * Deselect files on click
 */
$('body').on('click', function (event) {
    if(window.location.pathname != "/metadata/grid") { // grid view
        return;
    }
    var deselectAll = true;
    $(event.target).parents().each(function( index ) {
        if($( this ).hasClass("metadata-container")) {
            deselectAll = false;
        }
        if($( this ).hasClass("img-container")) {
            deselectAll = false;
        }
        if($( this ).hasClass("cell-action")) {
            deselectAll = false;
        }
    });
    if(deselectAll) {
        gridDeselectAllFiles();
    }
});
//USED BY SELECTION OF FOLDER AND OF SEARCH BY
$(".search-by").change(function() {
    searchFilesRequest();
});
/*
* Auto resize of metadata input field on grid view
* */
function auto_grow(element){
    if(element instanceof jQuery){
        element[0].style.height = "90px";
        element[0].style.height = (element[0].scrollHeight)+"px";
    } else{
        element.style.height = "90px";
        element.style.height = (element.scrollHeight)+"px";
    }

}

$('span.handle').tooltip();

/**
 * Sync data on AWS ( run symfony command )
 */
function syncMetadata() {
    var url = "/metadata/ajax";
    var form_data = new FormData();
    form_data.append("action","sync-metadata");
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        if(response.status) {
            //location.reload();
            showPageNotification(response.message);
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}
/*Switching view*/
$('#listViewButton').click(function(e){
    e.preventDefault();
    if(!$(this).attr('disabled')){
        url = window.location.origin + '/metadata' + window.location.search;
        window.location.replace(url);
    }

    return false;
});

$('#gridViewButton').click(function(e){
    e.preventDefault();
    if(!$(this).attr('disabled')){
        url = window.location.origin + '/metadata/grid' + window.location.search;
        window.location.replace(url);
    }
    return false;
});

/*Autocomplete Folder Start*/
$( "#autocomplete-folder" ).autocomplete({
    autoFocus: true,
    minLength: 0,
    source: foldersList,
    select: function( event, ui ) {
        folderObject = _.findWhere(allFolders, {label:ui.item.value});
        $('#autocomplete-value').val(folderObject.value);
        searchFilesRequest();
    }

});
$( "#autocomplete-folder" ).focus(function() {
    $("#autocomplete-folder").autocomplete( "search", "" );
});
/*Autocomplete Folder End*/

/*GENERAL FUNCTIONS START*/

/*Removing GET param from URL*/
function removeGetParam(parameter)
{
    var url=document.location.href;
    var urlparts= url.split('?');

    if (urlparts.length>=2)
    {
        var urlBase=urlparts.shift();
        var queryString=urlparts.join("?");

        var prefix = encodeURIComponent(parameter)+'=';
        var pars = queryString.split(/[&;]/g);
        for (var i= pars.length; i-->0;)
            if (pars[i].lastIndexOf(prefix, 0)!==-1)
                pars.splice(i, 1);
        url = urlBase+'?'+pars.join('&');
        window.history.pushState('',document.title,url); // added this line to push the new url directly to url bar .

    }
    return url;
}
/*Find params from GET*/
function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}
/*Replace param from GET*/
function replaceGetParameter(paramName, paramValue){
    console.log('replaceGetParameter');
    var url=document.location.href;
    if(paramValue == null)
        paramValue = '';
    var pattern = new RegExp('\\b('+paramName+'=).*?(&|$)')
    if(url.search(pattern)>=0){
        window.history.pushState('', document.title, url.replace(pattern,'$1' + paramValue + '$2')); // added this line to push the new url directly to url bar .
        return url.replace(pattern,'$1' + paramValue + '$2');
    }
    window.history.pushState('', document.title, url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue); // added this line to push the new url directly to url bar .
    return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue
}

/*GENERAL FUNCTIONS END*/