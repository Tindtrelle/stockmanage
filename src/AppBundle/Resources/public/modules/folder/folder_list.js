/**
 * FOLDER LIST
 */

function clearFolderFilters() {
    $("input.filter-photographer").each(function() {
        $(this).prop("checked",true)
    });
    $("input.filter-retoucher").each(function() {
        $(this).prop("checked",true)
    });
    $("input.filter-collection").each(function() {
        $(this).prop("checked",true)
    });
    $("input#filter-date-from").val("01/01/2011");
    var now = new Date();
    $("input#filter-date-to").val(formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear());

    var url = "/folders";
    updateFilterIcon();
    window.history.pushState("object or string", "Title", url);
    $.ajax({
        url: window.location.origin+url,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('.folder-list').html(response);
    });
}

function updateFilterIcon() {
    var elem = $(".filter-icon");
    var active = false;
    $("input.filter-photographer").each(function() {
        if(!$(this).is(":checked")) {
            active = true;
        }
    });
    $("input.filter-retoucher").each(function() {
        if(!$(this).is(":checked")) {
            active = true;
        }
    });
    $("input.filter-collection").each(function() {
        if(!$(this).is(":checked")) {
            active = true;
        }
    });
    if($("input#filter-date-from").val() != "01/01/2011") {
        active = true;
    }
    var now = new Date();
    var date_string = formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear();
    if($("input#filter-date-to").val() != date_string) {
        active = true;
    }
    if(active) {
        elem.addClass("active");
        elem.attr('src', '/bundles/app/images/filtersActive.png');
    } else {
        elem.removeClass("active");
        elem.attr('src', '/bundles/app/images/filters.png');
    }
}

function openNewFolderModal() {
    // Reset values
    $("#create-folder-modal .modal-title").html("Create Folder");
    $(".input-f-id").val("");
    $(".input-f-name").val("");
    $(".input-f-collection").val("");
    $(".input-f-description").val("");

    $("#create-folder-modal").modal('show');
}

function openEditFolderModal(id) {
    var parent_row = "tr[data-folder-id='"+id+"']";
    var f_name = $(parent_row+" .folder-name").text();
    var f_description = $(parent_row+" .folder-description").text();
    var f_collection = $(parent_row+" .folder-collection").text();
    // Set values
    $("#create-folder-modal .modal-title").html("Update Folder")
    $(".input-f-id").val(id);
    $(".input-f-name").val(f_name);
    $(".input-f-collection").val(f_collection);
    $(".input-f-description").val(f_description);
    $("#create-folder-modal").modal('show');
}

function submitCreateNewFolder(event,form) {
    event.preventDefault();
    var url = "/folders/ajax";
    var data = getFormResults(form);
    data.action = "folder-create";
    startLoading();

    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "json",
        method: "POST"
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

var fileTypingTimer;
var $input = $('#folder-search');
$input.on('keyup', function () {
    clearTimeout(fileTypingTimer);
    fileTypingTimer = setTimeout(
        searchFolders,700);
});
$input.on('keydown', function () {
    clearTimeout(fileTypingTimer);
});

function searchFolders() {
    var url = "/folders";
    var query = "?";
    var data = [];
    var search_name = $("input.search").val();
    if(search_name!= "") {
        data.push("name="+search_name);
    }
    // Photographers
    var photographersArr = [];
    $("input.filter-photographer").each(function() {
        if($(this).is(":checked")) {
            photographersArr.push($(this).attr("data-id"));
        }
    });
    var photographers = photographersArr.join(",");
    if(photographers != "" ) {
        data.push('p='+photographers);
    }
    // Retouchers
    var retouchersArr = [];
    $("input.filter-retoucher").each(function() {
        if($(this).is(":checked")) {
            retouchersArr.push($(this).attr("data-id"));
        }
    });
    var retouchers = retouchersArr.join(",");
    if(retouchers != "" ) {
        data.push('r='+retouchers);
    }
    // Collection
    var collectionArr = [];
    $("input.filter-collection").each(function() {
        if($(this).is(":checked")) {
            collectionArr.push($(this).attr("data-name"));
        }
    });
    var collection = collectionArr.join(",");
    if(collection != "") {
        data.push('collection='+collection);
    }
    // Date from - to
    var dateFrom = $("input#filter-date-from").val();
    if(dateFrom != "01/01/2011")  {
        data.push('date_from='+dateFrom);
    }
    var dateTo = $("input#filter-date-to").val();
    var now = new Date();
    var nowString = formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear();

    if(dateTo != nowString) {
        data.push('date_to=' + dateTo);
    }
    query += data.join('&');

    window.history.pushState("object or string", "Title", url+query);

    updateFilterIcon();

    $.ajax({
        url: window.location.origin+url+query,
        data: data,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('.folder-list').html(response);
    });
}
var selected_all_folders = false;
function selectAllFolders() {
    if(selected_all_folders) {
        $("input.folder-select").prop('checked', false);
        selected_all_folders = false;
    } else {
        $("input.folder-select").prop('checked', true);
        selected_all_folders = true;
    }
}

/**
 * Delete selected folders
 * NOTE: Only empty folders will be deleted
 */
function deleteFolders() {
    var r = confirm("Are you sure that you want to delete?");
    if (r == false) {
        return false;
    }
    var folders_to_delete = [];
    $("input.folder-select").each(function(index) {
        if($(this).is(':checked')) {
            folders_to_delete.push($(this).attr("data-folder-id"));
        }
    });
    if(folders_to_delete.length == 0) {
        return;
    }

    var url = "/folders/ajax";
    var form_data = new FormData();
    form_data.append("folders",folders_to_delete);
    form_data.append("action","folders-delete");
    startLoading();
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

function downloadFolderFiles() {
    var folders_to_download = [];
    $("input.folder-select").each(function(index) {
        if($(this).is(':checked')) {
            folders_to_download.push($(this).attr("data-folder-id"));
        }
    });
    // ALLOW ONLY ONE FOLDER DOWNLOAD
    if(folders_to_download.length != 1) {
        return;
    }

    window.open(window.location.origin+'/folder/download?id='+folders_to_download, '_blank');
}