/**
 * FOLDER VIEW 
 */
var order_by = "";
var file_type = "";
var existing_files = [];
function openUploadFilesDialog() {
    $("#btn-files-upload").trigger("click");
}

function submitUploadFilesForm(event) {
    fileList = event.target.files;
    // CHECK IF FILES ALREADY EXISTS
    var form_data = new FormData();
    $.each(fileList, function(key, value){
        form_data.append("file_"+key, value.name);
    });
    form_data.append("files_number",fileList.length);
    form_data.append("action",'validate-file-exists');
    $.ajax({url: window.location.origin+"/folders/ajax",data: form_data,dataType: 'json',
        method: 'POST',processData: false,contentType: false
    }).done(function(response) {
        if(response.status) {
            var existing_files_response = response.data.existing_files;
            if(existing_files_response != "") {
                existing_files = existing_files_response.split(",");
            }
            // NOW SUBMIT UPLOAD FORM
            $("#frm-files-upload").submit();
        }
    });
}

/**
 * Upload files
 * @param event
 * @param form
 */
var upload_files_count = 0;
var uploaded_files_count = 0;
function startUploadFiles(event,form)
{
    event.stopPropagation();
    event.preventDefault();
    // UPLOAD FILE ONE BY ONE
    var files_to_upload = [];
    $.each(fileList, function(key, value)
    {
        if(existing_files.indexOf(value.name) < 0) { // Skip existing files
            files_to_upload.push(value);
        }
    });
    startLoading();
    upload_files_count = files_to_upload.length;
    uploadFileRequest(form,files_to_upload);
}

function uploadFileRequest(form,files_to_upload) {
    if(!files_to_upload.length) { // DONE WITH UPLOADING
        endLoading();
        var message = uploaded_files_count + " files uploaded <br>";
        if(existing_files.length) {
            message += existing_files.join("<br>") + " <br> already exists ";
        }
        showPageNotification(message);
        searchFilesRequest();
        // Reset counters
        upload_files_count = 0;
        uploaded_files_count = 0;
        return;
    }

    $(".loading-caption").html("Uploading file "+ (uploaded_files_count + 1) + "/"+upload_files_count);
    var url = "/folders/ajax";
    files_to_upload.forEach(function (item,index) {
        if(index == 0) { // ONLY FOR ONE ITEM
            // Create form data
            var form_data = new FormData();
            form_data.append("action","files-upload");
            var dataFields = getFormResults(form);
            form_data.append("folder_id",dataFields['folder_id']);
            form_data.append("folder_name",dataFields['folder_name']);
            // Add file
            form_data.append("file_0", item);
            form_data.append("files_number",1);
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function(){
                if(xhr.readyState == 4){
                    //clearInterval(uploadInterval);
                    var response = JSON.parse(xhr.response);
                    if(response.status) {
                        console.log(response);
                        files_to_upload.splice(index,1);
                        uploaded_files_count++;
                        $(".loading-caption").html("Uploading files "+uploaded_files_count+"/"+upload_files_count);
                        uploadFileRequest(form,files_to_upload);
                    } else { // ERRORS
                        endLoading();
                        openWarningModal({message:response.message, status: 0 });
                    }
                }
            };
            xhr.upload.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    var total = (percentComplete * 100).toFixed(2);
                    $(".loading-description").html(total+"%");
                }
            }, false);
            xhr.open("post", url);
            xhr.send(form_data);
            return false;
        }
    });
}

function clearFilesFilters() {
    $("input.filter-photographer").each(function() {
        $(this).prop("checked",true)
    });
    $("input.filter-retoucher").each(function() {
        $(this).prop("checked",true)
    });
    $("input.filter-collection").each(function() {
        $(this).prop("checked",true)
    });
    $("input#filter-date-from").val("01/01/2011");
    var now = new Date();
    $("input#filter-date-to").val(formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear());

    updateFilterIcon();
    var url = "/folders/view/"+$("#folder-id").val();
    window.history.pushState("object or string", "Title", url);
    $.ajax({
        url: window.location.origin+url,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('.files-list').html(response);
    });
}

function updateFilterIcon() {
    var elem = $(".filter-icon");
    var active = false;
    $("input.filter-photographer").each(function() {
        if(!$(this).is(":checked")) {
            active = true;
        }
    });
    $("input.filter-retoucher").each(function() {
        if(!$(this).is(":checked")) {
            active = true;
        }
    });
    if($("input#filter-date-from").val() != "01/01/2011") {
        active = true;
    }
    var now = new Date();
    var date_string = formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear();
    if($("input#filter-date-to").val() != date_string) {
        active = true;
    }
    if(active) {
        elem.addClass("active");
        elem.attr('src', '/bundles/app/images/filtersActive.png');
    } else {
        elem.removeClass("active");
        elem.attr('src', '/bundles/app/images/filters.png');
    }
}

/**
 * NOT USED ATM ( USING FOR UPLOAD BACKEND PROCESS )
 * Get uploading process
 * @param upload_progress_key
 */
// function uploadProgress(upload_progress_key) {
//     var url = "/folders/ajax";
//     var form_data = new FormData();
//     form_data.append("action","upload-progress");
//     form_data.append("upload_progress_key",upload_progress_key);
//     var up_xhr = new XMLHttpRequest();
//     up_xhr.onreadystatechange = function(){
//         if(up_xhr.readyState == 4){
//             var response = JSON.parse(up_xhr.response);
//             if(response.status) {
//                 $(".loading-caption").html("Uploading...");
//                 if(response.data) {
//                     $(".loading-caption").html(response.data+"% uploading to Amazon...");
//                 }
//             }
//         }
//     };
//     up_xhr.open("post", url);
//     up_xhr.send(form_data);
// }

/**
 * Select / deselect files
 * @type {boolean}
 */
var selected_all = false;
function selectAllFiles() {
    if(selected_all) {
        $(".img-container").removeClass("selected");
        selected_all = false;
    } else {
        $(".img-container").addClass("selected");
        selected_all = true;
    }
    showInfoPopup();
}

function deselectAll() {
    $(".img-container").removeClass("selected");
    showInfoPopup();
}
/**
 * Select single files
 * @param elem
 */
function selectSingleFile(elem) {
    // IF SHIFT PRESSED
    if(shifted) {
        var selectedFiles = $(".img-container.selected");
        if(selectedFiles.length < 1) {
            return;
        }
        var firstSelectedIndex = parseInt($(selectedFiles[0]).attr("data-index"));
        var lastSelectedIndex = parseInt($(elem).attr("data-index"));
        $(".img-container").each(function(index) {
            var fileIndex = parseInt($(this).attr("data-index"));
            if(fileIndex >= firstSelectedIndex  &&  fileIndex <= lastSelectedIndex) {
                $(this).addClass("selected")
            }
        });
        showInfoPopup();
        return;
    }

    //IF CTRL PRESSED
    if(!ctrled) {
        $(".img-container").each(function(index) {
            $(this).removeClass("selected")
        });
    }

    if($(elem).hasClass("selected")) {
        $(elem).removeClass("selected")
    } else {
        $(elem).addClass("selected")
    }
    showInfoPopup();
}
/**
 * Show top right info popup
 */
function showInfoPopup() {
    var info_popup = $(".page-notifications");
    var total_files = 0;
    var selected_files = 0;
    $(".img-container").each(function(index) {
        total_files++;
        if($(this).hasClass("selected")) {
            selected_files++;
        }
    });

    if(selected_files) {
        info_popup.removeClass("hidden");
        $("p.notification-content").html(selected_files+"/"+total_files+" images selected<br><a class='deselect-link' onclick='deselectAll()'>Deselect all</a>");
    } else {
        info_popup.addClass("hidden");
    }
}
/**
 * Download Files
 */
function downloadFiles() {
    var files_to_download = [];
    $(".img-container").each(function(index) {
        if($(this).hasClass("selected")) {
            files_to_download.push($(this).attr("data-file-id"));
        }
    });

    if(files_to_download.length == 0) {
        return;
    }

    window.open(window.location.origin+'/file/download?id='+files_to_download, '_blank');
}
/*
* Mark files as video
* */
function markAsVideo() {
    var files_to_mark = [];
    $(".img-container").each(function(index) {
        if($(this).hasClass("selected")) {
            files_to_mark.push($(this).attr("data-file-id"));
        }
    });

    if(files_to_download.length == 0) {
        return;
    }

    var url = "/folders/ajax";
    var form_data = new FormData();
    form_data.append("files",files_to_delete);
    form_data.append("action","mark-as-video");
    startLoading();
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}
/**
 * Delete files
 */
function deleteFiles() {
    var r = confirm("Are you sure that you want to delete?");
    if (r == false) {
        return false;
    }
    var files_to_delete = [];
    $(".img-container").each(function(index) {
        if($(this).hasClass("selected")) {
            files_to_delete.push($(this).attr("data-file-id"));
        }
    });
    if(files_to_delete.length == 0) {
        return;
    }

    var url = "/folders/ajax";
    var form_data = new FormData();
    form_data.append("files",files_to_delete);
    form_data.append("action","files-delete");
    startLoading();
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}


/**
 * Search files
 */
var fileTypingTimer;
var $input = $('#input-file-search');
$input.on('keyup', function () {
    clearTimeout(fileTypingTimer);
    fileTypingTimer = setTimeout(
        searchFilesRequest,500);
});
$input.on('keydown', function () {
    clearTimeout(fileTypingTimer);
});

function searchFilesRequest() {
    var url = "/folders/view/"+$("#folder-id").val();
    var data = [];
    var query = "?";
    var search_name = $("input.search").val();
    if(search_name!= "") {
        data.push("name="+search_name);
    }
    // PHOTOGRAPHERS CHECKBOX
    var photographersArr = [];
    var photographersLength = $("input.filter-photographer").length;
    $("input.filter-photographer").each(function() {
        if($(this).is(":checked")) {
            photographersArr.push($(this).attr("data-id"));
        }
    });
    var photographers = photographersArr.join(",");
    if(photographers != "" && photographersLength != photographersArr.length  ) {
        data.push('p='+photographers);
    }
    // RETOUCHERS CHECKBOX
    var retouchersArr = [];
    var retouchersLength = $("input.filter-retoucher").length;
    $("input.filter-retoucher").each(function() {
        if($(this).is(":checked")) {
            retouchersArr.push($(this).attr("data-id"));
        }
    });
    var retouchers = retouchersArr.join(",");
    if(retouchers != "" && retouchersLength != retouchersArr.length ) {
        data.push('r='+retouchers);
    }
    // Collection
    var collectionArr = [];
    $("input.filter-collection").each(function() {
        if($(this).is(":checked")) {
            collectionArr.push($(this).attr("data-name"));
        }
    });
    var collection = collectionArr.join(",");
    if(collection != "") {
        data.push('collection='+collection);
    }
    // Date from - to
    var dateFrom = $("input#filter-date-from").val();
    if(dateFrom != "01/01/2011")  {
        data.push('date_from='+dateFrom);
    }
    var dateTo = $("input#filter-date-to").val();
    var now = new Date();
    var nowString = formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear();

    if(dateTo != nowString) {
        data.push('date_to=' + dateTo);
    }
    if(order_by != "") {
        data.push('order_by=' + order_by);
    }
    if(file_type != ""){
        data.push(file_type);
    }

    query += data.join('&');

    window.history.pushState("object or string", "Title", url+query);
    updateFilterIcon();
    $.ajax({
        url: window.location.origin+url+query,
        data: data,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('.files-list').html(response);
    });
}

/**
 * Change order
 * @param elem
 * @param orderBy
 */
function updateOrderBy(elem,orderBy) {
    $("li.order-option").removeClass("active");
    $(elem).addClass("active");
    order_by = orderBy;
    searchFilesRequest();
}

/*Display videos*/

function updateFileType(elem, type) {
    $("li.order-option").removeClass("active");
    $(elem).addClass("active");
    file_type = type;
    searchFilesRequest();
}
