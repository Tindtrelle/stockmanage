var filter_agency = "";
var filter_status = "";
var order_by = "";
var file_type = "";

function openNewAgencyModal() {
    // Reset values
    $("#create-agency-modal .modal-title").html("Create Agency");
    $(".input-f-id").val("");
    $(".input-f-name").val("");

    $("#create-agency-modal").modal('show');
}

function submitCreateNewAgency(event,form) {
    event.preventDefault();
    var url = "/agency/ajax";
    var data = getFormResults(form);
    data.action = "agency-create";
    startLoading();

    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "json",
        method: "POST"
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

var selected_all_files = false;
function selectAllFolders() {
    if(selected_all_files) {
        $("input.file-select").prop('checked', false);
        selected_all_files = false;
    } else {
        $("input.file-select").prop('checked', true);
        selected_all_files = true;
    }
    // update background
    $("input.file-select").each(function(index) {
        var elem_file_row = $('tr[data-file-id='+$(this).attr("data-file-id")+']');
        if($(this).is(':checked')) {
            elem_file_row.addClass("selected");
        } else {
            elem_file_row.removeClass("selected");
        }
    });
    showInfoPopup();
}

function deselectAll() {
    $("input.file-select").prop('checked', false);
    $("input.file-select").each(function(index) {
        var elem_file_row = $('tr[data-file-id='+$(this).attr("data-file-id")+']');
        elem_file_row.removeClass("selected");
    });
    showInfoPopup();
}

function downloadFiles() {
    var files_to_download = [];
    $("input.file-select").each(function(index) {
        if($(this).is(':checked')) {
            files_to_download.push($(this).attr("data-file-id"));
        }
    });

    if(files_to_download.length == 0) {
        return;
    }

    window.open(window.location.origin+'/file/download?id='+files_to_download, '_blank');
}

/**
 * Delete files
 */
function deleteFiles() {
    var r = confirm("Are you sure that you want to delete?");
    if (r == false) {
        return false;
    }
    var files_to_delete = [];
    $("input.file-select").each(function(index) {
        if($(this).is(':checked')) {
            files_to_delete.push($(this).attr("data-file-id"));
        }
    });
    if(files_to_delete.length == 0) {
        return;
    }

    var url = "/folders/ajax";
    var form_data = new FormData();
    form_data.append("files",files_to_delete);
    form_data.append("action","files-delete");
    startLoading();
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

/**
 * Search files
 */
var fileTypingTimer;
var $input = $('#input-file-search');
$input.on('keyup', function () {
    clearTimeout(fileTypingTimer);
    fileTypingTimer = setTimeout(
        searchFilesRequest,200);
});
$input.on('keydown', function () {
    clearTimeout(fileTypingTimer);
});

function searchFilesRequest() {
    var url = "/agency";
    $("#current-page").val(1);
    var queryData = getQueryData();
    var data = queryData['data'];
    var query = queryData['query'];

    window.history.pushState("object or string", "Title", url+query);
    updateFilterIcon();
    $.ajax({
        url: window.location.origin+url+query,
        data: data,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('.files-list').html(response);
    });
}

function getQueryData() {
    var response = {};

    var data = [];
    var query = "?";

    var page = $("#current-page").val();
    if(page != "") {
        data.push("page="+page);
    }

    var search_name = $("input.search").val();
    if(search_name!= "") {
        data.push("name="+search_name);
    }
    // PHOTOGRAPHERS CHECKBOX
    var photographersArr = [];
    var photographersLength = $("input.filter-photographer").length;
    $("input.filter-photographer").each(function() {
        if($(this).is(":checked")) {
            photographersArr.push($(this).attr("data-id"));
        }
    });
    var photographers = photographersArr.join(",");
    if(photographers != "" && photographersLength != photographersArr.length  ) {
        data.push('p='+photographers);
    }
    // RETOUCHERS CHECKBOX
    var retouchersArr = [];
    var retouchersLength = $("input.filter-retoucher").length;
    $("input.filter-retoucher").each(function() {
        if($(this).is(":checked")) {
            retouchersArr.push($(this).attr("data-id"));
        }
    });
    var retouchers = retouchersArr.join(",");
    if(retouchers != "" && retouchersLength != retouchersArr.length ) {
        data.push('r='+retouchers);
    }
    // Collection
    var collectionArr = [];
    $("input.filter-collection").each(function() {
        if($(this).is(":checked")) {
            collectionArr.push($(this).attr("data-name"));
        }
    });
    var collection = collectionArr.join(",");
    if(collection != "") {
        data.push('collection='+collection);
    }
    // Date from - to
    var dateFrom = $("input#filter-date-from").val();
    if(dateFrom != "01/01/2011")  {
        data.push('date_from='+dateFrom);
    }
    var dateTo = $("input#filter-date-to").val();
    var now = new Date();
    var nowString = formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear();

    if(dateTo != nowString) {
        data.push('date_to=' + dateTo);
    }

    if(filter_agency != "" && filter_status != "") {
        data.push('agency=' + filter_agency);
        data.push('status=' + filter_status);
    }

    if(order_by != "") {
        data.push('order_by=' + order_by);
    }

    if(file_type != ""){
        data.push(file_type);
    }

    query += data.join('&');

    response['data'] = data;
    response['query'] = query;

    return response;
}


/* PAGINATION */
function changePage(page) {
    $("#current-page").val(page);
    var url = "/agency";
    var queryData = getQueryData();
    var data = queryData['data'];
    var query = queryData['query'];

    window.history.pushState("object or string", "Title", url+query);
    updateFilterIcon();
    $.ajax({
        url: window.location.origin+url+query,
        data: data,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('.files-list').html(response);
    });
}

function clearFilters() {
    $("input.filter-photographer").each(function() {
        $(this).prop("checked",true)
    });
    $("input.filter-retoucher").each(function() {
        $(this).prop("checked",true)
    });
    $("input.filter-collection").each(function() {
        $(this).prop("checked",true)
    });
    $("input#filter-date-from").val("01/01/2011");
    var now = new Date();
    $("input#filter-date-to").val(formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear());

    updateFilterIcon();
    var url = "/agency";
    window.history.pushState("object or string", "Title", url);
    $.ajax({
        url: window.location.origin+url,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('.files-list').html(response);
    });
}

function clearAgencyFilter() {
    filter_agency = "";
    filter_status = "";
    searchFilesRequest();
}

function updateFilterIcon() {
    var elem = $(".filter-icon");
    var active = false;
    $("input.filter-photographer").each(function() {
        if(!$(this).is(":checked")) {
            active = true;
        }
    });
    $("input.filter-retoucher").each(function() {
        if(!$(this).is(":checked")) {
            active = true;
        }
    });
    if($("input#filter-date-from").val() != "01/01/2011") {
        active = true;
    }
    var now = new Date();
    var date_string = formatDateString(now.getDate())+"/"+(formatDateString(now.getMonth()+1))+"/"+now.getFullYear();
    if($("input#filter-date-to").val() != date_string) {
        active = true;
    }
    if(active) {
        elem.addClass("active");
        elem.attr('src', '/bundles/app/images/filtersActive.png');
    } else {
        elem.removeClass("active");
        elem.attr('src', '/bundles/app/images/filters.png');
    }
}

function updateFileAgencyStatus(status,file,agency) {
    var files_to_update = [];
    $("input.file-select").each(function(index) {
        if($(this).is(':checked')) {
            files_to_update.push($(this).attr("data-file-id"));
        }
    });
    if(files_to_update.length == 0) { // Update single file
        files_to_update.push(file);
    }

    if(status == "rejected") {
        openUpdateStatusModal(status,files_to_update,agency,file);
        return;
    }

    ajaxUpdateMultipleFiles(status,files_to_update,agency,file, "");
}

/**
 * Update multiple files agency status
 * @param status
 * @param files
 * @param agency
 * @param originalFile
 * @param description ( IF REJECTED )
 */
function ajaxUpdateMultipleFiles(status,files,agency,originalFile, description) {
    var url = "/agency/ajax";
    var form_data = new FormData();
    form_data.append("files",files);
    form_data.append("agency",agency);
    form_data.append("status",status);
    form_data.append("description",description);
    form_data.append("action","status-update-multiple");
    //startLoading();
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        //endLoading();
        if(response.status) {
            //location.reload();
            showPageNotification(response.message);
            // Close dropdown
            $("#"+"file_"+ originalFile +"_agency_"+ agency).parent().removeClass('open');
            for(var i=0; i < files.length; i++) {
                if(response.data) {
                    var elem = ".file_"+ files[i] +"_agency_"+ agency;
                    $(elem).html('<div class="status-icon-inner" />');
                    updateStatusClass(elem,response.data.status);
                }
            }
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

/**
 * Open status update modal ( in case that status is REJECTED )
 * @param status
 * @param files_to_update
 * @param agency
 * @param file
 */
function openUpdateStatusModal(status,files_to_update,agency,file) {
    $("#status-update-modal").modal('show');
    $("#sum-status").val(status);
    $("#sum-files").val(files_to_update);
    $("#sum-agency").val(agency);
    $("#sum-original-file").val(file);
}
/**
 * Update status from modal ( in case that status is REJECTED )
 * @param event
 * @param form
 */
function submitUpdateStatus(event,form) {
    event.preventDefault();
    var data = getFormResults(form);
    var files = data.files.split(",");
    ajaxUpdateMultipleFiles(data.status,files,data.agency,data.file,data.description);
    $("#status-update-modal").modal('hide');
}

// /** NOT USED ATM
//  * Update single file agency status
//  * @param status
//  * @param file
//  * @param agency
//  */
// function ajaxUpdateSingleFile(status,file,agency) {
//     var url = "/agency/ajax";
//     var form_data = new FormData();
//     form_data.append("file",file);
//     form_data.append("agency",agency);
//     form_data.append("status",status);
//     form_data.append("action","status-update");
//     //startLoading();
//     $.ajax({
//         url: window.location.origin+url,
//         data: form_data,
//         cache: false,
//         dataType: 'json',
//         method: 'POST',
//         processData: false, // Don't process the files
//         contentType: false // Set content type to false as jQuery will tell the server its a query string request
//     }).done(function(response) {
//         //endLoading();
//         if(response.status) {
//             //location.reload();
//             showPageNotification(response.message);
//             // Close dropdown
//             $("#"+"file_"+ file +"_agency_"+ agency).parent().removeClass('open');
//
//             if(response.data) {
//                 var elem = ".file_"+ file +"_agency_"+ agency;
//                 $(elem).html(response.data.status);
//                 updateStatusClass(elem,response.data.status);
//             }
//         } else { // ERRORS
//             console.log(response);
//             openWarningModal({message:response.message, status: 0 });
//         }
//     });
// }

function updateStatusClass(elem,status) {
    // Remove current
    if($(elem).hasClass("status-approved")) {
        $(elem).removeClass("status-approved");
    } else if($(elem).hasClass("status-rejected")) {
        $(elem).removeClass("status-rejected");
    } else if($(elem).hasClass("status-pending")) {
        $(elem).removeClass("status-pending")
    } else {
        $(elem).removeClass("status-not-sent");
    }
    // Add new
    if(status.toLowerCase() == 'approved') {
        $(elem).addClass("status-approved");
    } else if (status.toLowerCase() == 'rejected') {
        $(elem).addClass("status-rejected");
    } else if (status.toLowerCase() == 'pending') {
        $(elem).addClass("status-pending");
    } else {
        $(elem).addClass("status-not-sent");
    }
}

function showHideAgencyFilter(e,id) {
    e.stopPropagation();
    $("."+id).dropdown('toggle')
}

/**
 * On image click, on select all
 * @param id
 */
function selectFile(id) {
    var elem_file_row = $('tr[data-file-id='+id+']');
    var checkbox_file = $('input[data-file-id='+id+']');

    // IF SHIFT PRESSED
    if(shifted) {
        var selectedFiles = $("tr.item.selected");
        if(selectedFiles.length < 1) {
            return;
        }
        var firstSelectedIndex = parseInt($(selectedFiles[0]).attr("data-index"));
        var lastSelectedIndex = parseInt($(elem_file_row).attr("data-index"));
        $("tr.item").each(function(index) { // TR
            var fileIndex = parseInt($(this).attr("data-index"));
            if(fileIndex >= firstSelectedIndex  &&  fileIndex <= lastSelectedIndex) {
                $(this).addClass("selected")
            }
        });
        $("input.file-select").each(function(index) { // INPUT
            var fileIndex = parseInt($(this).attr("data-index"));
            if(fileIndex >= firstSelectedIndex  &&  fileIndex <= lastSelectedIndex) {
                $(this).prop('checked', true);
            }
        });
        showInfoPopup();
        return;
    }

    //IF CTRL PRESSED
    if(!ctrled) {
        $("tr.item").each(function(index) {
            $(this).removeClass("selected")
        });
        $("input.file-select").each(function(index) {
            $(this).prop('checked', false);
        });
    }

    if(elem_file_row.hasClass("selected")) {
        elem_file_row.removeClass("selected");
        checkbox_file.prop('checked', false);
    } else {
        elem_file_row.addClass("selected");
        checkbox_file.prop('checked', true);
    }
    showInfoPopup();
}
/**
 * Filter by agency and status
 * @type {string}
 */
function filterByAgencyStatus(agency,status) {
    filter_agency = agency;
    filter_status = status;
    searchFilesRequest();
}

/**
 * Change order
 * @param elem
 * @param orderBy
 */
function updateOrderBy(elem,orderBy) {
    $("li.order-option").removeClass("active");
    $(elem).addClass("active");
    order_by = orderBy;
    searchFilesRequest();
}
/*Display videos*/
function updateFileType(elem, type) {
    $("li.order-option").removeClass("active");
    $(elem).addClass("active");
    file_type = type;
    searchFilesRequest();
}

/**
 * Send files to FTP
 */
function sendFilesToFtp(agencyId) {
    var files_to_send = [];
    $("input.file-select").each(function(index) {
        if($(this).is(':checked')) {
            files_to_send.push($(this).attr("data-file-id"));
        }
    });
    if(files_to_send.length == 0) {
        showPageNotification("Please select file(s)");
        return;
    }

    var url = "/agency/ajax";
    var form_data = new FormData();
    form_data.append("files",files_to_send);
    form_data.append("agency",agencyId);
    form_data.append("action","files-send-ftp");
    startLoading();
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        endLoading();
        console.log(response);
        if(response.status) {
            showPageNotification(response.message);
        } else { // ERRORS
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

/**
 * Show top right info popup
 */
function showInfoPopup() {
    var info_popup = $(".page-notifications");
    var total_files = 0;
    var selected_files = 0;
    $("input.file-select").each(function(index) {
        total_files++;
        if($(this).is(':checked')) {
            selected_files++;
        }
    });

    if(selected_files) {
        info_popup.removeClass("hidden");
        $("p.notification-content").html(selected_files+"/"+total_files+" images selected<br><a class='deselect-link' onclick='deselectAll()'>Deselect all</a>");
    } else {
        info_popup.addClass("hidden");
    }
}





