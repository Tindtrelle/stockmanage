updateTransactionType();
updateSelectedSourceDest();
function updateTransactionType() {
    var type = $('#id_type');
    changeTransactionType(type[0]);
}
function updateSelectedSourceDest() {
    sourceCat = $('#source-category');
    sourceDrop = $('#source_dropdown');
    destinationCat = $('#destination_category');
    destinationDrop = $('#destination_dropdown');
    sourceCatId = sourceCat[0].value;
    destinationCatId = destinationCat[0].value;

    $.ajax({
        url: "/finance/entities/list/"+sourceCatId,
        dataType: "json",
        method: "GET"
    }).done(function(response) {
        var dropdown = $(sourceDrop);
        dropdown.empty();
        $.each(response, function (i, category) {
            dropdown.append($('<option>', {
                value: category.id,
                text : capitalizeFirstLetter(category.name + " " + (category.currency?category.currency:""))
            }));
        });
        console.log(1);
        source_id = $(sourceDrop).data("source-id");
        $('#source_dropdown option[value='+source_id+']').prop('selected', true);
    });
    $.ajax({
        url: "/finance/entities/list/"+destinationCatId,
        dataType: "json",
        method: "GET"
    }).done(function(response) {
        var dropdown = $(destinationDrop);
        dropdown.empty();
        $.each(response, function (i, category) {
            dropdown.append($('<option>', {
                value: category.id,
                text : capitalizeFirstLetter(category.name + " " + (category.currency?category.currency:""))
            }));
        });
        console.log(2);
        source_id = $(destinationDrop).data("source-id");
        $('#destination_dropdown option[value='+source_id+']').prop('selected', true);
    });
}
function changeTransactionType(type) {
    if (type.value == 'expense'){
        $("#div_id_amount_destination").addClass('hidden');
        $("#destination_currency").addClass('hidden');
        $("#source_category_div").addClass('hidden');
        $("#destination_category_div").removeClass('hidden');
        $("#div_id_amount").removeClass('hidden');
        getSources();
    }else if(type.value == 'transfer'){
        $("#div_id_amount_destination").removeClass('hidden');
        $("#destination_currency").removeClass('hidden');
        $("#source_category_div").removeClass('hidden');
        $("#destination_category_div").removeClass('hidden');
        $("#div_id_amount").removeClass('hidden');
    }else if(type.value == 'income'){
        $("#destination_category_div").addClass('hidden');
        $("#div_id_amount").addClass('hidden');
        $("#div_id_amount_destination").removeClass('hidden');
        $("#destination_currency").removeClass('hidden');
        $("#source_category_div").removeClass('hidden');
        getDestinations();
    }
}
function getSources() {
    $.ajax({
        url: "/finance/entities/sources",
        dataType: "json",
        method: "GET"
    }).done(function(response) {
        var dropdown = $('#source_dropdown');
        dropdown.empty();
        $.each(response, function (i, category) {
            dropdown.append($('<option>', {
                value: category.id,
                text : capitalizeFirstLetter(category.name + " " +category.currency)
            }));
        });
        source_id = $('#source_dropdown').data("source-id");
        $('#source_dropdown option[value='+source_id+']').prop('selected', true)
    });
}
function getDestinations() {
    $.ajax({
        url: "/finance/entities/destinations",
        dataType: "json",
        method: "GET"
    }).done(function(response) {
        var dropdown = $('#destination_dropdown');
        dropdown.empty();
        $.each(response, function (i, category) {
            dropdown.append($('<option>', {
                value: category.id,
                text : capitalizeFirstLetter(category.name + " " +category.currency)
            }));
        });
        source_id = $('#destination_dropdown').data("source-id");
        $('#destination_dropdown option[value='+source_id+']').prop('selected', true)
    });
}
function changeCategory(category, type, onload){
    categoryId = category.value;
    if(type =='source'){
        dropDownID = "#source_dropdown";
    } else{
        dropDownID = "#destination_dropdown";
    }
    $.ajax({
        url: "/finance/entities/list/"+categoryId,
        dataType: "json",
        method: "GET"
    }).done(function(response) {
        var dropdown = $(dropDownID);
        dropdown.empty();
        $.each(response, function (i, category) {
            dropdown.append($('<option>', {
                value: category.id,
                text : capitalizeFirstLetter(category.name + " " + (category.currency?category.currency:""))
            }));
        });
        if(onload){
            source_id = $(dropDownID).data("source-id");
            $(dropDownID+' option[value='+source_id+']').prop('selected', true);
        }
    });
}
$('.f-datepicker').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
    todayBtn: true,
    todayHighlight: true,
    orientation: "top"
});
