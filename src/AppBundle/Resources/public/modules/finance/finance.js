var dropdownOptions = []; // source/destination options
var shootings = []; // source/destination options
var studios = []; // source/destination options
var usersSalary = []; // users for salary
var transactionType = ""; // transaction type
var current_user_id;

$('.f-datepicker').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
    todayBtn: true,
    todayHighlight: true,
    orientation: "bottom"
});
function showTransactionForm(type) {
    transactionType = type;
    $(".t-type-container").fadeOut();
    $(".t-form-container").fadeIn();
    updateTransFormBasedOnType(type);
}

function hideTransactionForm() {
    $(".t-type-container").fadeIn();
    $(".t-form-container").fadeOut();
}

function updateTransactionType() {
    var type = $('select[name=type]').val();
    transactionType = type;
    updateTransFormBasedOnType(type);
}

function getDropdownOptionsByType(type) {
    var options = {source:[],destination:[]};
    _.each(dropdownOptions, function(item) {
        if(type == 'expense') {
            if(_.indexOf(item.positions, 'expense-source') > -1) {
                if (item.owner && (current_user_id==item.owner || item.accessUsers.indexOf(""+current_user_id+"")!=-1) && item.is_parent != '1'){
                    options.source.push(item);
                }
            }
            if(_.indexOf(item.positions, 'expense-destination') > -1) {
                options.destination.push(item);
            }
        } else if (type == 'transfer') {
            if(_.indexOf(item.positions, 'transfer-source') > -1) {
                if (item.owner && (current_user_id==item.owner || item.accessUsers.indexOf(""+current_user_id+"")!=-1) && item.is_parent != '1'){
                    options.source.push(item);
                }
            }
            if(_.indexOf(item.positions, 'transfer-destination') > -1) {
                if ((item.is_parent == "" && !item.parent) || item.is_parent == '1'){
                    options.destination.push(item);
                }
            }
        } else if( type == 'income') {
            if(_.indexOf(item.positions, 'income-source') > -1) {
                options.source.push(item);
            }
            if(_.indexOf(item.positions, 'income-destination') > -1) {
                options.destination.push(item);
            }
        }
    });

    return options;
}
/**
 * Update transaction form based on transaction type
 * @param type
 */
function updateTransFormBasedOnType(type) {
    $('select[name=type]').val(type);
    if(type == 'expense') {
        $(".t-form-container").addClass('expense');
        $(".t-form-container").removeClass('transfer');
        $(".t-form-container").removeClass('income');
        $('[name=source_amount]').removeClass('hidden');
        $('[name=destination_amount]').addClass('hidden');

        $('[name=source_currency]').removeClass('hidden');
        $('[name=destination_currency]').addClass('hidden');
    } else if (type == 'transfer') {
        $(".t-form-container").removeClass('expense');
        $(".t-form-container").addClass('transfer');
        $(".t-form-container").removeClass('income');
        $('[name=source_amount]').removeClass('hidden');
        $('[name=destination_amount]').removeClass('hidden');

        $('[name=source_currency]').removeClass('hidden');
        $('[name=destination_currency]').removeClass('hidden');
    } else if(type == 'income') {
        $(".t-form-container").removeClass('expense');
        $(".t-form-container").removeClass('transfer');
        $(".t-form-container").addClass('income');
        $('[name=source_amount]').addClass('hidden');
        $('[name=destination_amount]').removeClass('hidden');

        $('[name=source_currency]').addClass('hidden');
        $('[name=destination_currency]').removeClass('hidden');
    }
    // Update source destination options
    updateSourceDestinationDropdowns(type);

}

/**
 * Reset all dropdowns to default position
 */
function resetDropdownsToDefault(type) {
    var sourceDropdown =$('select[name=source]');
    var sourceCategoryDropdown =$('select[name=source-category]');
    var destinationDropdown = $('select[name=destination]');
    var destinationCategoryDropdown =$('select[name=destination-category]');
    var folderSelection =$('select[name=select-transaction-folder]');

    sourceDropdown.find('option').remove();
    sourceDropdown.removeClass("hidden");
    sourceDropdown.append($('<option>', {
        label: "Source"
    }));
    sourceCategoryDropdown.find('option').remove();
    sourceCategoryDropdown.addClass("hidden");
    sourceCategoryDropdown.append($('<option>', {
        label: "Source category"
    }));
    destinationDropdown.find('option').remove();
    destinationDropdown.removeClass("hidden");
    destinationDropdown.append($('<option>', {
        label: "Destination"
    }));
    destinationCategoryDropdown.find('option').remove();
    destinationCategoryDropdown.addClass("hidden");
    destinationCategoryDropdown.append($('<option>', {
        label: "Destination category"
    }));
    // Move in default position
    $(".source-destination-container").html("");
    $(".info-container").html("");
    if(type == 'income') {
        $(".source-destination-container").append(sourceCategoryDropdown);
        $(".source-destination-container").append(destinationCategoryDropdown);
        $(".source-destination-container").append(destinationDropdown);
        $(".info-container").append(sourceDropdown);
    } else if ( type == 'expense' ) {
        $(".source-destination-container").append(sourceCategoryDropdown);
        $(".source-destination-container").append(sourceDropdown);
        $(".source-destination-container").append(destinationCategoryDropdown);
        $(".info-container").append(destinationDropdown);
        $(".info-container").append(folderSelection);
    } else if ( type == 'transfer') {
        $(".source-destination-container").append(sourceCategoryDropdown);
        $(".source-destination-container").append(sourceDropdown);
        $(".source-destination-container").append(destinationCategoryDropdown);
        $(".source-destination-container").append(destinationDropdown);
    }

}

function resetCurrencyDropdowns(elem) {
    var sourceCurrency =$('select[name=source_currency]');
    var destinationCurrency =$('select[name=destination_currency]');
    if($(elem).hasClass('select-source')) {
        sourceCurrency.find('option').attr("disabled",false);
    } else if ($(elem).hasClass('select-destination')) {
        destinationCurrency.find('option').attr("disabled",false);
    } else {
        sourceCurrency.find('option').attr("disabled",false);
        destinationCurrency.find('option').attr("disabled",false);
    }
}

/**
 * Update source and destination options based on type
 * @param type
 */
function updateSourceDestinationDropdowns(type) {
    var options = getDropdownOptionsByType(type);
    var sourceDropdown =$('select[name=source]');
    var sourceCategoryDropdown =$('select[name=source-category]');
    var destinationDropdown = $('select[name=destination]');
    var destinationCategoryDropdown =$('select[name=destination-category]');
    // Reset to default
    resetDropdownsToDefault(type);

    // SOURCE OPTIONS
    if(type == 'income') {
        sourceCategoryDropdown.removeClass("hidden");
        //sourceDropdown.addClass("hidden");
        var sGroupOptions = _.chain(options.source).groupBy('category_slug').map(function(value, key) {
            return {
                category: key,
                options: value
            }
        }).value();

        $.each(sGroupOptions, function (i, groupOption) {
            sourceCategoryDropdown.append($('<option>', {
                value: groupOption.category,
                text : capitalizeFirstLetter(groupOption.category)
            }));
        });
    } else {
        $.each(options.source, function (i, option) {
            var optionName = option.name;
            if(option.currency != "" && !option.is_parent) {
                optionName += " ("+ option.currency +")";
            }
            sourceDropdown.append($('<option>', {
                value: option.id,
                text : optionName
            }));
        });
    }
    // DESTINATION OPTIONS
    if (type == 'expense') {
        destinationCategoryDropdown.removeClass("hidden");
        //destinationDropdown.addClass("hidden");
        var dGroupOptions = _.chain(options.destination).groupBy('category_slug').map(function(value, key) {
            return {
                category: key,
                options: value
            }
        }).value();

        $.each(dGroupOptions, function (i, groupOption) {
            destinationCategoryDropdown.append($('<option>', {
                value: groupOption.category,
                text : capitalizeFirstLetter(groupOption.category.replace(/_/g, ' '))
            }));
        });
    } else {
        $.each(options.destination, function (i, option) {
            var optionName = option.name;
            if(option.currency != "" && !option.is_parent) {
                optionName += " ("+ option.currency +")";
            }
            destinationDropdown.append($('<option>', {
                value: option.id,
                text : optionName
            }));
        });
    }

}

/**
 * Sync source and destination dropdowns ( disable sending to same entites )
 * @param elem
 */
function disableSourceDestination(elem) {
    var sourceDropdown =$('select.select-destination');
    var destinationDropdown = $('select.select-source');
    // reset
    sourceDropdown.find('option').attr('disabled',false);
    destinationDropdown.find('option').attr('disabled',false);
    var value = $(elem).val();
    if(value == "") {
        return;
    }
    // update
    if($(elem).hasClass('select-source')) {
        sourceDropdown.find('option[value="'+value+'"]').attr("disabled",true);
    } else if ($(elem).hasClass('select-destination')) {
        destinationDropdown.find('option[value="'+value+'"]').attr("disabled",true);
    }

}

function showStatement(elem) {
    var selected = elem.value;
    var displayStatementOptions = _.filter(dropdownOptions, function (o) { return o.display_statement == 1; });
    var displayStatement = false;

    for(var i=0; i<displayStatementOptions.length; i++){
        if(displayStatementOptions[i].id == selected){
            // display statement
            displayStatement = true;
        }
    }

    if(displayStatement){
        $('.statement-container').show();
    }else{
        $('.statement-container input').val('');
        $('.statement-container').hide();
    }
}

// UPDATE CURRENCY DROPDOWN IF ACCOUNT
function updateCurrencyDropdown(elem) {
    var entityId = $(elem).val();
    resetCurrencyDropdowns(elem);
    var entity = _.find(dropdownOptions, function(obj){
        return obj.id == entityId && obj.category_slug == "account";
    });
    if(!entity) { return; }

    if(entity.currency != "") {
        if(transactionType == "transfer") {
            if($(elem).hasClass('select-source')) {
                $('.select-source-currency').find('option').attr("disabled",true);
                $('.select-source-currency').find('option[value="'+entity.currency.toLowerCase()+'"]').attr("disabled",false);
                $('.select-source-currency').val(entity.currency.toLowerCase());
            } else if ($(elem).hasClass('select-destination')) {
                $('.select-destination-currency').find('option').attr("disabled",true);
                $('.select-destination-currency').find('option[value="'+entity.currency.toLowerCase()+'"]').attr("disabled",false);
                $('.select-destination-currency').val(entity.currency.toLowerCase());
            }
        } else if(transactionType == "income") {
            if($(elem).hasClass('select-destination')) {
                $('.select-destination-currency').find('option').attr("disabled",true);
                $('.select-destination-currency').find('option[value="'+entity.currency.toLowerCase()+'"]').attr("disabled",false);
                $('.select-destination-currency').val(entity.currency.toLowerCase());
            }
        } else if (transactionType == "expense") {
            if($(elem).hasClass('select-source')) {
                $('.select-source-currency').find('option').attr("disabled",true);
                $('.select-source-currency').find('option[value="'+entity.currency.toLowerCase()+'"]').attr("disabled",false);
                $('.select-source-currency').val(entity.currency.toLowerCase());
            }
        }
    }
}

/**
 * Update source based on transaction type and source category
 * @param elem
 */
function updateSourceOptions(elem) {
    var sourceDropdown =$('select.select-source');
    sourceDropdown.find('option').remove();
    sourceDropdown.append($('<option>', {
        label: "Source"
    }));

    var category = $(elem).val();
    var sourceOptions  = _.filter(dropdownOptions, function (o) { return o.category_slug == category; });

    $.each(sourceOptions, function (i, option) {
        var optionName = option.name;
        if(option.currency != "" && !option.is_parent) {
            optionName += " ("+ option.currency +")";
        }
        sourceDropdown.append($('<option>', {
            value: option.id,
            text : optionName
        }));
    });

    // COPY TO INFO PART
    $(".info-container").html("");
    $(".info-container").append(sourceDropdown);

}

/**
 * Called on transfer when destination is changed
 * @param elem
 */
function transferDestinationChanged(elem){

    if($(elem).attr('name')=='destination-child'){
        return
    }

    var id = $(elem).val();
    var sourceOptions  = _.filter(dropdownOptions, function (o) { return o.parent == id; });
    $('select.select-destination-child').remove();

    if (sourceOptions.length == 0){
        return;
    }

    var destinationDropdown =$('select.select-destination').clone();
    destinationDropdown.addClass('select-destination-child');
    destinationDropdown.removeClass('select-destination');
    destinationDropdown.attr('name','destination-child');
    destinationDropdown.find('option').remove();
    destinationDropdown.append($('<option>', {
        label: "Destination"
    }));

    $.each(sourceOptions, function (i, option) {
        var optionName = option.name;
        if(option.currency != "" && !option.is_parent) {
            optionName += " ("+ option.currency +")";
        }
        destinationDropdown.append($('<option>', {
            value: option.id,
            text : optionName
        }));
    });

    // COPY TO INFO PART

    if($('select[name=type]').val() !='expense' && $('select[name=type]').val() !='income'){
        $(".info-container").html("");
    }
    $(".info-container").append(destinationDropdown);
}

/**
 * Update destination based on transaction type and destination category
 * @param elem
 */
function updateDestinationOptions(elem) {
    var destinationDropdown =$('select.select-destination');
    destinationDropdown.find('option').remove();
    destinationDropdown.append($('<option>', {
        label: "Destination"
    }));

    var category = $(elem).val();
    var sourceOptions  = _.filter(dropdownOptions, function (o) { return o.category_slug == category; });

    $.each(sourceOptions, function (i, option) {
        var optionName = option.name;
        if(option.currency != "" && !option.is_parent) {
            optionName += " ("+ option.currency +")";
        }
        destinationDropdown.append($('<option>', {
            value: option.id,
            text : optionName
        }));
    });

    if(category == 'shoot'){
        var folderSelectionDropdown =$('select.select-destination').clone();
        folderSelectionDropdown.addClass('select-transaction-folder');
        folderSelectionDropdown.removeAttr("onchange");
        folderSelectionDropdown.removeClass('select-destination');
        folderSelectionDropdown.attr('name','transaction_folder');
        folderSelectionDropdown.find('option').remove();

        folderSelectionDropdown.append($('<option>', {
            label: "Select Shooting"
        }));

        $.each(shootings, function (i, option) {
            folderSelectionDropdown.append($('<option>', {
                value: option.id,
                text : option.name
            }));
        });

        $(folderSelectionDropdown).removeClass('hidden');
        $('select.select-transaction-destination').remove();
    } else if(category=='studio'){
        var studioSelectionDropdown =$('select.select-destination').clone();
        studioSelectionDropdown.addClass('select-studio');
        studioSelectionDropdown.removeClass('select-destination');
        studioSelectionDropdown.attr('name','transaction_studio');
        studioSelectionDropdown.find('option').remove();

        studioSelectionDropdown.append($('<option>', {
            label: "Select Studio"
        }));
        studioSelectionDropdown.append($('<option>', {
            value: 'every',
            text : 'Every'
        }));

        $.each(studios, function (i, option) {
            studioSelectionDropdown.append($('<option>', {
                value: option.id,
                text : option.name
            }));
        });

        $(studioSelectionDropdown).removeClass('hidden');
        $('select.select-transaction-folder').remove();
    }else if(category=='salary'){
        var studioUserSalaryDropdown =$('select.select-destination').clone();
        studioUserSalaryDropdown.addClass('select-user-salary');
        studioUserSalaryDropdown.removeClass('select-destination');
        studioUserSalaryDropdown.attr('name','transaction_user_salary');
        studioUserSalaryDropdown.find('option').remove();

        studioUserSalaryDropdown.append($('<option>', {
            label: "Select User"
        }));

        $.each(usersSalary, function (i, option) {
            studioUserSalaryDropdown.append($('<option>', {
                value: option.id,
                text : option.name
            }));
        });

        $(studioUserSalaryDropdown).removeClass('hidden');
        $('select.select-transaction-folder').remove();
    } else{
        $('select.select-transaction-folder').remove();
        $('select.select-transaction-destination').remove();
    }

    // COPY TO INFO PART
    $(".info-container").html("");
    $(".info-container").append(destinationDropdown);
    if(folderSelectionDropdown){
        $(".info-container").append(folderSelectionDropdown);
    } else if(studioSelectionDropdown){
        $(".info-container").append(studioSelectionDropdown);
    }else if(studioUserSalaryDropdown){
        $(".info-container").append(studioUserSalaryDropdown);
    }
}

/**
 * Submit new transaction
 */
function submitNewTransaction(event,form) {
    event.preventDefault();
    var url = "/finance/ajax";
    var data = getFormResults(form);
    data.action = "new-transaction";
    startLoading();

    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "json",
        method: "POST"
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            openWarningModal({message:response.message, status: 0 });
            $(':submit').removeAttr('disabled');
        }
    });
}

/*Display list of agencies*/
function transactionEntityCategoryChanged(selected){
    if(!selected){
        selected = $('select[name=category] :selected');
    } else{
        selected = selected.selectedOptions[0];
    }
    if($(selected).data('slug') == 'agency'){
        $('#dropdown-currency').addClass('hidden');
        $('#dropdown-agency').removeClass('hidden');
        $('#dropdown-shoot').addClass('hidden');
    } else if($(selected).data('slug') == 'account'){
        $('#dropdown-currency').removeClass('hidden');
    }else if($(selected).data('slug') == 'shoot'){
        $('#dropdown-currency').addClass('hidden');
        $('#dropdown-agency').addClass('hidden');
        $('#dropdown-shoot').removeClass('hidden');
    } else{
        $('#dropdown-currency').addClass('hidden');
        $('#dropdown-agency').addClass('hidden');
        $('#dropdown-shoot').addClass('hidden');
    }

    /*Change parents*/
	populateParentByCategory(selected);
}

function populateParentByCategory(selected){
	if(!selected){
		var selected = $('select[name=category] :selected');
		if(!selected){
			selected = $('select[name=category] :first');
		}
		var categoryId = selected.val();
    }else{
		var categoryId = selected.value;
	}
	var parentSelect = $("select[name='parent_selected']");
	parentSelect.find("option").remove();
	parentSelect.append($('<option></option>').html("No Parent"));
    for(var i = 0; i < parentEntities.length; i++){
        if(parentEntities[i].category == categoryId){
            parentSelect.append($('<option></option>').val(parentEntities[i].value).html(parentEntities[i].name));
        }
    }
}

/*Display list of parents*/
function parentChanged(selected){
    if ($(selected).is(":checked")){
        $('#dropdown-parent').addClass('hidden');
        $('#dropdown-currency').addClass('hidden');
    } else{
        transactionEntityCategoryChanged();
        if($('#dropdown-parent > select > option').length !=0){
            $('#dropdown-parent').removeClass('hidden');
        }
    }
}

/*All balances*/
function financeAccountSelected(checkbox) {
    var url = "/finance/balances";
    var query = "?&ids=";
    if($('input:checkbox:checked').length > 0){
        for (i=0; i< $('input:checkbox:checked').length; i++){
            if (i!=0){
                query += ','
            }
            query += $('input:checkbox:checked')[i].value;
        }
    }
    var sort = $( "#search-sort" ).val();
    if(sort != "") {
        query += "&sort="+sort;
    }
    window.history.pushState("object or string", "Title", url+query);
    $.ajax({
        url: url+query,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('#table-transactions').replaceWith(response);
    });
}

/**
 * Get the value of a querystring
 * @param  {String} field The field to get the value of
 * @return {String}       The field value
 */
var getQueryString = function ( field ) {
    var href = window.location.href;
    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
    var string = reg.exec(href);
    return string ? string[1] : null;
};


function updateTransactionStatus(status, transactionId) {
    var url = "/finance/update/";
    var query = ""+transactionId+"/"+status+"";
    var selectQuery = "&ids=";
    var page = "?&page=" + getQueryString('page');
    if($('input:checkbox:checked').length > 0){
        for (i=0; i< $('input:checkbox:checked').length; i++){
            if (i!=0){
                selectQuery += ','
            }
            selectQuery += $('input:checkbox:checked')[i].value;
        }
    }
    $.ajax({
        url: url+query+page+selectQuery,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        response = JSON.parse(response);
        if(response.status){
            $('#table-transactions #transaction-' + transactionId).find('.approve-btn').remove();
        }else{
            console.log('Error');
        }
        // $('#table-transactions #transaction-' + transactionId).replaceWith(response);
    });
}

function deleteTransaction(transactionId) {
    var url = "/finance/delete/";
    var query = ""+transactionId+"";
    var selectQuery = "&ids=";
    var page = "?&page=" + getQueryString('page');
    if($('input:checkbox:checked').length > 0){
        for (i=0; i< $('input:checkbox:checked').length; i++){
            if (i!=0){
                selectQuery += ','
            }
            selectQuery += $('input:checkbox:checked')[i].value;
        }
    }
    $.ajax({
        url: url+query+page+selectQuery,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('#table-transactions #transaction-' + transactionId).remove();
    });
}

function transactionSort() {
    var url = "/finance/balances";
    var query = "?";
    if($('input:checkbox:checked').length > 0){
        query += "&ids="
        for (i=0; i< $('input:checkbox:checked').length; i++){
            if (i!=0){
                query += ','
            }
            query += $('input:checkbox:checked')[i].value;
        }
    }
    var sort = $( "#search-sort" ).val();
    if(sort != "") {
        query += "&sort="+sort;
    }
    window.history.pushState("object or string", "Title", url+query);
    $.ajax({
        url: url+query,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('#table-transactions').replaceWith(response);
    });
}