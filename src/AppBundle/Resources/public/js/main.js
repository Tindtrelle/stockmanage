
$(document).ready(function(){
    /* Load tooltips */
    bindTooltips();
});
$(".dropdown-toggle").dropdown();

$('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayBtn: true,
    todayHighlight: true,
    orientation: "top"
});

/*Hide alert on page change*/

$(document).ready (function(){
    setTimeout(function(){
        $(".alert").remove();
    }, 800);

});


/**
 * Not used at the moment
 */
function loadImages() {
    var images = document.images;
    for(var i = 0; i < images.length; i++) {
        if($(images[i]).hasClass("load-async")) {
            var downloading_image = new Image();
            var imgId = $(images[i]).attr("id");
            window[imgId] = $(images[i]);
            console.log(imgId);

            downloading_image.onload = function(){
                console.log(this.src);
                window[$(this).attr("id")].attr("src",this.src)
            };

            downloading_image.src = window[imgId].attr("data-src");
            downloading_image.id = window[imgId].attr("id");
        }
    }
}

function bindTooltips() {
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'top'
    });
}

function getFormResults(form) {
    var formElements = form.elements;
    var formParams = {};
    var elem = null;
    for (var i = 0; i < formElements.length; i += 1) {
        elem = formElements[i];
        switch (elem.type) {
            case 'submit':
                break;
            case 'radio':
                if (elem.checked) {
                    formParams[elem.name] = elem.value;
                }
                break;
            case 'checkbox':
                if (elem.checked) {
                    formParams[elem.name] = elem.value;
                }
                break;
            case 'text':
                if(elem.value != ""){
                    formParams[elem.name] = elem.value;
                }
                break;
            default:
                formParams[elem.name] = elem.value
        }
    }
    return formParams;
}

function showPageNotification(message) {
    $('.page-notifications').removeClass("hidden");
    $('.page-notifications .notification-content').html(message);
    setTimeout(function(){ $('.page-notifications').addClass("hidden"); }, 3000);
}

/* Loading screen */
function startLoading() {
    $("#s-loading").removeClass("hidden");
}

function endLoading() {
    $("#s-loading").addClass("hidden");
    $(".loading-caption").html("");
    $(".loading-description").html("");
    $(".loading-description-2").html("")
}

function showTmpLoadingDescription(content) {
    $(".loading-description").html(content);
    setTimeout(function(){
        $(".loading-description").html("");
    }, 10000);

}

/* SIGN UP */
function submitSignUpForm(event,form) {
    event.preventDefault();
    var url = "/user/signup/ajax";
    var data = getFormResults(form);
    startLoading();
    
    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "json",
        method: "POST"
    }).done(function(response) {
        endLoading();
        if(response.status) { // LOGIN & REDIRECT
            if(response.data.redirect) {
                window.location.href = window.location.origin + "/"+response.data.redirect;
            } else {
                location.reload();
            }
        } else { // ERRORS
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

/* LOGIN */
function submitLoginForm(event,form) {
    event.preventDefault();
    var url = "/user/login/ajax";
    var data = getFormResults(form);
    startLoading();

    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "json",
        method: "POST"
    }).done(function(response) {
        endLoading();
        if(response.status) { // LOGIN & REDIRECT
            if(response.data.redirect) {
                window.location.href = window.location.origin + "/"+response.data.redirect;
            } else {
                location.reload();
            }
        } else { // ERRORS
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

/* Warning/error modal */
function openWarningModal(data) {
    var title;
    var message;

    if(typeof(data) == 'undefined') {
        title = "Something went wrong";
        message = "Something happen. Please try again.";
    } else {
        if(data.status) {
            title = "Successful!";
        } else {
            title = "Something went wrong";
        }

        if(data.title) {
            title = data.title;
        }

        if(!data.message) {
            message = "Something happen. Please try again.";
        } else {
            message = data.message;
        }
    }

    $("#warning-modal #warning-message").html(message);
    $("#warning-modal #warning-title").html(title);
    $("#warning-modal").modal('show');
}

// Add zero in front of day or month
function formatDateString(str) {
    if(String(str).length < 2) {
        str = "0"+str;
    }
    return str;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * FILTER DROPDOWN START
 */
$('ul.dropdown-menu').on('click', function(event){
    event.stopPropagation();
});

// $('#dropdown-filter').on('hidden.bs.dropdown', function () {
//     $(".action-icon.filter-icon").removeClass("active");
//     $(".action-icon.filter-icon").attr('src', '/bundles/app/images/filters.png');
// });

$('body').on('click', function (event) {
    var closeFilters = true;
    $(event.target).parents().each(function( index ) {
        if($( this ).hasClass("datepicker-dropdown")) {
            closeFilters = false;
        }
    });
    if($(event.target).hasClass("day")) {
        closeFilters = false;
    }

    if(!closeFilters) {
        event.stopPropagation();
    }

});
/**
 * FILTER DROPDOWN END
 */
/**
 * CTRL / SHIFT PRESSED
 * @type {boolean}
 */
var shifted = false;
$(document).on('keyup keydown',
    function(e){
        shifted = e.shiftKey;
    });

var ctrled = false;
$(document).on('keyup keydown',
    function(e){
        if(e.ctrlKey || e.metaKey) {
            ctrled = true;
        } else {
            ctrled = false;
        }
    });

/*GALLERY START*/
/*GRID*/
$('.img-container').on( "dblclick", function(event){

    var imgSrc = $(this).find('img').attr('src'),
        imgTitle = $(this).find('img').attr('alt');

    if(imgSrc){
        $.fancybox.open({href : imgSrc});
    }
});
/*LIST*/
$('.img-content').on( "dblclick", function(event){

    var imgSrc = $(this).find('img').attr('src'),
        imgTitle = $(this).find('img').attr('alt');

    if(imgSrc){
        $.fancybox.open({href : imgSrc});
    }
});
/*GALLERY END*/

/* Comma separator for form input fields */
function formatThousands() {
    var values = $(this).val().toString().split('.');
    $(this).val( values[0].replace(/.(?=(?:.{3})+$)/g, '$&,') + ( values.length == 2 ? '.' + values[1] : '' ) );
    if($(this).val() != "" && $(this).val().toString().indexOf(".") < 0) {
        $(this).val( $(this).val() + ".00" );
    }
}

function deFormatThousands() {
    var values = $(this).val().toString().split('.');
    if(values.length == 2){
        if(values[1] == "00"){
            $(this).val(values[0]);
        }else{
            $(this).val(values[0] + "." + values[1]);
        }
    }
    $(this).val($(this).val().replace(",", ""));
}

$('#source_amount').click(function() {
    this.focus();
    this.setSelectionRange(0, 0);
});
$('#destination_amount').click(function() {
    this.focus();
    this.setSelectionRange(0, 0);
});

jQuery.fn.forceNumericOnly = function() {
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};
