<?php

namespace AppBundle\Controller;

use AppBundle\Entity\File;
use AppBundle\Entity\Folder;
use AppBundle\Entity\TmpData;
use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FolderController extends Controller
{
    /**
     * @Route("/folders", name="folder.list")
     */
    public function folderListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if(!$user) {
            return $this->redirectToRoute('homepage');
        }

        $query = $request->query->all();
        $query["company_id"] = $user->getCompany()->getId();
        $folders = $em->getRepository('AppBundle:Folder')->getByParams($query);
        $folderStatus = $em->getRepository('AppBundle:FolderStatus')->findAll();
        $members = $em->getRepository('AppBundle:User')->findBy(['company'=>$user->getCompany()->getId()]);
        $collections = $this->get("s_folder")->findAllCollections(['company_id'=>$user->getCompany()->getId()]);
        foreach($folders as $key=>$folder) {
            $countFiles = $em->getRepository('AppBundle:File')->countByParams(["folder_id"=>$folder->getId()]);
            $countVideoFiles = $em->getRepository('AppBundle:File')->countByParams(["folder_id"=>$folder->getId(), "video"=>1]);
            $folder->setFiles($countFiles);
            $folder->setVideoFiles($countVideoFiles);
            $folders[$key] = $this->get("s_folder")->getFolderAdditionalData($folder,['folder-users']);
        }

        if($request->isXmlHttpRequest()) { // AJAX
            return $this->render('AppBundle::Folder/foldersList.html.twig', array('folders'=>$folders, "members"=>$members, "collections"=>$collections, 'folderStatus'=>$folderStatus));
        } else {
            return $this->render('AppBundle::Folder/folders.html.twig', array('folders'=>$folders, "members"=>$members, "collections"=>$collections, 'folderStatus'=>$folderStatus));
        }
    }

    /**
     * @Route("/folders/view/{id}", name="folder.view")
     */
    public function folderViewAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if(!$user) {
            return $this->redirectToRoute('homepage');
        }
        $folder = $em->getRepository('AppBundle:Folder')->find($id);
        $members = $em->getRepository('AppBundle:User')->findBy(['company'=>$user->getCompany()->getId()]);
        $collections = $this->get("s_folder")->findAllCollections(['company'=>$user->getCompany()->getId()]);

        // SEARCH QUERY
        $query = $request->query->all();
        $query["folder_id"] = $folder->getId();
        $query["company_id"] = $user->getCompany()->getId();
        $query["name"] = empty($request->query->get("name")) ? null : $request->query->get("name");
        $files = $em->getRepository('AppBundle:File')->getByParams($query);

        if($request->isXmlHttpRequest()) { // AJAX
            return $this->render('AppBundle::Folder/filesList.html.twig', array('folder'=>$folder,'files'=>$files,'members'=>$members,'collections'=>$collections));
        } else {
            return $this->render('AppBundle::Folder/folderView.html.twig', array('folder'=>$folder,'files'=>$files,'members'=>$members,'collections'=>$collections));
        }

    }
    /**
     * @Route("/folders/info/{id}", name="folder.info")
     */
    public function folderInfoAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        $folderService = $this->get("s_folder");
        if(!$user) {
            return $this->redirectToRoute('homepage');
        }
        $folder = $em->getRepository('AppBundle:Folder')->find($id);
        $folder = $folderService->getTransactions($folder);

        return $this->render('AppBundle::Folder/folderInfo.html.twig', array(
            'shoot'=>$folder,
        ));

    }

    /**
     * @Route("/folders/sync/{id}", name="folder.sync")
     */
    public function folderSyncAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if(!$user) {
            return $this->redirectToRoute('homepage');
        }
        $folder = $em->getRepository('AppBundle:Folder')->find($id);
        $query["folder_id"] = $folder->getId();
        $files = $em->getRepository('AppBundle:File')->getByParams($query);
        foreach($files as $file) {
            $file = $this->container->get('s_image')->setImageUsersFromName($file);
            $file = $this->container->get('s_image')->setImageDateFromName($file);
            $em->persist($file);
            // CREATE DEFAULT AGENCY STATUS
            $this->container->get("s_file")->setDefaultAgencyStatus($file);
        }
        $em->flush();

        return $this->redirectToRoute("folder.view",array("id"=>$folder->getId()));
    }

    /**
     * @Route("/folders/ajax", name="folder.ajax", defaults={"_format" = "json"})
     * @Extra\Method("POST")
     */
    public function folderAjaxAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if(!$user) {
            throw $this->createNotFoundException('You are not logged in.');
        }
        
        $action = $request->request->get('action');
        if(!$action){
            return $this->get("s_controller")->createResponse(false,"There is no action defined - ".$action);
        }
        // FOLDER CREATE
        if($action == 'folder-create') {
            $id = $request->request->get('id');
            $name = $request->request->get('name');
            $description = $request->request->get('description');
            $collection = $request->request->get('collection');
            $status_id = $request->request->get('status');
            $newFolder = false;
            if(empty($name)) {
                return $this->get("s_controller")->createResponse(false,"Title is empty");
            }
            if(!empty($status_id)){
                $status = $em->getRepository("AppBundle:FolderStatus")->find($status_id);
            }
            if(empty($id)) {
                $folder = new Folder();
                $folder->setDateCreated(new \DateTime());
                $newFolder = true;
            } else {
                $folder = $em->getRepository("AppBundle:Folder")->find($id);
            }
            $folder->setName($name);
            $folder = $this->get("s_folder")->setFolderDateFromName($folder);
            $folder->setDescription($description);
            $folder->setCollection($collection);
            $folder->setOwner($user);
            $folder->setStatus($status);

            if($user->getCompany()) {
                $folder->setCompany($user->getCompany());
            }
            $em->persist($folder);
            $em->flush();
            // INSERT LOG
            if($newFolder) {
                $this->container->get("s_log")->insertUserLog($user,["entityName"=>"folder","entityId"=>$folder->getId(),"action"=>"create"]);
            } else {
                $this->container->get("s_log")->insertUserLog($user,["entityName"=>"folder","entityId"=>$folder->getId(),"action"=>"update"]);
            }

            return $this->get("s_controller")->createResponse(true,"Success");

        }
        // CHECK IF FILES EXISTS
        if($action == 'validate-file-exists') {
            $files = array();
            $filesCount =  $request->request->get('files_number');
            for($i = 0; $i < $filesCount ; $i++) {
                $files[] = $request->request->get('file_'.$i);
            }
            // Check if exists
            $existingFiles = array();
            foreach($files as $filename) {
                if(!$this->container->get("s_image")->isUniqueFileNameByName($filename)) {
                    $existingFiles[] = $filename;
                }
            }
            $existingFilesStr = implode(",",$existingFiles);
            return $this->get("s_controller")->createResponse(true,"Success",["existing_files"=>$existingFilesStr]);
        }
        // UPLOAD FILES
        if($action == 'files-upload') {
            $files = array();
            $filesCount =  $request->request->get('files_number');
            $folderName = $request->request->get('folder_name');
            $folderId = $request->request->get('folder_id');
            $uploadProgressKey = $request->request->get('upload_progress_key');
            $folder = $em->getRepository("AppBundle:Folder")->find($folderId);
            if(!$folder) {
                return $this->get("s_controller")->createResponse(false,"Folder not found");
            }

            for($i = 0; $i < $filesCount ; $i++) {
                $files[] = $request->files->get('file_'.$i);
            }
            $awsService = $this->get('s_aws');
            $awsService->createAwsClient();

            session_write_close();

            $result = $awsService->uploadMultipleFiles($files, 
                array('folder_id'=>$folderId,'folder_name' => $folderName,'upload_progress'=>null,"currentUser"=>$user),
                array("resize"=>300));
            if($result instanceof AwsException) { // IF ERROR
                return $this->get("s_controller")->createResponse(false,"Failed",$result->getMessage());
            }

            session_start();
            return $this->get("s_controller")->createResponse(true,"Success",["upload_info"=>$awsService->getUploadInfo()]);
        }
        // GET UPLOAD PROGRESS
        if($action == 'upload-progress') {
            $uploadProgressKey = $request->request->get('upload_progress_key');
            $uploadProgress = $em->getRepository("AppBundle:TmpData")->findOneByName($uploadProgressKey);
            if($uploadProgress) {
                return $this->get("s_controller")->createResponse(true,"Upload progress",$uploadProgress->getValue());
            }
        }
        //MARK AS VIDEO
        if($action == 'mark-as-video'){
            $filesStr = $request->request->get("files");
            $filesIds = explode(",",$filesStr);
            $files = $em->getRepository("AppBundle:File")->getByParams(['ids'=>$filesIds]);
            foreach($files as $file) {
                // Insert log
                $this->container->get("s_log")->insertUserLog($user,["entityName"=>"file","entityId"=>$file->getId(),"action"=>"mark-as-video"]);
                //set as video
                $file->setVideo(true);
            }
            $em->flush();
            return $this->get("s_controller")->createResponse(true,"Files successful removed");
        }
        // DELETE FILES
        if($action == 'files-delete') {
            $imageService = $this->get("s_image");
            $awsService = $this->get('s_aws');
            $awsService->createAwsClient();

            $filesStr = $request->request->get("files");
            $filesIds = explode(",",$filesStr);
            $files = $em->getRepository("AppBundle:File")->getByParams(['ids'=>$filesIds]);
            foreach($files as $file) {
                // Insert log
                $this->container->get("s_log")->insertUserLog($user,["entityName"=>"file","entityId"=>$file->getId(),"action"=>"delete"]);
                // REMOVE ORIGINAL FILE
                $awsService->deleteFile($file);
                $em->remove($file);
                // REMOVE THUMBS
                $awsService->deleteFileByKey($imageService->getImageThumbKey($file));
            }
            $em->flush();
            return $this->get("s_controller")->createResponse(true,"Files successful removed");

        }
        // DELETE FOLDER
        if($action == 'folders-delete') {
            $foldersStr = $request->request->get("folders");
            $foldersIds = explode(",",$foldersStr);
            $folders = $em->getRepository("AppBundle:Folder")->getByParams(['ids'=>$foldersIds]);
            foreach($folders as $folder) {
                $countFiles = $em->getRepository('AppBundle:File')->countByParams(["folder_id"=>$folder->getId()]);
                if(!$countFiles) {
                    // Insert log
                    $this->container->get("s_log")->insertUserLog($user,["entityName"=>"folder","entityId"=>$folder->getId(),"action"=>"delete"]);
                    $em->remove($folder);
                }
            }
            $em->flush();
            return $this->get("s_controller")->createResponse(true,"Folders successful removed");

        }


        return $this->get("s_controller")->createResponse(false,"What should I do? - ".$action);
    }

    /**
     * @Route("/file/download", name="file.download")
     */
    public function fileDownloadAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $filesStr = $request->query->get("id");
        $filesArr = explode(",",$filesStr);
        $awsService = $this->get('s_aws');
        $awsService->createAwsClient();
        // DOWNLOAD 1 FILE  
        if(count($filesArr) == 1) {
            $file = $em->getRepository("AppBundle:File")->find($filesArr[0]);
            if(!$file) {
                throw new NotFoundHttpException("File not found");
            }
            $filePath = $awsService->downloadFile($file);
        } else { // ARCHIVE & DOWNLOAD MORE FILES
            $filesToDownload = [];
            foreach($filesArr as $fileId) {
                $file = $em->getRepository("AppBundle:File")->find($fileId);
                if(!$file) { continue; }
                $filesToDownload[] = $file;
            }
            $tmpFolderName = $awsService->downloadFiles($filesToDownload);
            $zipName = $tmpFolderName.'.tar.gz';
            $tmpFolderPath = "uploads/".$zipName;
            $zipPath = "uploads/".$zipName;
            $output = shell_exec("cd uploads/ && tar -zcvf  ".$zipName." ".$tmpFolderName."/ 2>&1");
            //var_dump($output);
            $filePath = $zipPath;
        }
        // SET DOWNLOAD RESPONSE
        if (!file_exists($filePath)) {
            throw new NotFoundHttpException("File not found");
        }

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header('Content-Disposition: attachment; filename="'.basename($filePath).'"');
        ob_clean(); flush();
        readfile($filePath);

        ignore_user_abort(true);
        // Cleanup
        if (file_exists($filePath)) {
            unlink($filePath);
        }
        if(isset($tmpFolderPath)) {
            if(file_exists($tmpFolderPath)) {
                unlink($tmpFolderPath);
            }
        }
        exit;
    }

    /**
     * @Route("/folder/download", name="folder.download")
     */
    public function folderDownloadAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $folderStr = $request->query->get("id");
        $folderArr = explode(",",$folderStr);
        $awsService = $this->get('s_aws');
        $awsService->createAwsClient();

        if(count($folderArr) != 1) {
            throw new NotFoundHttpException("Only 1 Folder can be donwnloaded");
        }
        // DOWNLOAD 1 FOLDER
        $folder = $em->getRepository("AppBundle:Folder")->find($folderArr[0]);
        if (!$folder) {
            throw new NotFoundHttpException("Folder not found");
        }
        $query["folder_id"] = $folder->getId();
        $filesToDownload = $em->getRepository('AppBundle:File')->getByParams($query);
        $tmpFolderName = $awsService->downloadFiles($filesToDownload);
        $zipName = $tmpFolderName.'.tar.gz';
        $tmpFolderPath = "uploads/".$zipName;
        $zipPath = "uploads/".$zipName;
        $output = shell_exec("cd uploads/ && tar -zcvf  ".$zipName." ".$tmpFolderName."/ 2>&1");

        $filePath = $zipPath;
        // SET DOWNLOAD RESPONSE
        // Cleanup
        if (!file_exists($filePath)) {
            throw new NotFoundHttpException("File not found");
        }

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header('Content-Disposition: attachment; filename="' . basename($filePath) . '"');
        ob_clean();
        flush();
        readfile($filePath);
        // Cleanup
        if(isset($tmpFolderPath)) {
            if(file_exists($tmpFolderPath)) {
                unlink($tmpFolderPath);
            }
        }
        exit;
    }

    /**
     * TMP ROUTE: move all files in single folder
     * @Route("/folders/move", name="folder.move")
     */
    public function folderMoveAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if(!$user) {
            return $this->redirectToRoute('homepage');
        }
        $awsService = $this->get('s_aws');
        $awsService->createAwsClient();
        $files = $em->getRepository('AppBundle:File')->findAll();

        foreach($files as $key=>$file) {
            $awsService->moveFile($file);
            if($key > 9 ) {
                break;
            }
        }

        return $this->redirectToRoute('folder.list');
    }
}
