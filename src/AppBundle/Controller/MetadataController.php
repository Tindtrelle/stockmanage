<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Agency;
use AppBundle\Entity\File;
use AppBundle\Entity\FileAgencyStatus;
use AppBundle\Entity\FileMetaUpdate;
use AppBundle\Entity\Folder;
use AppBundle\Entity\TmpData;
use AppBundle\Entity\Keywords;
use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MetadataController extends Controller
{
    /**
     * @Route("/metadata", name="metadata.list")
     */
    public function metadataListAction(Request $request)
    {
    	/** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('homepage');
        }
        $members = $em->getRepository('AppBundle:User')->findBy(['company' => $user->getCompany()->getId()]);
        $collections = $this->get("s_folder")->findAllCollections(['company' => $user->getCompany()->getId()]);
        $agencies = $em->getRepository('AppBundle:UserAgency')->findBy(['user' => $user, 'hidden' => false], ['orderNumber' => 'ASC']);
        $folders = $em->getRepository('AppBundle:Folder')->findBy(['company' => $user->getCompany()->getId()]);

	    // SEARCH QUERY
        $query = $request->query->all();
        $query["company_id"] = $user->getCompany()->getId();
        $query["name"] = empty($request->query->get("name")) ? null : $request->query->get("name");

        // PAGINATION START
        $paginationService = $this->get('s_pagination');
        $paginationService->setLimit(50);
        $paginationService->setCurrentPage($request->query->get('page'));
        $query['limit'] = $paginationService->getLimit();
        $query['offset'] = $paginationService->countOffset($paginationService->getCurrentPage());

        $currentPage = $paginationService->getCurrentPage();
        $totalFiles = $em->getRepository('AppBundle:File')->countByParams($query);
        $totalPages = $paginationService->countTotalPages($totalFiles);
        // PAGINATION END

        $files = $em->getRepository('AppBundle:File')->getByParams($query);

        if ($request->isXmlHttpRequest()) { // AJAX
            return $this->render('AppBundle::Metadata/filesList.html.twig',
                array(
                    'files' => $files,
                    'members' => $members,
                    'folders' => $folders,
                    'collections' => $collections,
                    'agencies' => $agencies,
                    'currentPage' => $currentPage,
                    'totalPages' => $totalPages,
                ));
        } else {
            return $this->render('AppBundle::Metadata/metadataManagement.html.twig',
                array('files' => $files,
                    'members' => $members,
                    'folders' => $folders,
                    'collections' => $collections,
                    'agencies' => $agencies,
                    'currentPage' => $currentPage,
                    'totalPages' => $totalPages,
                ));
        }
    }

    /**
     * @Route("/metadata/ajax", name="metadata.ajax", defaults={"_format" = "json"})
     * @Extra\Method("POST")
     */
    public function metadataAjaxAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('homepage');
        }

        $action = $request->request->get('action');
        // UPDATE METADATA
        if($action == 'metadata-update') {
            $fileId = $request->request->get('id');
            $title = $request->request->get('title');
            $description = $request->request->get('description');
            $keywords = $request->request->get('keywords');
            $keywordsNew = array_map('trim',explode(',', $keywords));
            $keywordsOld = implode(',',array_unique(array_map('trim',explode(',', $keywords)))); // remove duplicated keywords and empty space
            $file = $em->getRepository("AppBundle:File")->find($fileId);
            if(!$file) {
                return $this->get("s_controller")->createResponse(false,"File not found");
            }
            #TODO improve this, this is part that is added for fix for deleting of keywords
            $currentKeywords = $file->getKeywords();
            foreach ($currentKeywords as $keyword){
                $keyword->removeFile($file);
                $file->removeKeyword($keyword);
            }

            $keywordRepository = $em->getRepository('AppBundle:Keywords');
            foreach ($keywordsNew as $keyword){
                $name = trim($keyword);
                if(empty($name)){
                    continue;
                }
                $keyword = $keywordRepository->findOneBy(array('name'=>$name));
                if(!isset($keyword)){
                    $keyword = new Keywords($name);
                    $em->persist($keyword);
                    $em->flush();
                }
                if(!empty($name)){
                    $keyword->addFile($file);
                    $em->persist($keyword);
                }
            }

            $file->setMetadataUpdated(true);
            $metadata = $file->getMetadata();
            $metadata->setTitle($title);
            $metadata->setDescription($description);
            $metadata->setKeywords($keywordsOld);
            $em->persist($metadata);
            $em->persist($file);

            $em->flush();

            $data['keywords'] =  str_replace(',', ', ', $keywords);

            return $this->get("s_controller")->createResponse(true,"Metadata update successfully", $data);

        }
        // GRID UPDATE METADATA
        else if($action == 'grid-metadata-update') {
            $fileIds = $fileIds = explode(",",$request->request->get('files'));
            $title = $request->request->get('title');
            $description = $request->request->get('description');
            $keywords = $request->request->get('keywords');
            $files = $em->getRepository("AppBundle:File")->getByParams(array("ids"=>$fileIds));
            $multiSelect = $request->request->get('multi_select');
            $keywordsOldWay = implode(',',array_unique(array_map('trim',explode(',', $keywords)))); // remove duplicated keywords and empty space
            $keywordsNewWay = array_map('trim',explode(',', $keywords));
            $keywordRepository = $em->getRepository('AppBundle:Keywords');
            foreach($files as $file) {
                $metadata = $file->getMetadata();
                //SET META TITLE
                if(!empty($title)) {
                    $title = str_replace(array("\r\n", "\n", "\r"),"",$title); // remove new lines
                    $metadata->setTitle($title);
                } else {
                    $metadata->setTitle("");
                }
                //SET META DESC
                if(!empty($description)) {
                    $description = str_replace(array("\r\n", "\n", "\r"),"",$description); // remove new lines
                    $metadata->setDescription($description);
                } else {
                    $metadata->setDescription("");
                }
                //SET META KEYWORD OLD WAY
                if(!empty($keywordsOld)) {
                    if($multiSelect!='false'){
                        $keywordsOne = explode(',', $keywordsOldWay);
                        $keywordsTwo = explode(',', $metadata->getKeywords());
                        $keywordsOldWay = implode(',',array_unique(array_merge($keywordsOne,$keywordsTwo), SORT_REGULAR));
                    }
                    $metadata->setKeywords($keywordsOldWay);
                }
                //SET KEYWORDS NEW WAY
                foreach ($keywordsNewWay as $keyword){
                    $name = trim($keyword);
                    if(empty($name)){
                        continue;
                    }
                    $keyword = $keywordRepository->findOneBy(array('name'=>$name));
                    if(!isset($keyword)){
                        $keyword = new Keywords($name);
                        $em->persist($keyword);
                        $em->flush();
                    }
                    if(!empty($name)){
                        $keyword->addFile($file);
                        $em->persist($keyword);
                    }
                }

                $file->setMetadataUpdated(true);
            }
            $em->flush();
            $data['keywords'] =  $keywordsOldWay;
            return $this->get("s_controller")->createResponse(true,"Files metadata update successfully", $data);
        }
        //TODO Move to other function, refactor upper one
        /*Change order of photo*/
        else if ($action=="order-update"){
            $fileIds = explode(",",$request->request->get('files'));
            $positions = explode(",",$request->request->get('positions'));
            $ordered =  array_combine($fileIds,$positions);
            $oldPositionFiles = $em->getRepository("AppBundle:File")->getByParams(array("orderNumber"=>$positions));
            $maxPositionFile = $em->getRepository("AppBundle:File")->getMaxOrdered();
            $maxOrderNumber = $maxPositionFile[0]->getOrderNumber();
            foreach($oldPositionFiles as $singleOldFile){
                $singleOldFile->setOrderNumber($maxOrderNumber+1);
                $em->persist($singleOldFile);
                $maxOrderNumber++;
            }
            $files = $em->getRepository("AppBundle:File")->getByParams(array("ids"=>$fileIds));
            $counter = 0;
            foreach($files as $file) {
                $file->setOrderNumber($ordered[$file->getId()]+1);
                $em->persist($file);
                $counter++;
            }
            $em->flush();
            return $this->get("s_controller")->createResponse(true,"Order of files changed successfully");
        }
        /* Sync data with AWS ( run symfony command ) */
        else if($action == "sync-metadata") {
            exec('php ../app/console metadata:update > /dev/null 2>/dev/null &');
            return $this->get("s_controller")->createResponse(true,"Images will be updated shortly");
        }
    }

    /**
     * @Route("/metadata/grid", name="metadata.grid")
     */
    public function metadataGridAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('homepage');
        }
        $members = $em->getRepository('AppBundle:User')->findBy(['company' => $user->getCompany()->getId()]);
        $collections = $this->get("s_folder")->findAllCollections(['company' => $user->getCompany()->getId()]);
        $agencies = $em->getRepository('AppBundle:UserAgency')->findBy(['user' => $user, 'hidden' => false], ['orderNumber' => 'ASC']);
        $folders = $em->getRepository('AppBundle:Folder')->findBy(['company' => $user->getCompany()->getId()]);
	    $words = $em->createQuery('SELECT keywords.name FROM AppBundle:Keywords keywords')
	                ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

	    $keywords = [];
	    foreach ($words as $word) {
		    $keywords[] = $word["name"];
	    }

        // SEARCH QUERY
        $query = $request->query->all();
        $query["company_id"] = $user->getCompany()->getId();
        $query["name"] = empty($request->query->get("name")) ? null : $request->query->get("name");

        // PAGINATION START | DONT USER PAGINATION IF FOLDER IS SET
        $paginationService = $this->get('s_pagination');
        if(!isset($query['folder_id'])){
            $paginationService->setLimit(50);
            $paginationService->setCurrentPage($request->query->get('page'));
            $query['limit'] = $paginationService->getLimit();
            $query['offset'] = $paginationService->countOffset($paginationService->getCurrentPage());
        } else{
            unset($query['page']);
            $request->query->remove('page');
        }
        $currentPage = $paginationService->getCurrentPage();
        $totalFiles = $em->getRepository('AppBundle:File')->countByParams($query);
        $totalPages = $paginationService->countTotalPages($totalFiles);
        // PAGINATION END

        $files = $em->getRepository('AppBundle:File')->getByParams($query);

        if ($request->isXmlHttpRequest()) { // AJAX
            return $this->render('AppBundle::Metadata/filesGrid.html.twig',
                array(
                    'files' => $files,
                    'members' => $members,
                    'collections' => $collections,
                    'agencies' => $agencies,
                    'currentPage' => $currentPage,
                    'totalPages' => $totalPages,
	                'keywords' => $keywords
                ));
        } else {
            return $this->render('AppBundle::Metadata/gridMetadataManagement.html.twig',
                array('files' => $files,
                    'folders' => $folders,
                    'members' => $members,
                    'collections' => $collections,
                    'agencies' => $agencies,
                    'currentPage' => $currentPage,
                    'totalPages' => $totalPages,
                    'keywords' => $keywords
                ));
        }
    }
}