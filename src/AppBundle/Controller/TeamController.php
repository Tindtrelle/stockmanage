<?php

namespace AppBundle\Controller;

use AppBundle\Entity\File;
use AppBundle\Entity\Folder;
use AppBundle\Entity\TeamMember;
use AppBundle\Entity\TmpData;
use AppBundle\Entity\User;
use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TeamController extends Controller
{
    /**
     * @Route("/team", name="team")
     */
    public function teamMembersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('homepage');
        }
        
        $members = $em->getRepository('AppBundle:User')->findBy(['company'=>$user->getCompany()->getId()]);

        return $this->render('AppBundle::Team/teamMembers.html.twig', array("members"=>$members));
    }

    /**
     * @Route("/team/ajax", name="team.ajax", defaults={"_format" = "json"})
     * @Extra\Method("POST")
     */
    public function folderAjaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            throw $this->createNotFoundException('You are not logged in.');
        }

        $action = $request->request->get('action');
        // FOLDER CREATE
        if ($action == 'member-create') {
            $id = $request->request->get('id');
            $name = $request->request->get('name');
            $customId = $request->request->get('custom_id');
            $rolePhotographer = $request->request->get('role_photographer');
            $roleRetoucher = $request->request->get('role_retoucher');
            $enabled = $request->request->get('enabled');
            $email = $request->request->get('email');
            $newMember = false;
            if (empty($name)) {
                return $this->get("s_controller")->createResponse(false, "Title is empty");
            }

            if (empty($id)) {
                $member = new User();
                $member->setDateCreated(new \DateTime());
                $newMember = true;
            } else {
                $member = $em->getRepository("AppBundle:User")->find($id);
            }
            if(isset($email)){
                $member->setEmail($email);
            }
            $member->setUsername($name);
            if($enabled){
                $member->setEnabled(true);
            } else{
                $member->setEnabled(false);
            }

            $member->setInternalId($customId);
            $internalRoles = array();
            if(!empty($rolePhotographer)) {
                $internalRoles[] = "photographer";
            }
            if(!empty($roleRetoucher)) {
                $internalRoles[] = "retoucher";
            }
            $member->setInternalRoles($internalRoles);
            $member->setOwner($user);
            $member->setCompany($user->getCompany());
            $member->setInternalAccount(true);
            $em->persist($member);
            $em->flush();

            // INSERT LOG
            if($newMember) {
                $this->container->get("s_log")->insertUserLog($user,["entityName"=>"user","entityId"=>$member->getId(),"action"=>"create"]);
            } else {
                $this->container->get("s_log")->insertUserLog($user,["entityName"=>"user","entityId"=>$member->getId(),"action"=>"update"]);
            }

            return $this->get("s_controller")->createResponse(true, "Success");

        }
        
        if($action == 'members-delete') {
            $membersStr = $request->request->get("members");
            $membersIds = explode(",",$membersStr);
            $members = $em->getRepository("AppBundle:User")->getByParams(['ids'=>$membersIds]);
            foreach($members as $member) {
                        // Insert log
                        $this->container->get("s_log")->insertUserLog($user,["entityName"=>"user","entityId"=>$member->getId(),"action"=>"delete"]);
                        $em->remove($member);

            }
            $em->flush();
            return $this->get("s_controller")->createResponse(true,"Members successful removed");
        }

        return $this->get("s_controller")->createResponse(false,"What should I do?");

    }

}
