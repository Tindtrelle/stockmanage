<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('homepage');
        }
        $users = $em->getRepository('AppBundle:User')->findAll();
        $companies = $em->getRepository('AppBundle:Company')->findAll();

        return $this->render('AppBundle::Admin/index.html.twig', array("users"=>$users,"companies"=>$companies));
    }

    /**
     * Login as specific User
     * @Route("/admin/users/login/{id}", name="admin.user.login.as")
     */
    public function userLoginAsAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $userService = $this->get('s_user');
        $user = $userService->getUser();
        // Redirect if not logged in
        if (!$user) {
            return $this->redirectToRoute('user.login');
        }

        $loginAsUser = $em->getRepository('AppBundle:User')->findOneById($id);
        if (!$loginAsUser) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $token = new UsernamePasswordToken($loginAsUser->getEmail(), NULL, "main", $loginAsUser->getRoles());
        $this->get('security.context')->setToken($token);

        $event = new InteractiveLoginEvent($request, $token);
        $this->get('event_dispatcher')->dispatch('security.interactive_login', $event);

        return $this->redirectToRoute('homepage');

    }

    /**
     * @Route("/admin/companies", name="admin.company.list")
     */
    public function companyListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('homepage');
        }
        $companies = $em->getRepository('AppBundle:Company')->findAll();

        return $this->render('AppBundle::Admin/Company/companyList.html.twig', array("companies"=>$companies));
    }

    /**
     * @Route("/admin/company/update/{id}", name="admin.company.update")
     */
    public function companyUpdateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('homepage');
        }

        $company = $em->getRepository('AppBundle:Company')->find($id);
        if(!$company) {
            $company = new Company();
        }

        if($request->getMethod() == "POST") {
            $name = $request->request->get("name");
            $id = $request->request->get("id");
            $guid = $request->request->get("guid");

            if(empty($id)) {
                $guid = md5($user->getEmail() . time());
                $company->setGuid($guid);
            } else {
                $company->setGuid($guid);
            }
            
            $company->setName($name);

            $em->persist($company);
            $em->flush();

            return $this->redirectToRoute("admin.company.list");
        }

        return $this->render('AppBundle::Admin/Company/companyUpdate.html.twig', array("company"=>$company));
    }
}