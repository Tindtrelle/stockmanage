<?php

namespace AppBundle\Controller;

use AppBundle\Entity\File;
use AppBundle\Entity\Folder;
use AppBundle\Entity\TmpData;
use AppBundle\Entity\TransactionEntity;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\TransactionEntityCategory;
use AppBundle\Entity\TransactionEntityUser;
use AppBundle\Entity\TransactionConnection;
use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Doctrine\Common\Util\Debug;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use PHPExcel_Style_Alignment;
use Symfony\Component\HttpFoundation\Response;

class FinanceController extends Controller
{
    /**
     * @Route("/finance", name="finance.list")
     */
    public function financeListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('homepage');
        }

        $query = $request->query->all();
        $query["company_id"] = $user->getCompany()->getId();
        $optionsSourceDestination = $em->getRepository("AppBundle:TransactionEntity")->findBy(array(), array('name' => 'ASC'));
        $activeStatus = $em->getRepository("AppBundle:FolderStatus")->findByName('active');
        $shootings = $em->getRepository("AppBundle:Folder")->findByStatus($activeStatus, array('name'=>'ASC'));
        $accountCategory = $em->getRepository("AppBundle:TransactionEntityCategory")->findOneBy(array("slug"=>'account'));
        $salaryUsers = $em->getRepository("AppBundle:User")->findByEnabled(true);
        $studios = $em->getRepository("AppBundle:Studio")->findBy(array(), array('name' => 'ASC'));

        $resultAccounts = $em->getRepository("AppBundle:TransactionEntity")->findBy(['owner'=>$user,'category'=>$accountCategory->getId()]);
        $userAccounts   = array();
        foreach($resultAccounts as $key=>$account){
            if(!$account->getParent()){
                $userAccounts[] = $account;
            }
        }
        #TODO improve this, added for calling because of transactions for shared users
        $sharedTransactionsRels = $em->getRepository('AppBundle:TransactionEntityUser')->findBy([
            'user' => $user
        ]);
        $userEntitiesIds = [];
        foreach ($sharedTransactionsRels as $sharedTransactionRel){
            array_push($userEntitiesIds, $sharedTransactionRel->getTransactionEntity()->getId());
        }

        $transactions = $em->getRepository("AppBundle:Transaction")->getAllByUser($user, $accountCategory->getId(), true, $userEntitiesIds);

	    /**
	     * Pagination START , prihvata i query i objekat, sredicu da svuda ide preko query-ja jer je tako brze...
	     */
	    $paginator  = $this->get('knp_paginator');
	    $pagination = $paginator->paginate(
		    $transactions, /* query NOT result */
		    $request->query->getInt('page', 1)/*page number*/,
		    100/*limit per page*/
	    );
	    // Pagination END

	    foreach($pagination->getItems() as $key=>$transaction){
		    $this->getAdditionalData($transaction);
	    }

        if ($request->isXmlHttpRequest()) { // AJAX
            return $this->render('AppBundle::Finance/transactionList.html.twig',[
                "optionsSourceDestination"=>$optionsSourceDestination,
                "transactions"=>$pagination,
                "userAccounts"=>$userAccounts,
                "shootings"=>$shootings,
                "studios"=>$studios,
                'salaryUsers'=>$salaryUsers,
            ]);
        } else {
            return $this->render('AppBundle::Finance/finance.html.twig',[
                "optionsSourceDestination"=>$optionsSourceDestination,
                "transactions"=>$pagination,
                "userAccounts"=>$userAccounts,
                "shootings"=>$shootings,
                "studios"=>$studios,
                'salaryUsers'=>$salaryUsers,
            ]);
        }
    }/**
     * @Route("/finance/all", name="finance.list.all")
     */
    public function financeLisAllAction(Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('homepage');
        }

        $query = $request->query->all();
        $query["company_id"] = $user->getCompany()->getId();

        $optionsSourceDestination = $em->getRepository("AppBundle:TransactionEntity")->findAll();
        $accountCategory = $em->getRepository("AppBundle:TransactionEntityCategory")->findOneBy(array("slug"=>'account'));
        $transactionsQuery = $em->getRepository("AppBundle:Transaction")->findAllPaginated();

	    /**
	     * Pagination START
	     */
	    $paginator  = $this->get('knp_paginator');
	    $pagination = $paginator->paginate(
		    $transactionsQuery, /* query NOT result */
		    $request->query->getInt('page', 1)/*page number*/,
		    100/*limit per page*/
	    );
	    // Pagination END

        $userAccounts = $em->getRepository("AppBundle:TransactionEntity")->findBy(['owner'=>$user,'category'=>$accountCategory->getId()]);
        if ($request->isXmlHttpRequest()) { // AJAX
            return $this->render('AppBundle::Finance/transactionList.html.twig',["optionsSourceDestination"=>$optionsSourceDestination,"transactions"=>$pagination, "userAccounts"=>$userAccounts]);
        } else {
            return $this->render('AppBundle::Finance/finance.html.twig',["optionsSourceDestination"=>$optionsSourceDestination,"transactions"=>$pagination, "userAccounts"=>$userAccounts]);
        }
    }
    /**
     * @Route("/finance/balances", name="finance.balances")
     */
    public function financeBalancesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('homepage');
        }
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        //$financeService = $this->get("s_finance");
        $accountCategory = $em->getRepository("AppBundle:TransactionEntityCategory")->findOneBy(array("slug"=>'account'));
        $accountIds = $request->query->get('ids');
        $filter_order = $request->query->get('sort');
        /*TODO make this part with params nicer - it is ugly IM 23.05.2017 */
        $field = 'date';
        $sort = 'DESC';
        if (!empty($filter_order)){
            $filter = explode("_", $filter_order);
            $field = $filter[0];
            $sort = $filter[1];
        }
        $selectedAccounts = [];
        if(!empty($accountIds)){
            $selectedAccounts = $em->getRepository("AppBundle:TransactionEntity")->findById(explode(',', $accountIds));
            $transactions = $em->getRepository("AppBundle:Transaction")->getAllByAccountIds($accountIds, $field, $sort, true);
        } else{
            $transactions = $em->getRepository("AppBundle:Transaction")->findByPaginated( [], [$field  => $sort] );
        }

	    /**
	     * Pagination START
	     */
	    $paginator  = $this->get('knp_paginator');
	    $page = $request->query->getInt('page', 1);
        unset($_GET);
        $pagination = $paginator->paginate(
		    $transactions, /* query NOT result */
            $page/*page number*/,
		    100/*limit per page*/
	    );
	    // Pagination END

	    foreach($pagination->getItems() as $key=>$transaction){
		    $this->getAdditionalData($transaction);
	    }

        $resultAccounts = $em->getRepository("AppBundle:TransactionEntity")->findBy(['category'=>$accountCategory->getId()]);
        $userAccounts = array();
        foreach($resultAccounts as $key=>$account){
            if(!$account->getParent()){
                $userAccounts[] = $account;
            }
        }
        if ($request->isXmlHttpRequest()) { // AJAX
            return $this->render('AppBundle::Finance/financeBalancesTransactions.html.twig',["transactions"=>$pagination, 'selectedAccounts'=>$selectedAccounts]);
        } else {
            return $this->render('AppBundle::Finance/financeBalances.html.twig',["userAccounts"=>$userAccounts, "transactions"=>$pagination]);
        }

    }


    /**
     * @Route("/finance/ajax", name="finance.ajax", defaults={"_format" = "json"})
     * @Extra\Method("POST")
     */
    public function financeAjaxAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        $financeService = $this->get("s_finance");
        if(!$user) {
            throw $this->createNotFoundException('You are not logged in.');
        }

        $action = $request->request->get('action');
        // TRANSACTION CREATE
        if($action == 'new-transaction') {
            $type = $request->request->get('type');
            $date = $request->request->get('date');
            $sourceAmount = floatval(str_replace(',', '', $request->request->get('source_amount')));
            $sourceAmount = abs($sourceAmount);
            $sourceCurrency = $request->request->get('source_currency');
            $sourceId = $request->request->get('source');
            $destinationAmount = floatval(str_replace(',', '', $request->request->get('destination_amount')));
            $destinationAmount = abs($destinationAmount);
            $destinationChildId = $request->request->get('destination-child');
            $statement = $request->request->get('statement');
            if(isset($destinationChildId)){
                $destinationId = $destinationChildId;
            } else{
                $destinationId = $request->request->get('destination');
            }
            $destinationCurrency = $request->request->get('destination_currency');
            $description = $request->request->get('description');

            // VALIDATE
            if($type != "expense" && $type != "income" && $type != "transfer") {
                return $this->get("s_controller")->createResponse(false,"Transaction type not allowed");
            }
            $transaction = new Transaction();
            $name = uniqid($user->getId());
            $transaction->setName($name);
            $transaction->setDateCreated(new \DateTime());
            $transaction->setDate(new \DateTime($date));
            $transaction->setType($type);
            $transaction->setDescription($description);
            $transaction->setStatement($statement);
            $transaction->setOwner($user);
            //STATUS
            if ($this->isGranted('ROLE_GENERAL_MANAGER')) {
                $transactionStatus = $em->getRepository("AppBundle:TransactionStatus")->findOneBy(array('name'=>'approved'));
            } else{
                $transactionStatus = $em->getRepository("AppBundle:TransactionStatus")->findOneBy(array('name'=>'pending'));
            }
            $transaction->setStatus($transactionStatus);
            // SOURCE
            $source = $em->getRepository("AppBundle:TransactionEntity")->findOneById($sourceId);
            if(!$source) {
                return $this->get("s_controller")->createResponse(false,"Source not found");
            }
            $transaction->setSource($source);
            // DESTINATION
            $destination = $em->getRepository("AppBundle:TransactionEntity")->findOneById($destinationId);
            if(!$destination) {
                return $this->get("s_controller")->createResponse(false,"Destination not found");
            }
            $transaction->setDestination($destination);
            // AMOUNT & CURRENCY
            if($type == 'transfer') {
                $transaction->setSourceAmount($sourceAmount);
                $transaction->setSourceCurrency($sourceCurrency);
                $transaction->setDestinationAmount($destinationAmount);
                $transaction->setDestinationCurrency($destinationCurrency);
            } else if ($type == 'income') {
                $transaction->setDestinationAmount($destinationAmount);
                $transaction->setDestinationCurrency($destinationCurrency);
            } else if($type == 'expense') {
                $transaction->setSourceAmount($sourceAmount);
                $transaction->setSourceCurrency($sourceCurrency);
            }

            // UPDATE BALANCE
            $balanceUpdateStatus = $financeService->updateBalance($transaction);
            if(!$balanceUpdateStatus['status']) {
                return $this->get("s_controller")->createResponse(false,$balanceUpdateStatus['message']);
            }

            $em->persist($transaction);

	        //Log transaction
	        $this->container->get("s_transaction_log")->insertTransactionLog($user, $transaction,
		        [ "action"=>"create", ]
	        );

            if($type=='expense'){
                $destinationCategory = $request->request->get('destination-category');
                if($destinationCategory == 'shoot'){
                    $transactionFolder = $request->request->get('transaction_folder');
                    $folder = $em->getRepository("AppBundle:Folder")->findOneById($transactionFolder);
                    if(!$folder){
                        return $this->get("s_controller")->createResponse(false,"Please select folder");
                    }
                    /*TODO if we start expanding this move it from here, find nicer solution IM 24.02.2017 */
                    $transactionConnection = new TransactionConnection();
                    $transactionConnection->setFolderId($folder);
                    $transactionConnection->setTransaction($transaction);
                    $em->persist($transactionConnection);

                } elseif($destinationCategory == 'studio'){
                    $studioId = $request->request->get('transaction_studio');
                    /*TODO if we start expanding this move it from here, find nicer solution IM 24.02.2017 */
                    if($studioId=='every'){
                        $studios = $em->getRepository("AppBundle:Studio")->findAll();
                        foreach($studios as $studio){
                            $transactionConnection = new TransactionConnection();
                            $transactionConnection->setStudioId($studio);
                            $transactionConnection->setTransaction($transaction);
                            $em->persist($transactionConnection);
                        }
                    } else{
                        $studio = $em->getRepository("AppBundle:Studio")->findOneById($studioId);
                        /*TODO if we start expanding this move it from here, find nicer solution IM 24.02.2017 */
                        if(!$studio){
                            return $this->get("s_controller")->createResponse(false,"Please select studio");
                        }
                        $transactionConnection = new TransactionConnection();
                        $transactionConnection->setStudioId($studio);
                        $transactionConnection->setTransaction($transaction);
                        $em->persist($transactionConnection);
                    }

                } elseif($destinationCategory == 'salary'){
                    $userId = $request->request->get('transaction_user_salary');
                    $user = $em->getRepository("AppBundle:User")->findOneById($userId);
                    if(!$user){
                        return $this->get("s_controller")->createResponse(false,"Please select User");
                    }
                    /*TODO if we start expanding this move it from here, find nicer solution IM 24.02.2017 */
                    $transactionConnection = new TransactionConnection();
                    $transactionConnection->setUserId($user);
                    $transactionConnection->setTransaction($transaction);
                    $em->persist($transactionConnection);
                }
            }

            $em->flush();

            return $this->get("s_controller")->createResponse(true,"Success");

        }
    }

    /**
     * @Route("/finance/transactions", name="finance.transactions")
     */
    public function transactionAction(Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('page.dashboard');
        }
        $em = $this->getDoctrine()->getManager();
        $transactions = $em->getRepository('AppBundle:Transaction')->findAllPaginated();
        $accountCategory = $em->getRepository("AppBundle:TransactionEntityCategory")->findOneBy(array("slug"=>'account'));
        $balance = $em->getRepository("AppBundle:TransactionEntity")->findBy(array("category"=>$accountCategory->getId()));

	    /**
	     * Pagination START
	     */
	    $paginator  = $this->get('knp_paginator');
	    $pagination = $paginator->paginate(
		    $transactions, /* query NOT result */
		    $request->query->getInt('page', 1)/*page number*/,
		    100/*limit per page*/
	    );
	    // Pagination END

        return $this->render('AppBundle::Finance/financeTransactionList.html.twig', array("transactions"=>$pagination, "balance"=>$balance));
    }
    /**
     * @Route("/finance/transaction/update/{id}", name="finance.transaction.update")
     */
    /*public function transactionUpdateAction(Request $request,$id) {
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $em = $this->getDoctrine()->getManager();
        $transaction = $em->getRepository('AppBundle:Transaction')->find($id);
        if($request->getMethod() == "POST") {
            return $this->redirectToRoute("finance.transactions");
        }
        return $this->render('AppBundle::Finance/transactionUpdate.html.twig', array(
            'transaction'=>$transaction
        ));

    }*/

    /**
     * @Route("/finance/entities", name="finance.entities")
     */
    public function transactionEntityAction(Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('page.dashboard');
        }
        $em = $this->getDoctrine()->getManager();
        //$transactionEntity = $em->getRepository('AppBundle:TransactionEntity')->findAll();
        $financeService = $this->get("s_finance");
        $transactionEntity = $em->getRepository('AppBundle:TransactionEntity')->findBy(array('parentEntity'=>null), array('name' => 'ASC'));

        foreach ($transactionEntity as $key=>$singleEntity){
            $transactionEntity[$key] = $financeService->getAdditionalDataEntity($singleEntity);
        }

        $transactionEntityCategory = $em->getRepository('AppBundle:TransactionEntityCategory')->findAll();

        return $this->render('AppBundle::Finance/financeSourceDestination.html.twig', array("user"=>$user, "transactions"=>$transactionEntity, "categories"=>$transactionEntityCategory));
    }

    /**
     * @Route("/finance/entities/new", name="finance.entities.create")
     */
    public function transactionEntityCreateAction(Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('page.dashboard');
        }

        $entityCategory = $em->getRepository('AppBundle:TransactionEntityCategory')->findAll();
        $agencies = $em->getRepository('AppBundle:Agency')->findAll();
        $folders = $em->getRepository('AppBundle:Folder')->findAll();
        $owners = $em->getRepository('AppBundle:User')->findAll();

        if($request->getMethod() == "POST") {
            $name = $request->request->get('name');
            $categoryId = $request->request->get('category');
            $position = $request->request->get('position');
            $currency = $request->request->get('currency');
            $owner = $request->request->get('owner');

            $category = $em->getRepository('AppBundle:TransactionEntityCategory')->findOneById((int)$categoryId);

            $transactionEntity = new TransactionEntity($name);
            $transactionEntity->setCategory($category);
            $transactionEntity->setPosition($position);
            $transactionEntity->setDateCreated(new \DateTime());
            $categorySlug = $transactionEntity->getCategory()->getSlug();
            if(isset($owner)){
                $userOwner = $em->getRepository('AppBundle:User')->findOneById((int)$owner);
                $transactionEntity->setOwner($userOwner);
            }

            if($categorySlug!='agency'){
                $transactionEntity->setCurrency($currency);
            }

            $agencyId = $request->request->get('agency');
            $folderId = $request->request->get('folder');

            if($categorySlug=='account'){
                $parent = $request->request->get('parent');
                $selectedParent = $request->request->get('parent_selected');
                if(isset($parent)){
                    $transactionEntity->setParent(true);
                } else{
                    if(!empty($selectedParent)){
                        $parentEntity = $em->getRepository('AppBundle:TransactionEntity')->findOneById((int)$selectedParent);
                        if(isset($parentEntity)){
                            $transactionEntity->setParentEntity($parentEntity);
                        }
                    } else{
                        $transactionEntity->setParentEntity(null);
                    }
                }

            } else if(isset($agencyId) && $categorySlug=='agency'){
                $connectionEntity = $em->getRepository('AppBundle:Agency')->findOneById((int)$agencyId);
            } else if(isset($folderId) && $categorySlug=='shoot'){
                $connectionEntity = $em->getRepository('AppBundle:Folder')->findOneById((int)$folderId);
            }

            if($request->request->get('displayStatement')){
                $transactionEntity->setdisplatStatement(true);
            }else{
                $transactionEntity->setdisplatStatement(false);
            }

            $em->persist($transactionEntity);

            $transactionEntityUsers = $request->request->get('shared_users');

            if(!empty($transactionEntityUsers)){
                foreach($transactionEntityUsers as $singleUser){
                    $transactionEntityUser = new TransactionEntityUser();
                    $user = $em->getRepository('AppBundle:User')->findOneById((int)$singleUser);
                    $transactionEntityUser->setUser($user);
                    $transactionEntityUser->setTransactionEntity($transactionEntity);
                    $em->persist($transactionEntityUser);
                }
            }

            if(isset($connectionEntity)){
                $connectionEntity->setTransactionEntity($transactionEntity);
                $em->persist($connectionEntity);
            }

            $em->flush();

            return $this->redirectToRoute('finance.entities');
        }

        $parentEntities = $em->getRepository('AppBundle:TransactionEntity')->findBy(array(
            'parent'=>true
        ));

        return $this->render('AppBundle::Finance/financeEntityCreate.html.twig', array(
            'entityCategory'=>$entityCategory,
            'agencies'=>$agencies,
            'folders'=>$folders,
            'parentEntities'=>$parentEntities,
            'owners'=>$owners
        ));
    }

    /**
     * @Route("/finance/entities/update/{id}", name="finance.entities.update")
     */
    public function transactionEntityUpdateAction(Request $request,$id)
    {
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('page.dashboard');
        }
        $transactionEntity = $em->getRepository('AppBundle:TransactionEntity')->find($id);
        $entityCategory = $em->getRepository('AppBundle:TransactionEntityCategory')->findAll();
        $agencies = $em->getRepository('AppBundle:Agency')->findAll();
        $folders = $em->getRepository('AppBundle:Folder')->findAll();
        $owners = $em->getRepository('AppBundle:User')->findAll();
        $selectedAgency = $em->getRepository('AppBundle:Agency')->findOneBy(array('transactionEntity'=>$transactionEntity->getId()));
        $accessUsers = $em->getRepository('AppBundle:TransactionEntityUser')->findBy(array('transactionEntity'=>$transactionEntity->getId()));
        if(!$transactionEntity) {
            throw new NotFoundHttpException("Agency not found");
        }

        if($request->getMethod() == "POST") {
            $name = $request->request->get('name');
            $categoryId = $request->request->get('category');
            $position = $request->request->get('position');
            $currency = $request->request->get('currency');
            $owner = $request->request->get('owner');

            $category = $em->getRepository('AppBundle:TransactionEntityCategory')->findOneById((int)$categoryId);
            $categorySlug = $transactionEntity->getCategory()->getSlug();
            $transactionEntity->setName($name);
            $transactionEntity->setCategory($category);
            $transactionEntity->setPosition($position);
            if(isset($owner)){
                $userOwner = $em->getRepository('AppBundle:User')->findOneById((int)$owner);
                $transactionEntity->setOwner($userOwner);
            }
            if($categorySlug!='agency'){
                $transactionEntity->setCurrency($currency);
            }

            $em->persist($transactionEntity);

            $agencyId = $request->request->get('agency');
            $folderId = $request->request->get('folder');

            $transactionEntityUsers = $request->request->get('shared_users');

            if(!empty($transactionEntityUsers)){
                foreach($transactionEntityUsers as $singleUser){
                    $accessUser = $em->getRepository('AppBundle:TransactionEntityUser')->findOneBy(array('transactionEntity'=>$transactionEntity->getId(), 'user'=>(int)($singleUser)));
                    if(!$accessUser){
                        $transactionEntityUser = new TransactionEntityUser();
                        $user = $em->getRepository('AppBundle:User')->findOneById((int)$singleUser);
                        $transactionEntityUser->setUser($user);
                        $transactionEntityUser->setTransactionEntity($transactionEntity);
                        $em->persist($transactionEntityUser);
                    }
                }
            }

            $parent = $request->request->get('parent');
            $selectedParent = $request->request->get('parent_selected');
            if(isset($parent)){
                $transactionEntity->setParent(true);
            } else{
                if(!empty($selectedParent)){
                    $parentEntity = $em->getRepository('AppBundle:TransactionEntity')->findOneById((int)$selectedParent);
                    if(isset($parentEntity)){
                        $transactionEntity->setParentEntity($parentEntity);
                        $transactionEntity->setParent(false);
                    }
                }
            }

            if($request->request->get('displayStatement')){
                $transactionEntity->setdisplatStatement(true);
            }else{
                $transactionEntity->setdisplatStatement(false);
            }

            if(isset($agencyId) && $categorySlug=='agency'){
                $connectionEntity = $em->getRepository('AppBundle:Agency')->findOneById((int)$agencyId);
            } else if(isset($folderId) && $categorySlug=='shoot'){
                $connectionEntity = $em->getRepository('AppBundle:Folder')->findOneById((int)$folderId);
            }

            if(isset($connectionEntity)){
                $connectionEntity->setTransactionEntity($transactionEntity);
                $em->persist($connectionEntity);
            }

            $em->flush();

            return $this->redirectToRoute('finance.entities');
        }
        $parentEntities = $em->getRepository('AppBundle:TransactionEntity')->findBy(array(
            'parent'=>true,
            'category'=>$transactionEntity->getCategory()
        ));
		
		$allParentEntities = $em->getRepository('AppBundle:TransactionEntity')->findBy(array(
            'parent'=>true
        ));

        $usersWithAccess = array();
        if(!empty($accessUsers)){
            foreach($accessUsers as $singleUser){
                $usersWithAccess[$singleUser->getUser()->getId()] = $singleUser->getUser()->getUsername();
            }
        }
        return $this->render('AppBundle::Finance/financeEntityUpdate.html.twig', array(
            "transactionEntity"=>$transactionEntity,
            'entityCategory'=>$entityCategory,
            'agencies'=>$agencies,
            'selectedAgency'=>$selectedAgency,
            'folders'=>$folders,
            'parentEntities'=>$parentEntities,
            'allParentEntities'=>$allParentEntities,
            'owners'=>$owners,
            'usersWithAccess'=>$usersWithAccess
        ));
    }
    /**
     * @Route("/finance/entities/category/new", name="finance.entities.category.create")
     */
    public function transactionEntityCategoryCreateAction(Request $request){

        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('page.dashboard');
        }
        if($request->getMethod() == "POST") {
            $id = $request->request->get('id');
            $name = $request->request->get('name');
            if(empty($id)) {
                $entityCategory = new TransactionEntityCategory($name, strtolower(str_replace(' ', '_', $name)));
                $entityCategory->setLocked(false);
            } else{
                $entityCategory = $em->getRepository("AppBundle:Folder")->find($id);
                if($entityCategory->getLocked()){
                    $this->addFlash(
                        'danger',
                        'Sorry this one is locked!!'
                    );
                    return $this->redirectToRoute("finance.entities");
                }
                $entityCategory->setName($name);
                $entityCategory->setSlug(strtolower(str_replace(' ', '_', $name)));
            }
            $em->persist($entityCategory);
            $em->flush();
            return $this->redirectToRoute("finance.entities");
        }
        return $this->render('AppBundle::Finance/financeEntityCategoryCreate.html.twig', array(

        ));

    }
    /**
     * @Route("/finance/entities/category/update/{id}", name="finance.entities.category.update")
     */
    public function transactionEntityCategoryUpdateAction(Request $request,$id) {


        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('page.dashboard');
        }

        $transactionEntityCategory = $em->getRepository('AppBundle:TransactionEntityCategory')->find($id);


        if($request->getMethod() == "POST") {

            $name = $request->request->get('name');
            $transactionEntityCategory->setName($name);
            $transactionEntityCategory->setSlug(strtolower($name));


            $em->persist($transactionEntityCategory);
            $em->flush();
            return $this->redirectToRoute("finance.entities");
        }
        return $this->render('AppBundle::Finance/financeEntityCategoryUpdate.html.twig', array(
            'entityCategory'=>$transactionEntityCategory
        ));

    }

    /**
     * Soft delete specific Transaction by ordinary user
     * @Extra\Route("finance/soft-delete/{id}", name="finance.soft.delete")
     * @Extra\Method("GET")
     *
     * @param $id
     * @return array
     */
    public function softDeleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
	    $user = $this->get("s_user")->getUser();
	    /** @var Transaction $transaction */
        $transaction = $em->getReference('AppBundle\Entity\Transaction', $id);

        if (!$transaction) {
            throw $this->createNotFoundException('Unable to find Finance with that id.');
        }
        $financeService = $this->get("s_finance");
        $balanceUpdateStatus = $financeService->updateBalance($transaction, true);
        $transaction->setArchive(true);

	    //Log transaction
	    $this->container->get("s_transaction_log")->insertTransactionLog($user, $transaction,
		    [ "action"=>"soft-delete", ]
	    );

        $em->persist($transaction);
        $em->flush();

	    $this->addFlash(
            'success',
            'Transaction Deleted!'
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Restore soft deleted specific Transaction by ordinary user to previous state
     * @Extra\Route("finance/restore-deleted/{id}", name="finance.restore.deleted")
     * @Extra\Method("GET")
     *
     * @param $id
     * @return array
     */
    public function restoreSoftDeletedAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $em = $this->getDoctrine()->getManager();
	    $user = $this->get("s_user")->getUser();

	    /** @var Transaction $transaction */
        $transaction = $em->getReference('AppBundle\Entity\Transaction', $id);

        if (!$transaction) {
            throw $this->createNotFoundException('Unable to find Finance with that id.');
        }
        $financeService = $this->get("s_finance");
        $balanceUpdateStatus = $financeService->updateBalance($transaction, false);
        $transaction->setArchive(false);

	    //Log transaction
	    $this->container->get("s_transaction_log")->insertTransactionLog($user, $transaction,
		    [ "action"=>"restore-soft-deleted", ]
	    );

        $em->persist($transaction);
        $em->flush();

        $this->addFlash(
            'success',
            'Transaction Restored!'
        );

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Hard Delete specific Transaction
     * @Extra\Route("finance/delete/{id}", name="finance.delete")
     * @Extra\Method("GET")
     *
     * @param $id
     * @return array
     */
    public function deleteAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $em = $this->getDoctrine()->getManager();
	    $user = $this->get("s_user")->getUser();
        $financeService = $this->get("s_finance");

	    /** @var Transaction $transaction */
        $transaction = $em->getReference('AppBundle\Entity\Transaction', $id);

        if (!$transaction) {
            throw $this->createNotFoundException('Unable to find Finance with that id.');
        }
        /** If transaction is not soft deleted than update balance on hard delete */
        if(!$transaction->getArchive()) {
            $financeService = $this->get("s_finance");
            $balanceUpdateStatus = $financeService->updateBalance($transaction, true);
        }

	    //Log transaction
	    $this->container->get("s_transaction_log")->insertTransactionLog($user, $transaction,
		    [ "action"=>"hard-delete", ]
	    );

        $em->remove($transaction);
        $em->flush();

        $this->addFlash(
            'success',
            'Transaction Deleted!'
        );

        if ($request->isXmlHttpRequest()) { // AJAX
            return new Response();
        } else {
            return $this->redirect($request->headers->get('referer'));
        }


    }

    /**
     * Update status of specific Transaction
     * @Extra\Route("finance/update/{id}/{status}", name="finance.update.status")
     * @Extra\Method("GET")
     *
     * @param $id
     * @param $status
     * @return array
     */
    public function updateStatusAction($id,$status, Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $em = $this->getDoctrine()->getManager();
	    $user = $this->get("s_user")->getUser();

	    /** @var Transaction $transaction */
	    $transaction = $em->getReference('AppBundle\Entity\Transaction', $id);
        $transactionStatus = $em->getRepository('AppBundle:TransactionStatus')->findOneBy(array('name'=>$status));
        if (!$transaction || !$transactionStatus) {
            throw $this->createNotFoundException('Unable to find transaction with that id.');
        }
        $oldStatus = $transaction->getStatus();
        $financeService = $this->get("s_finance");
        if($oldStatus->getName()=='declined' && $status=='approved'){
            $balanceUpdateStatus = $financeService->updateBalance($transaction);
        } else if($status=='declined'){
            $balanceUpdateStatus = $financeService->updateBalance($transaction, true);
        }
        $transaction->setStatus($transactionStatus);

	    //Log transaction
	    $this->container->get("s_transaction_log")->insertTransactionLog($user, $transaction,
		    [ "action"=>$status, ]
	    );

        $em->persist($transaction);
        $em->flush();
        $transactionForReposne = $transaction;

        $this->addFlash(
            'success',
            'Transaction Updated!'
        );
        if (!$request->isXmlHttpRequest()) { // AJAX
            $accountIds = $request->query->get('ids');
            $page = $request->query->get('page') != "null" ? $request->query->get('page') : 1;

            $transactions = $em->getRepository("AppBundle:Transaction")->findByPaginated(array(), array('id' => 'DESC'));

            /**
             * Pagination START , prihvata i query i objekat, sredicu da svuda ide preko query-ja jer je tako brze...
             */

            $paginator    = $this->get( 'knp_paginator' );
            $pagination   = $paginator->paginate(
                $transactions, /* query NOT result */
                $page/*$request->query->getInt( 'page', $page )/*page number*/,
                100/*limit per page*/
            );
            $params = $pagination->getParams();
            foreach ($params as $paramKey => $value) {
                if($paramKey != "page") {
                    $pagination->setParam($paramKey, null);
                }
            }
            $pagination->setUsedRoute('finance.balances');
            // Pagination END

            foreach($pagination->getItems() as $key=>$transaction){
                $this->getAdditionalData($transaction);
            }
        }


        if ($request->isXmlHttpRequest()) { // AJAX
            $response = new Response(json_encode(array('status' => true)));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            return $this->render('AppBundle::Finance/financeBalancesTransactions.html.twig',["transactions"=>$pagination]);
        }
    }
    /**
     * @Extra\Route("finance/edit/{id}", name="finance.edit")
     */
    public function editAction($id,Request $request)
    {
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        $em = $this->getDoctrine()->getManager();

        /** @var Transaction $transaction */
        $transaction = $em->getReference('AppBundle\Entity\Transaction', $id);
        $financeService = $this->get("s_finance");
        $user = $this->get("s_user")->getUser();
        if (!$transaction) {
            throw $this->createNotFoundException('Unable to find transaction with that id.');
        }
        if($request->getMethod() == "POST") {

            $type = $request->request->get('type');
            $date = $request->request->get('date');
            $sourceAmount = floatval(str_replace(',', '', $request->request->get('source_amount')));
            $sourceAmount = abs($sourceAmount);
            $sourceCurrency = $request->request->get('source_currency');
            $sourceId = $request->request->get('source');
            $destinationAmount = floatval(str_replace(',', '', $request->request->get('destination_amount')));
            $destinationAmount = abs($destinationAmount);
            $destinationId = $request->request->get('destination');

            //reset balance
            $financeService->updateBalance($transaction, true);

            if(isset($type)){
                $transaction->setType($type);
            }
            if(isset($date)){
                $transaction->setDate(new \DateTime($date));
            }
            if(isset($sourceAmount)){
                $transaction->setSourceAmount($sourceAmount);
            }
            if(isset($sourceCurrency)){
                $transaction->setSourceCurrency($sourceCurrency);
            }
            if(isset($sourceId)){
                $source = $em->getRepository("AppBundle:TransactionEntity")->findOneById($sourceId);
                $transaction->setSource($source);
            }
            if(isset($destinationAmount)){
                $transaction->setDestinationAmount($destinationAmount);
            }
            if(isset($destinationId)){
                $destination = $em->getRepository("AppBundle:TransactionEntity")->findOneById($destinationId);
                $transaction->setDestination($destination);
            }

            //update it again
            $balanceUpdateStatus = $financeService->updateBalance($transaction);
            if(!$balanceUpdateStatus['status']) {
                $this->addFlash(
                    'danger',
                    'There was some problem with transaction - #'.$transaction->getId()
                );
                return $this->redirect($request->headers->get('referer'));
            }

            $em->persist($transaction);

	        //Log transaction
	        $this->container->get("s_transaction_log")->insertTransactionLog($user, $transaction,
		        [ "action"=>"edit", ]
	        );

            $em->flush();
            $this->addFlash(
                'success',
                'Transaction Updated!'
            );
            return $this->redirectToRoute("finance.edit",array("id"=>$transaction->getId()));
        }
        //$financeService = $this->get("s_finance");
        $this->getAdditionalData($transaction);
        $sourceCategory = $em->getRepository('AppBundle:TransactionEntity')->getSourceCategories();
        $destinationCategory = $em->getRepository('AppBundle:TransactionEntity')->getDestinationCategories();
        return $this->render('AppBundle::Finance/editTransaction.html.twig',[
            "transaction"=>$transaction,
            "sourceCategory"=>$sourceCategory,
            "destinationCategory"=>$destinationCategory
        ]);
    }
    /**
     * Delete specific Transaction Entity
     * @Extra\Route("finance/entities/delete/{id}", name="finance.entities.delete")
     * @Extra\Method("GET")
     *
     * @param $id
     * @return array
     */
    public function deleteEntityAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $transactionEntity = $em->getReference('AppBundle\Entity\TransactionEntity', $id);

        if (!$transactionEntity) {
            throw $this->createNotFoundException('Unable to find Finance with that id.');
        }

        $singleTransaction = $em->getRepository('AppBundle:Transaction')->findOneBy(array('source'=>$transactionEntity->getId()));
        $singleSecondTransaction = $em->getRepository('AppBundle:Transaction')->findOneBy(array('destination'=>$transactionEntity->getId()));
        if($singleTransaction || $singleSecondTransaction){
            $this->addFlash(
                'danger',
                'Finance Entity contains transactions and could not be deleted!'
            );

            return $this->redirectToRoute('finance.list');
        }

        $em->remove($transactionEntity);
        $em->flush();

        $this->addFlash(
            'success',
            'Finance Entity Deleted!'
        );

        return $this->redirectToRoute('finance.entities');
    }
    /**
     * Delete specific Transaction Entity Category
     * @Extra\Route("finance/entities/category/delete/{id}", name="finance.entities.category.delete")
     * @Extra\Method("GET")
     *
     * @param $id
     * @return array
     */
    public function deleteEntityCategoryAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $transactionEntityCategory = $em->getReference('AppBundle\Entity\TransactionEntityCategory', $id);

        if (!$transactionEntityCategory) {
            throw $this->createNotFoundException('Unable to find Finance Category with that id.');
        }

        $transactionEntity = $em->getRepository('AppBundle:TransactionEntity')->findOneBy(array('category'=>$transactionEntityCategory->getId()));
        if($transactionEntity){
            $this->addFlash(
                'danger',
                'Finance Category could not be deleted!'
            );

            return $this->redirectToRoute('finance.entities');
        }

        $em->remove($transactionEntityCategory);
        $em->flush();

        $this->addFlash(
            'success',
            'Finance Category Deleted!'
        );

        return $this->redirectToRoute('finance.entities');
    }

    /**
     * Get
     * @Extra\Route("finance/entities/list/{id}", name="finance.entities.list")
     * @Extra\Method("GET")
     * * @param $id
     */
    public function getSourcesByCategoryAction($id) {
        $em = $this->getDoctrine()->getManager();
        $transactionCategory = $em->getReference('AppBundle\Entity\TransactionEntityCategory', $id);
        if (!$transactionCategory) {
            throw $this->createNotFoundException('Unable to find transaction with that id.');
        }
        $transactionEntities = $em->getRepository('AppBundle:TransactionEntity')->getEntitiesByCategory($id);

        $response = new Response(json_encode($transactionEntities));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    /**
     * Get
     * @Extra\Route("finance/entities/sources", name="finance.entities.sources")
     * @Extra\Method("GET")
     */
    public function getSourcesAction() {
        $em = $this->getDoctrine()->getManager();
        $transactionEntities = $em->getRepository('AppBundle:TransactionEntity')->getByPosition("expense-source");
        $response = new Response(json_encode($transactionEntities));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    /**
     * Get
     * @Extra\Route("finance/entities/destinations", name="finance.entities.destinations")
     * @Extra\Method("GET")
     */
    public function getDestinationsAction() {
        $em = $this->getDoctrine()->getManager();
        $transactionEntities = $em->getRepository('AppBundle:TransactionEntity')->getByPosition("income-destination");
        $response = new Response(json_encode($transactionEntities));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Export all transactions to xls file
     * @Extra\Route("finance/export", name="finance.export")
     * @Extra\Method("GET")
     *
     */
    public function exportAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('homepage');
        }
        $this->denyAccessUnlessGranted(['ROLE_GENERAL_MANAGER']);
        // ask the service for a Excel5
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->getProperties()->setCreator("lumina")->setTitle("Finance List");
        $phpExcelObject->getDefaultStyle()
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        //get data
        $accountCategory = $em->getRepository("AppBundle:TransactionEntityCategory")->findOneBy(array("slug"=>'account'));
        $allAccounts = $em->getRepository("AppBundle:TransactionEntity")->findBy(['category'=>$accountCategory->getId()]);

        $financeService = $this->get("s_finance");

        $resultAccounts = $em->getRepository("AppBundle:TransactionEntity")->findBy(['owner'=>$user,'category'=>$accountCategory->getId()]);
        $userAccounts = array();
        foreach($resultAccounts as $key=>$account){
            if(!$account->getParent()){
                $userAccounts[] = $account;
            }
        }

        $sheetIndex = 0;
        foreach ($allAccounts as $singleAccount){
            $transactions = $em->getRepository("AppBundle:Transaction")->getAllByAccountIds($singleAccount->getId());
            foreach($transactions as $key=>$transaction){
                $this->getAdditionalData($transaction);
            }
            $phpExcelObject->setActiveSheetIndex($sheetIndex)
                ->setCellValue('A1', 'Type')
                ->setCellValue('B1', 'Date')
                ->setCellValue('C1', 'Value')
                ->setCellValue('D1', 'Currency')
                ->setCellValue('E1', 'Source/Destination')
                ->setCellValue('F1', 'Description')
                ->setCellValue('G1', 'Info')
                ->setCellValue('H1', 'User')
                ->setCellValue('I1', 'Balance / ' . $user->getUsername());

            $cellIndex=2;
            foreach ($userAccounts as $userAccount){
                $phpExcelObject->setActiveSheetIndex($sheetIndex)
                    ->setCellValue('I'.$cellIndex, $userAccount->getName() . ' ( ' .$userAccount->getCurrency() . ' )');
                $cellIndex++;
                $phpExcelObject->setActiveSheetIndex($sheetIndex)
                    ->setCellValue('I'.$cellIndex, number_format((float) $userAccount->getBalance(), '2', '.', ','));
                $cellIndex++;
            }

            $cellIndex = 2;
            foreach ($transactions as $singleTransaction){
                $phpExcelObject->setActiveSheetIndex($sheetIndex)
                    ->setCellValue('A'.$cellIndex, ucfirst($singleTransaction->getType()))
                    ->setCellValue('B'.$cellIndex, $singleTransaction->getDate()->format("m/d/Y"));

                if($singleTransaction->getType() == 'expense'){
                    $phpExcelObject->setActiveSheetIndex($sheetIndex)
                        ->setCellValue('C'.$cellIndex, "-".number_format($singleTransaction->getSourceAmount(), 2, '.', ''))
                        ->setCellValue('D'.$cellIndex, strtoupper($singleTransaction->getSourceCurrency()))
                        ->setCellValue('E'.$cellIndex, $singleTransaction->getSource()->getName().PHP_EOL.$singleTransaction->getDestination()->getCategory()->getName());

                    $infoCellString = $singleTransaction->getDestination()->getName();
                    if($singleTransaction->getDestination()->getCategory()->getSlug() == 'studio'){
                        $infoCellString = $infoCellString.PHP_EOL;
                        $numItems = count($singleTransaction->getStudios());
                        $i = 0;
                        if($numItems != 0){
                            foreach($singleTransaction->getStudios() as $studio) {
                                $infoCellString = $infoCellString.$studio->getName();
                                if(++$i < $numItems) {
                                    $infoCellString = $infoCellString.", ";
                                }
                            }
                        }

                    } elseif($singleTransaction->getDestination()->getCategory()->getSlug() == 'shoot'){
                        $infoCellString = $infoCellString.PHP_EOL;
                        $numItems = count($singleTransaction->getShoots());
                        $i = 0;
                        if($numItems != 0){

                            foreach($singleTransaction->getShoots() as $shoot) {
                                $infoCellString = $infoCellString.$shoot->getName();
                                if(++$i < $numItems) {
                                    $infoCellString = $infoCellString.", ";
                                }
                            }
                        }

                    }
                    $phpExcelObject->setActiveSheetIndex($sheetIndex)
                        ->setCellValue('G'.$cellIndex, $infoCellString);

                } elseif ($singleTransaction->getType() == 'transfer'){

                    if($singleTransaction->getSource() == $singleAccount){
                        $phpExcelObject->setActiveSheetIndex($sheetIndex)
                            ->setCellValue('C'.$cellIndex, "-".number_format($singleTransaction->getSourceAmount(), 2, '.', ''));
                    } else{
                        $phpExcelObject->setActiveSheetIndex($sheetIndex)
                            ->setCellValue('C'.$cellIndex, "+".number_format($singleTransaction->getDestinationAmount(), 2, '.', ''));
                    }
                    $phpExcelObject->setActiveSheetIndex($sheetIndex)
                        ->setCellValue('D'.$cellIndex, strtoupper($singleTransaction->getSourceCurrency().PHP_EOL.$singleTransaction->getDestinationCurrency()));
                        $stringForLine = $singleTransaction->getSource()->getName();
                        if($singleTransaction->getDestination()->getParentEntity()){
                            $stringForLine = $stringForLine.PHP_EOL.$singleTransaction->getDestination()->getParentEntity()->getName();
                        } else{
                            $stringForLine = $stringForLine.PHP_EOL.$singleTransaction->getDestination()->getName();
                        }
                    $phpExcelObject->setActiveSheetIndex($sheetIndex)
                        ->setCellValue('E'.$cellIndex, $stringForLine);
                    if($singleTransaction->getDestination()->getParentEntity()){
                        $phpExcelObject->setActiveSheetIndex($sheetIndex)
                            ->setCellValue('G'.$cellIndex, $singleTransaction->getDestination()->getName());
                    }

                } else{
                    $phpExcelObject->setActiveSheetIndex($sheetIndex)
                        ->setCellValue('C'.$cellIndex, "+".number_format($singleTransaction->getDestinationAmount(), 2, '.', ''))
                        ->setCellValue('D'.$cellIndex, strtoupper($singleTransaction->getDestinationCurrency()))
                        ->setCellValue('E'.$cellIndex, $singleTransaction->getDestination()->getCategory()->getName().PHP_EOL.$singleTransaction->getDestination()->getName());
                    $phpExcelObject->setActiveSheetIndex($sheetIndex)
                        ->setCellValue('G'.$cellIndex, $singleTransaction->getDestination()->getName());
                }

                $phpExcelObject->setActiveSheetIndex($sheetIndex)
                    ->setCellValue('F'.$cellIndex, $singleTransaction->getDescription())
                    ->setCellValue('H'.$cellIndex, $singleTransaction->getOwner()->getUsername());

                $phpExcelObject->getActiveSheet()
                    ->getStyle('C'.$cellIndex)
                    ->getAlignment()
                    ->setWrapText(true);

                $phpExcelObject->getActiveSheet()
                    ->getStyle('D'.$cellIndex)
                    ->getAlignment()
                    ->setWrapText(true);

                $phpExcelObject->getActiveSheet()
                    ->getStyle('E'.$cellIndex)
                    ->getAlignment()
                    ->setWrapText(true);

                $phpExcelObject->getActiveSheet()
                    ->getStyle('G'.$cellIndex)
                    ->getAlignment()
                    ->setWrapText(true);
                $cellIndex++;
            }
            //Set size of columns
            foreach(range('A','I') as $columnID) {
                $phpExcelObject->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $phpExcelObject->getActiveSheet()->setTitle($singleAccount->getName());
            $phpExcelObject->createSheet();
            $sheetIndex +=1;
        }
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        //$phpExcelObject->setActiveSheetIndex(0);
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $date = new \DateTime();
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'finance'.$date->format('_d_m_Y').'.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

	/**
	 * Get user additional data ( profile image, active role )
	 * @var Transaction
	 */
	public function getAdditionalData(Transaction $transaction) {
		$connections = $transaction->getConnection()->toArray();
		$transactionStudios = [];
		$transactionFolders = [];
		$transactionUsers = [];

		foreach ($connections as $connection) {
			if($connection->getStudioId())
				$transactionStudios[] = $connection->getStudioId();
			if($connection->getFolderId())
				$transactionFolders[] = $connection->getFolderId();
			if($connection->getUserId())
				$transactionUsers[] = $connection->getUserId();
		}

		if(!empty($transactionStudios))
			$transaction->setStudios($transactionStudios);
		if(!empty($transactionFolders))
			$transaction->setShoots($transactionFolders);
		if(!empty($transactionUsers))
			$transaction->setUsersSalary($transactionUsers);

	}

}