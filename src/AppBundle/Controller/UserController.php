<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use AppBundle\Entity\UserAgency;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\HttpFoundation\Session\Session;
/**
 * User controller.
 */
class UserController extends Controller
{

    /**
     * Sign Up
     * @Extra\Route("user/signup/ajax", name="user.signup.ajax", defaults={"_format" = "json"})
     * @Extra\Method("POST")
     */
    public function userSignUpAjaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $userData = $this->getUserDataRequest($request);
        $redirectTo = "dashboard";

        $user = $em->getRepository('AppBundle:User')->findOneByEmail($userData['email']);
        /* IF USER EXISTS */
        if ($user) {
                return $this->get("s_controller")->createResponse(false,"User already registered");
        } else {  /* IF USER NOT EXISTS, CREATE NEW USER */
                $validator = $this->get('s_validator');
                $errors = $validator->userRegisterValidate($userData);
                if (count($errors)) {
                    return $this->get("s_controller")->createResponse(false,$errors);
                }
                // CREATE
                $user = $this->userCreate($userData);
                $user->setEnabled(false);
                $em->persist($user);
                $em->flush();
                // CREATE USER AGENCIES
                $this->addUserAgencies($user);

                // LOGIN
                $this->userLogin($user,$user->getPassword(),$request);

                return $this->get("s_controller")->createResponse(true,"Register successful", array('redirect'=>$redirectTo));
        }
    }

    /**
     * Sign Up
     * @Extra\Route("user/login/ajax", name="user.login.ajax", defaults={"_format" = "json"})
     * @Extra\Method("POST")
     */
    public function userLoginAjaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $userData = $this->getUserDataRequest($request);
        $redirectTo = "dashboard";

        $user = $em->getRepository('AppBundle:User')->findOneByEmail($userData['email']);
        /* IF NOT EXISTS */
        if (!$user) {
            return $this->get("s_controller")->createResponse(false,"User with that email address doesn't exists");
        } else {
            if(!$user->isEnabled()) {
                $this->get("s_controller")->createResponse(false,"Your account is still not activated");
            }
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            if($encoder->isPasswordValid($user->getPassword(), $userData["password"],$user->getSalt())) {
                $this->userLogin($user, $userData["password"], $request);
                return $this->get("s_controller")->createResponse(true,"Login successful", array('redirect'=>$redirectTo));
            } else {
                return $this->get("s_controller")->createResponse(false,"Email or password are not valid");
            }


        }
    }

    /**
     * Logout
     * @Extra\Route("user/logout", name="user.logout")
     * @Extra\Method("GET")
     */
    public function userLogoutAction(Request $request)
    {
        $this->container->get('security.context')->setToken(NULL);

        return $this->redirectToRoute('homepage');

    }

    protected function userCreate($userData)
    {
        $user = new User();
        $user->setUsername($userData['email']);
        $user->setEmail($userData['email']);
        $user->setDateCreated(new \DateTime());
        $user->addRole("ROLE_USER");

        // Encode password
        if(isset($userData['password']))
        {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($userData['password'], $user->getSalt());
            $user->setPassword($password);
            // Token
            $token = md5($user->getEmail() . time());
            $user->setConfirmationToken($token);
        }
        if(isset($userData["company"])) {
            $company = $this->getDoctrine()->getManager()->getRepository('AppBundle:Company')->findOneByGuid($userData["company"]);
            if(!$company) {
                $company = new Company();
                $guid = md5($user->getEmail() . time());
                $company->setGuid($guid);
            }
        } else {
            $company = new Company();
            $guid = md5($user->getEmail() . time());
            $company->setGuid($guid);
        }
        $user->setCompany($company);
        return $user;

    }

    protected function getUserDataRequest($request) {
        $userData = array();
        $postData = $request->request->all();

        $userData['email'] = $postData['email'];
        $userData['password'] = $postData['password'];
        $userData["company"] = isset($postData["company"]) ? $postData["company"] : null;

        return $userData;
    }

    protected function userLogin(User $user, $password, Request $request) {
        $token = new UsernamePasswordToken($user->getEmail(), $password, "main", $user->getRoles());
        $this->get('security.context')->setToken($token);

        $event = new InteractiveLoginEvent($request, $token);
        $this->get('event_dispatcher')->dispatch('security.interactive_login', $event);
    }

    protected function addUserAgencies($user) {
        $em = $this->getDoctrine()->getManager();
        $stockAgencies  = $em->getRepository('AppBundle:Agency')->findAll();
        foreach($stockAgencies as $stockAgency) {
            $agency = $em->getRepository('AppBundle:UserAgency')->findOneBy(array('user'=>$user,'agency'=>$stockAgency));
            if(!$agency) {
                $userAgency = new UserAgency();
                $userAgency->setAgency($stockAgency);
                $userAgency->setUser($user);
                $userAgency->setOrderNumber(0);
                $userAgency->setHidden(false);
                $em->persist($userAgency);
            }
        }
        $em->flush();
    }
}
