<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Agency;
use AppBundle\Entity\File;
use AppBundle\Entity\FileAgencyStatus;
use AppBundle\Entity\Folder;
use AppBundle\Entity\TmpData;
use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AgencyController extends Controller
{
    /**
     * @Route("/agency", name="agency.list")
     */
    public function filesListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if(!$user) {
            return $this->redirectToRoute('homepage');
        }
        $members = $em->getRepository('AppBundle:User')->findBy(['company'=>$user->getCompany()->getId()]);
        $collections = $this->get("s_folder")->findAllCollections(['company'=>$user->getCompany()->getId()]);
        $agencies = $em->getRepository('AppBundle:UserAgency')->findBy(['user'=>$user,'hidden'=>false],['orderNumber'=>'ASC']);

        // SEARCH QUERY
        $query = $request->query->all();
        $query["company_id"] = $user->getCompany()->getId();
        $query["name"] = empty($request->query->get("name")) ? null : $request->query->get("name");

        // PAGINATION START
        $paginationService = $this->get('s_pagination');
        $paginationService->setLimit(50);
        $paginationService->setCurrentPage($request->query->get('page'));
        $query['limit'] = $paginationService->getLimit();
        $query['offset'] = $paginationService->countOffset($paginationService->getCurrentPage());

        $currentPage = $paginationService->getCurrentPage();
        $totalFiles = $em->getRepository('AppBundle:File')->countByParams($query);
        $totalPages =  $paginationService->countTotalPages($totalFiles);
        // PAGINATION END

        $files = $em->getRepository('AppBundle:File')->getByParams($query);

        if($request->isXmlHttpRequest()) { // AJAX
            return $this->render('AppBundle::Agency/filesList.html.twig',
                array(
                    'files'=>$files,
                    'members'=>$members,
                    'collections'=>$collections,
                    'agencies'=>$agencies,
                    'currentPage'=>$currentPage,
                    'totalPages'=>$totalPages
                ));
        } else {
            return $this->render('AppBundle::Agency/agencyManagement.html.twig',
                array('files'=>$files,
                    'members'=>$members,
                    'collections'=>$collections,
                    'agencies'=>$agencies,
                    'currentPage'=>$currentPage,
                    'totalPages'=>$totalPages
                    ));
        }
    }

    /**
     * @Route("/agency/ajax", name="agency.ajax", defaults={"_format" = "json"})
     * @Extra\Method("POST")
     */
    public function agencyAjaxAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            throw $this->createNotFoundException('You are not logged in.');
        }

        $action = $request->request->get('action');
        // FOLDER CREATE
        if ($action == 'agency-create') {
            $id = $request->request->get('id');
            $name = $request->request->get('name');
            if (empty($name)) {
                return $this->get("s_controller")->createResponse(false, "Title is empty");
            }

            if (empty($id)) {
                $agency = new Agency();
                $agency->setDateCreated(new \DateTime());
            } else {
                $agency = $em->getRepository("AppBundle:Agency")->find($id);
            }
            $agency->setName($name);
            $em->persist($agency);
            $em->flush();
            return $this->get("s_controller")->createResponse(true, "Success");
        }
        // STATUS UPDATE SINGLE FILE - NOT USED ATM
//        if ($action == 'status-update') {
//            $fileId = $request->request->get('file');
//            $agencyId = $request->request->get('agency');
//            $statusName = $request->request->get('status');
//            // Validate
//            $file = $em->getRepository("AppBundle:File")->find($fileId);
//            if(!$file) {
//                return $this->get("s_controller")->createResponse(false, "File not found");
//            }
//            $agency = $em->getRepository("AppBundle:Agency")->find($agencyId);
//            if(!$agency) {
//                return $this->get("s_controller")->createResponse(false, "Agency not found");
//            }
//            $agencyStatus = $em->getRepository("AppBundle:AgencyStatus")->findOneByName($statusName);
//            if(!$agencyStatus) {
//                return $this->get("s_controller")->createResponse(false, "Status not found");
//            }
//
//            // Update
//            $fileAgencyStatus = $em->getRepository("AppBundle:FileAgencyStatus")->findOneBy(array("file"=>$file,"agency"=>$agency));
//            if(!$fileAgencyStatus) {
//                $fileAgencyStatus = new FileAgencyStatus();
//                $fileAgencyStatus->setFile($file);
//                $fileAgencyStatus->setAgency($agency);
//            }
//            $fileAgencyStatus->setStatus($agencyStatus);
//
//            $em->persist($fileAgencyStatus);
//            $em->flush();
//            // Log
//            $this->container->get("s_log")->insertUserLog($user,["entityName"=>"status","entityId"=>$fileAgencyStatus->getId(),"action"=>"update"]);
//
//
//            return $this->get("s_controller")->createResponse(true, "Status updated successfully",array("status"=>$agencyStatus->getTitle()));
//        }

        // STATUS UPDATE MULTIPLE FILES
        if ($action == 'status-update-multiple') {
            $fileIds = $request->request->get('files');
            $agencyId = $request->request->get('agency');
            $statusName = $request->request->get('status');
            $description = $request->request->get('description');
            // Validate
            $agency = $em->getRepository("AppBundle:Agency")->find($agencyId);
            if(!$agency) {
                return $this->get("s_controller")->createResponse(false, "Agency not found");
            }
            $agencyStatus = $em->getRepository("AppBundle:AgencyStatus")->findOneByName($statusName);
            if(!$agencyStatus) {
                return $this->get("s_controller")->createResponse(false, "Status not found");
            }
            $fileIdsArr = explode(",",$fileIds);
            $counter = 0;
            foreach($fileIdsArr as $fileId) {
                $file = $em->getRepository("AppBundle:File")->find($fileId);
                if($file) {
                    // Update
                    $fileAgencyStatus = $em->getRepository("AppBundle:FileAgencyStatus")->findOneBy(array("file"=>$file,"agency"=>$agency));
                    if(!$fileAgencyStatus) {
                        $fileAgencyStatus = new FileAgencyStatus();
                        $fileAgencyStatus->setFile($file);
                        $fileAgencyStatus->setAgency($agency);
                    }
                    $fileAgencyStatus->setStatus($agencyStatus);
                    $fileAgencyStatus->setDescription($description);
                    $em->persist($fileAgencyStatus);
                    $counter++;
                    // Log
                    $this->container->get("s_log")->insertUserLog($user,["entityName"=>"status","entityId"=>$fileAgencyStatus->getId(),"action"=>"update"]);
                }
            }
            $em->flush();
            if($counter) {
                $message = $counter ." images successfully updated";
            } else {
                $message = "No images updated";
            }

            return $this->get("s_controller")->createResponse(true, $message,array("status"=>$agencyStatus->getTitle()));
        }

        // SEND TO FTP
        if ($action == 'files-send-ftp') {
            $fileIds = $request->request->get('files');
            $agencyId = $request->request->get('agency');
            // Validate
            $agency = $em->getRepository("AppBundle:UserAgency")->find($agencyId);
            if(!$agency) {
                return $this->get("s_controller")->createResponse(false, "Agency not found");
            }
            // Get files
            $fileIdsArr = explode(",",$fileIds);
            $filesToDownload = [];
            foreach($fileIdsArr as $fileId) {
                $file = $em->getRepository("AppBundle:File")->find($fileId);
                if(!$file) { continue; }
                $filesToDownload[] = $file;
            }
            // Try to connect
            $ftp_server = $agency->getFtpServer();
            $ftp_user_name = $agency->getFtpUser()  ;
            $ftp_user_pass = $agency->getFtpPass();
            if(empty($ftp_server) || empty($ftp_user_name) || empty($ftp_user_pass) ) {
                return $this->get("s_controller")->createResponse(false, "FTP data is not set");
            }
            try {
                $conn_id = ftp_connect($ftp_server);
            } catch (ContextErrorException $e) {
                return $this->get("s_controller")->createResponse(false, "Cannot connect to FTP server");
            }
            $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
            if(!$login_result) {
                return $this->get("s_controller")->createResponse(false, "Cannot connect to FTP server");
            }
            ftp_pasv($conn_id, true);

            $counter = 0;
            $message = "";
            foreach ($filesToDownload as $file) {
                if($file->getPath()) {
                    if (ftp_put($conn_id, $file->getName(), $file->getPath(), FTP_BINARY)) {
                        $counter++;
                    } else {
                    }
                }
            }
            ftp_close($conn_id);
            if($counter) {
                $message = $counter ." images successfully sent";
            }
            return $this->get("s_controller")->createResponse(true, $message);
        }

        return $this->get("s_controller")->createResponse(false, "no action");

    }
}
