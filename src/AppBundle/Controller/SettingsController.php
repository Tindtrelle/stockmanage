<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserAgency;
use AppBundle\Entity\Agency;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SettingsController extends Controller
{
    /**
     * @Route("/settings/account", name="settings.account")
     */
    public function indexAction(Request $request)
    {
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('page.dashboard');
        }
        if($request->getMethod() == "POST") {
            $em = $this->getDoctrine()->getManager();
            $name = $request->request->get('name');
            $email = $request->request->get('email');
            $firstName = $request->request->get('firstName');
            $lastName = $request->request->get('lastName');
            if(isset($email)){
                $user->setEmail($email);
            }
            if(isset($name)){
                $user->setUsername($name);
            }
            if(isset($firstName)){
                $user->setFirstName($firstName);
            }
            if(isset($lastName)){
                $user->setLastName($lastName);
            }
            $em->persist($user);
            $em->flush();
        }

        return $this->render('AppBundle::Settings/settingsAccount.html.twig', array("user"=>$user));
    }


    /**
     * @Route("/settings/agency", name="settings.agency")
     */
    public function agencyAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('page.dashboard');
        }
        $agencies = $em->getRepository('AppBundle:UserAgency')->findBy(['user'=>$user]);
        return $this->render('AppBundle::Settings/settingsAgency.html.twig', array("agencies"=>$agencies));
    }
    /**
     * @Route("/settings/agency/new", name="settings.agency.create")
     */
    public function agencyCreateAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('page.dashboard');
        }


        if($request->getMethod() == "POST") {
            $name = $request->request->get('name');
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $name)));
            $ftpServer = $request->request->get('ftp_server');
            $ftpUser = $request->request->get('ftp_user');
            $ftpPass = $request->request->get('ftp_pass');

            $agency = new Agency();
            $agency->setName($name);
            $agency->setSlug($slug);
            $userAgency = new UserAgency();
            $userAgency->setAgency($agency);
            $userAgency->setUser($user);

            $userAgency->setFtpServer($ftpServer);
            $userAgency->setFtpUser($ftpUser);
            $userAgency->setFtpPass($ftpPass);
            $userAgency->setOrderNumber(100);
            $userAgency->setHidden(false);

            $em->persist($agency);
            $em->persist($userAgency);

            $em->flush();

            return $this->redirectToRoute('settings.agency');
        }

        return $this->render('AppBundle::Settings/settingsAgencyCreate.html.twig');
    }

    /**
     * @Route("/settings/agency/update/{id}", name="settings.agency.update")
     */
    public function agencyUpdateAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get("s_user")->getUser();
        if (!$user) {
            return $this->redirectToRoute('page.dashboard');
        }
        $userAgency = $em->getRepository('AppBundle:UserAgency')->find($id);
        if(!$userAgency) {
            throw new NotFoundHttpException("Agency not found");
        }

        if($request->getMethod() == "POST") {
            $ftpServer = $request->request->get('ftp_server');
            $ftpUser = $request->request->get('ftp_user');
            $ftpPass = $request->request->get('ftp_pass');

            $userAgency->setFtpServer($ftpServer);
            $userAgency->setFtpUser($ftpUser);
            $userAgency->setFtpPass($ftpPass);

            $em->persist($userAgency);
            $em->flush();

            return $this->redirectToRoute('settings.agency.update',array('id'=>$userAgency->getId()));
        }
        
        return $this->render('AppBundle::Settings/settingsAgencyUpdate.html.twig', array("agency"=>$userAgency));
    }
}