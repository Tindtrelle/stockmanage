<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $user = $this->get("s_user")->getUser();
        if($user) {
            return $this->redirectToRoute('page.dashboard');
        }

        return $this->render('AppBundle::index.html.twig', array());
    }

    /**
     * @Route("/signup", name="page.signup")
     */
    public function signUpAction(Request $request)
    {
        $user = $this->get("s_user")->getUser();
        if($user) {
            return $this->redirectToRoute('page.dashboard');
        }
        return $this->render('AppBundle::Page/signup.html.twig', array());
    }

    /**
     * @Route("/dashboard", name="page.dashboard")
     */
    public function dashboardAction(Request $request)
    {
        $user = $this->get("s_user")->getUser();
        if(!$user) {
            return $this->redirectToRoute('homepage');
        }
        return $this->render('AppBundle::Page/dashboard.html.twig', array());
    }
}
