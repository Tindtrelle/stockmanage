<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="sm_file")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FileRepository")
 */
class File
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     *
     */
    protected $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $dateCreated;

    /**
     * @ORM\OneToMany(targetEntity="FileUser", mappedBy="file", cascade={"persist","remove"})
     **/
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="FileAgencyStatus", mappedBy="file", cascade={"persist","remove"})
     **/
    private $statuses;

    /**
     * @ORM\ManyToOne(targetEntity="Folder")
     * @ORM\JoinColumn(name="folder_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $folder;

    /**
     * @var string
     * @ORM\Column(name="path", type="string", nullable=true)
     *
     */
    protected $path;

    /**
     * @var string
     * @ORM\Column(name="aws_key", type="string", nullable=true)
     *
     */
    protected $awsKey;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=true, unique=false)
     */
    private $company;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="internal_date_created", type="datetime", nullable=true)
     */
    protected $internalDateCreated;

    /**
     * @ORM\ManyToOne(targetEntity="Metadata", cascade={"persist"})
     * @ORM\JoinColumn(name="meta_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     **/
    private $metadata;

    /**
     * Used for updating file metadata with cron
     * @var boolean
     * @ORM\Column(name="meta_updated", type="boolean", nullable=true)
     */
    protected $metadataUpdated;
    /**
     * @var boolean
     * @ORM\Column(name="video", type="boolean", nullable=true)
     */
    protected $video;
    /**
     * @ORM\Column(name="order_number", type="integer", nullable=true)
     *
     */
    protected $orderNumber;


    private $source;

    /**
     * TMP FileMetaUpdate entity
     */
    private $metaUpdate;

    /**
     * @ORM\ManyToMany(targetEntity="Keywords", mappedBy="file", indexBy="id", cascade={"persist"})
     * @ORM\OrderBy({"name" = "ASC"})*
     **/
    private $keywords;


    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->statuses = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return mixed
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @param mixed $folder
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getAwsKey()
    {
        return $this->awsKey;
    }

    /**
     * @param string $awsKey
     */
    public function setAwsKey($awsKey)
    {
        $this->awsKey = $awsKey;
    }

    /**
     * Add user
     */
    public function addUser($user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove users
     */
    public function removeUser($user)
    {
        $this->users->removeElement($user);
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return \DateTime
     */
    public function getInternalDateCreated()
    {
        return $this->internalDateCreated;
    }

    /**
     * @param \DateTime $internalDateCreated
     */
    public function setInternalDateCreated($internalDateCreated)
    {
        $this->internalDateCreated = $internalDateCreated;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * @param mixed $statuses
     */
    public function setStatuses($statuses)
    {
        $this->statuses = $statuses;
    }

    /**
     * Add status
     */
    public function addStatus($status)
    {
        $this->statuses[] = $status;

        return $this;
    }

    /**
     * Remove status
     */
    public function removeStatus($status)
    {
        $this->statuses->removeElement($status);
    }

    /**
     * @return mixed
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param mixed $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return boolean
     */
    public function isMetadataUpdated()
    {
        return $this->metadataUpdated;
    }

    /**
     * @param boolean $metadataUpdated
     */
    public function setMetadataUpdated($metadataUpdated)
    {
        $this->metadataUpdated = $metadataUpdated;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param mixed $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @param mixed $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @return mixed
     */
    public function getMetaUpdate()
    {
        return $this->metaUpdate;
    }

    /**
     * @param mixed $metaUpdate
     */
    public function setMetaUpdate($metaUpdate)
    {
        $this->metaUpdate = $metaUpdate;
    }

    /**
     * @return bool
     */
    public function isVideo()
    {
        return $this->video;
    }

    /**
     * @param bool $video
     */
    public function setVideo($video)
    {
        $this->video = $video;
    }

    /**
     * @return mixed
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param mixed $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    public function addKeyword(Keywords $keyword){
        if (!$this->keywords->contains($keyword)) {
            $this->keywords[] = $keyword;
        }
    }
    /**
     * Remove keyword
     */
    public function removeKeyword($keyword)
    {
        if ($this->keywords->contains($keyword)) {
            $this->keywords->removeElement($keyword);
        }

    }



}