<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 * @ORM\Table(name="sm_file_user")
 * @ORM\Entity
 */
class FileUser
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="File", inversedBy="users")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",  onDelete="CASCADE", nullable=true)
     **/
    private $user;

    /**
     * @var string
     * @ORM\Column(name="type", type="string" , nullable=true)
     *
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(name="role", type="string" , nullable=true)
     *
     */
    protected $role;


    /**
     * @var boolean
     * @ORM\Column(name="unknown_user", type="boolean", nullable=true)
     */
    protected $unknownUser;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }


    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return boolean
     */
    public function isUnknownUser()
    {
        return $this->unknownUser;
    }

    /**
     * @param boolean $unknownUser
     */
    public function setUnknownUser($unknownUser)
    {
        $this->unknownUser = $unknownUser;
    }







}