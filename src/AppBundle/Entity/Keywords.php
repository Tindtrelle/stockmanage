<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Table(name="sm_keywords", indexes={@Index(name="name_x", columns={"id", "name"})})
 * @ORM\Entity
 */
class Keywords
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OrderBy({"name" = "DESC"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="File", inversedBy="keywords", indexBy="id",cascade={"persist"})
     * @ORM\JoinTable(name="sm_file_keywords")
     */
    private $file;

    public function __construct($name = null) {
        $this->name = $name;
        $this->file = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    public function addFile(File $file){
        if (!$this->file->contains($file)) {
            $this->file[] = $file;
        }
    }
    /**
     * Remove keyword
     */
    public function removeFile($file)
    {
        if ($this->file->contains($file)) {
            $this->file->removeElement($file);
        }

    }



}