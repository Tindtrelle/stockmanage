<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * FolderDetails
 *
 * @ORM\Table(name="sm_folder_details")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FolderDetailsRepository")
 */
class FolderDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /***
     * @ORM\OneToOne(targetEntity="Folder", inversedBy="details")
     */
    private $folder;
    /**
     * @var string
     *
     * @ORM\Column(name="photos_number", type="string", length=55, nullable=true)
     */
    private $photosNumber;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="shooting_date", type="datetime")
     */
    protected $shootingDate;
    /**
     * @ORM\ManyToOne(targetEntity="Agency")
     * @ORM\JoinColumn(name="agency_id", referencedColumnName="id",  onDelete="SET NULL")
     **/
    private $agency;

    /**
     * @param mixed $agency
     */
    public function setAgency($agency)
    {
        $this->agency = $agency;
    }

    /**
     * @return mixed
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * @param mixed $folder
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    /**
     * @return mixed
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $photosNumber
     */
    public function setPhotosNumber($photosNumber)
    {
        $this->photosNumber = $photosNumber;
    }

    /**
     * @return string
     */
    public function getPhotosNumber()
    {
        return $this->photosNumber;
    }

    /**
     * @param \DateTime $shootingDate
     */
    public function setShootingDate($shootingDate)
    {
        $this->shootingDate = $shootingDate;
    }

    /**
     * @return \DateTime
     */
    public function getShootingDate()
    {
        return $this->shootingDate;
    }



}