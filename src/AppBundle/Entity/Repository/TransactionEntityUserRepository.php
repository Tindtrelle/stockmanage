<?php

namespace AppBundle\Entity\Repository;
use Doctrine\ORM\EntityRepository;
//use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\Query;
/**
 * TransactionntityUserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TransactionEntityUserRepository extends EntityRepository
{
    public function getByUser($user){
        $query = $this->createQueryBuilder('tu')->select('DISTINCT tu');
        $query->where('tu.user = (:user)');
        $query->orderBy("tu.id", 'DESC');
        $query->setParameter('user', $user->getId());
        return $query->getQuery()->getResult();
    }
}