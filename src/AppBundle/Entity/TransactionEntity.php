<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="sm_transaction_entity")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\TransactionEntityRepository")
 */
class TransactionEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     *
     */
    protected $name;


    /**
     * @ORM\ManyToOne(targetEntity="TransactionEntityCategory", cascade={"persist"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $category;

    /**
     * @var string
     * @ORM\Column(name="currency", type="string", nullable = true)
     *
     */
    protected $currency;

    /**
     *
     * [income-source,income-destination,transfer-source,transfer-destination,expense-source,expense-destination]
     * @var string
     * @ORM\Column(name="position", type="array", nullable = true)
     *
     */
    protected $position;

    /**
     * @var float
     * @ORM\Column(name="balance", type="float", nullable=true)
     */
    private $balance;
    /**
     * @var float
     * @ORM\Column(name="start_balance", type="float", nullable=true)
     */
    private $startBalance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=true)
     */
    protected $dateCreated;

    /**
     * @ORM\ManyToOne(targetEntity="User", cascade={"persist"})
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=true,  onDelete="CASCADE")
     **/
    private $owner;
    /**
     * Used to check is account parent
     * @var boolean
     * @ORM\Column(name="entity_parent", type="boolean", nullable=true)
     */
    protected $parent;
    /**
     * Used to check is statement displayed
     * @var boolean
     * @ORM\Column(name="display_statement", type="boolean", nullable=true)
     */
    protected $displayStatement;
    /**
     * @ORM\ManyToOne(targetEntity="TransactionEntity", cascade={"persist"})
     */
    protected $parentEntity;
    /**
     * @ORM\OneToMany(targetEntity="TransactionEntityUser", mappedBy="transactionEntity", cascade={"persist","remove"})
     **/
    private $users;

    private $childEntities;


    public function __construct($name, TransactionEntityCategory $category = null, $currency = null, $position = array())
    {
        $this->transactionType = array();
        $this->name = $name;
        $this->currency = $currency;
        $this->position = $position;
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }


    public function hasPosition($position)
    {
        return in_array(strtoupper($position), $this->getPosition(), true);
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @param boolean $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return boolean
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param boolean $parent
     */
    public function setdisplatStatement($displayStatement)
    {
        $this->displayStatement = $displayStatement;
    }

    /**
     * @return boolean
     */
    public function getdisplayStatement()
    {
        return $this->displayStatement;
    }

    /**
     * @param mixed $parentEntity
     */
    public function setParentEntity($parentEntity)
    {
        $this->parentEntity = $parentEntity;
    }

    /**
     * @return mixed
     */
    public function getParentEntity()
    {
        return $this->parentEntity;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $childEntities
     */
    public function setChildEntities($childEntities)
    {
        $this->childEntities = $childEntities;
    }

    /**
     * @return mixed
     */
    public function getChildEntities()
    {
        return $this->childEntities;
    }

    /**
     * @return mixed
     */
    public function getStartBalance()
    {
        return $this->startBalance;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }


}