<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="sm_studio")
 * @ORM\Entity()
 */
class Studio
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     *
     */
    protected $name;

	/**
	 * @ORM\OneToMany(targetEntity="TransactionConnection", mappedBy="studioId")
	 **/
	protected $transactions;

    public function __construct($name = null) {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * Add transaction
     *
     * @param \AppBundle\Entity\TransactionConnection $transaction
     *
     * @return Studio
     */
    public function addTransaction(\AppBundle\Entity\TransactionConnection $transaction)
    {
        $this->transactions[] = $transaction;

        return $this;
    }

    /**
     * Remove transaction
     *
     * @param \AppBundle\Entity\TransactionConnection $transaction
     */
    public function removeTransaction(\AppBundle\Entity\TransactionConnection $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }
}