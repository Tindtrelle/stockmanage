<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="sm_folder")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FolderRepository")
 */
class Folder
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     *
     */
    protected $name;
    /**
     * @ORM\OneToOne(targetEntity="FolderDetails", mappedBy="folder", cascade={"persist"})
     * @ORM\JoinColumn(name="details_id", referencedColumnName="id")
     **/
    private $details;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $dateCreated;

    /**
     * @ORM\OneToMany(targetEntity="FolderUser", mappedBy="folder", cascade={"persist","remove"})
     **/
    private $users;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable = true)
     *
     */
    protected $description;

    /**
     * @var string
     * @ORM\Column(name="collection", type="text", nullable = true)
     *
     */
    protected $collection;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=true, unique=false)
     */
    private $company;
    /**
     * @ORM\OneToOne(targetEntity="TransactionEntity")
     *
     * @ORM\JoinColumn(name="transaction_entity_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $transactionEntity;

	/**
	 * @ORM\OneToMany(targetEntity="TransactionConnection", mappedBy="folderId")
	 **/
	protected $transactions;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="internal_date_created", type="datetime", nullable=true)
     */
    protected $internalDateCreated;
    /**
     * @ORM\ManyToOne(targetEntity="FolderStatus")
     * @ORM\JoinColumn(name="status", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $status;

    protected $files = 0;
    protected $videoFiles = 0;

    protected $fileUsers;

    private $expenses;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @return string
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param string $collection
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getFileUsers()
    {
        return $this->fileUsers;
    }

    /**
     * @param mixed $fileUsers
     */
    public function setFileUsers($fileUsers)
    {
        $this->fileUsers = $fileUsers;
    }

    /**
     * @return \DateTime
     */
    public function getInternalDateCreated()
    {
        return $this->internalDateCreated;
    }

    /**
     * @param \DateTime $internalDateCreated
     */
    public function setInternalDateCreated($internalDateCreated)
    {
        $this->internalDateCreated = $internalDateCreated;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @param mixed $transactionEntity
     */
    public function setTransactionEntity($transactionEntity)
    {
        $this->transactionEntity = $transactionEntity;
    }

    /**
     * @return mixed
     */
    public function getTransactionEntity()
    {
        return $this->transactionEntity;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $details
     */
    public function setDetails($details)
    {
        $this->details = $details;
    }

    /**
     * @return mixed
     */
    public function getDetails()
    {
        return $this->details;
    }
    /**
     * @param mixed $expenses
     */
    public function setExpenses($expenses)
    {
        $this->expenses = $expenses;
    }

    /**
     * @return mixed
     */
    public function getExpenses()
    {
        return $this->expenses;
    }

    /**
     * @return int
     */
    public function getVideoFiles()
    {
        return $this->videoFiles;
    }

    /**
     * @param int $videoFiles
     */
    public function setVideoFiles($videoFiles)
    {
        $this->videoFiles = $videoFiles;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->transactions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\FolderUser $user
     *
     * @return Folder
     */
    public function addUser(\AppBundle\Entity\FolderUser $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\FolderUser $user
     */
    public function removeUser(\AppBundle\Entity\FolderUser $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Add transaction
     *
     * @param \AppBundle\Entity\TransactionConnection $transaction
     *
     * @return Folder
     */
    public function addTransaction(\AppBundle\Entity\TransactionConnection $transaction)
    {
        $this->transactions[] = $transaction;

        return $this;
    }

    /**
     * Remove transaction
     *
     * @param \AppBundle\Entity\TransactionConnection $transaction
     */
    public function removeTransaction(\AppBundle\Entity\TransactionConnection $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }
}