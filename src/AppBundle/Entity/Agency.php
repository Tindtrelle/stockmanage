<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="sm_agency")
 * @ORM\Entity()
 */
class Agency
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     *
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string")
     *
     */
    protected $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $dateCreated;
    /**
     * @ORM\OneToOne(targetEntity="TransactionEntity")
     *
     * @ORM\JoinColumn(name="transaction_entity_id", referencedColumnName="id")
     */
    protected $transactionEntity;

    public function __construct($name = null, $slug = null) {
        $this->name = $name;
        $this->slug = $slug;
        $this->dateCreated = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @param mixed $transactionEntity
     */
    public function setTransactionEntity($transactionEntity)
    {
        $this->transactionEntity = $transactionEntity;
    }

    /**
     * @return mixed
     */
    public function getTransactionEntity()
    {
        return $this->transactionEntity;
    }

}