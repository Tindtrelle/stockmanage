<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="sm_user")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 */
class User
{
    const ROLE_DEFAULT = 'ROLE_USER';
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected  $id;

    /**
     * @var string
     * @ORM\Column(name="username", type="string", nullable=true)
     *
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", nullable=true)
     */
    private $lastName;

    /**
     * Note: because of internal team members email can be null
     * @var string
     * @ORM\Column(name="email", type="string", nullable=true)
     *
     */
    protected $email;

    /**
     * @var boolean
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;


    /**
     * The salt to use for hashing
     *
     * @var string
     * @ORM\Column(name="salt", type="string")
     */
    protected $salt;

    /**
     * Encrypted password. Must be persisted.
     * @var string
     * @ORM\Column(name="password", type="string", nullable=true)
     */
    protected $password;

    /**
     * Random string sent to the user email address in order to verify it
     * @var string
     * @ORM\Column(name="confirmation_token", type="string", nullable=true)
     */
    protected $confirmationToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * @ORM\Column(type="array", name="roles", nullable=true)
     */
    protected $roles;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=true, unique=false)
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="Company",  cascade={"persist","remove"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=true, unique=false)
     */
    private $company;

    /**
     * @var string
     * @ORM\Column(name="internal_id", type="string", nullable=true, unique=true)
     *
     */
    protected $internalId;

    /**
     * @ORM\Column(type="array", name="internal_roles", nullable=true)
     */
    protected $internalRoles;

    /**
     * This type of account can not login.
     * @var boolean
     * @ORM\Column(name="internal_account", type="boolean", nullable=true)
     */
    protected $internalAccount;

    /**
     * @ORM\OneToMany(targetEntity="UserAgency", mappedBy="user", cascade={"persist","remove"})
     **/
    protected $agencies;

	/**
	 * @ORM\OneToMany(targetEntity="TransactionConnection", mappedBy="userId")
	 **/
    protected $transactions;


    public function __construct()
    {
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->enabled = true;
        $this->roles = array();
        $this->internalRoles = array();
        $this->agencies = new ArrayCollection();
    }
    /**
     * ROLES START
     */
    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }
        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * Returns the user roles
     *
     * @return array The roles
     */
    public function getRoles()
    {
        $roles = $this->roles;
        //we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    public function setRoles($roles) {
        $this->roles = $roles;
    }

    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param string $confirmationToken
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getInternalId()
    {
        return $this->internalId;
    }

    /**
     * @param string $internalId
     */
    public function setInternalId($internalId)
    {
        $this->internalId = $internalId;
    }

    /**
     * @return mixed
     */
    public function getInternalRoles()
    {
        return $this->internalRoles;
    }

    /**
     * @param mixed $internalRoles
     */
    public function setInternalRoles($internalRoles)
    {
        $this->internalRoles = $internalRoles;
    }

    /**
     * @return mixed
     */
    public function getInternalAccount()
    {
        return $this->internalAccount;
    }

    /**
     * @param mixed $internalAccount
     */
    public function setInternalAccount($internalAccount)
    {
        $this->internalAccount = $internalAccount;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getAgencies()
    {
        return $this->agencies;
    }

    /**
     * @param mixed $agencies
     */
    public function setAgencies($agencies)
    {
        $this->agencies = $agencies;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add agency
     *
     * @param \AppBundle\Entity\UserAgency $agency
     *
     * @return User
     */
    public function addAgency(\AppBundle\Entity\UserAgency $agency)
    {
        $this->agencies[] = $agency;

        return $this;
    }

    /**
     * Remove agency
     *
     * @param \AppBundle\Entity\UserAgency $agency
     */
    public function removeAgency(\AppBundle\Entity\UserAgency $agency)
    {
        $this->agencies->removeElement($agency);
    }

    /**
     * Add transaction
     *
     * @param \AppBundle\Entity\TransactionConnection $transaction
     *
     * @return User
     */
    public function addTransaction(\AppBundle\Entity\TransactionConnection $transaction)
    {
        $this->transactions[] = $transaction;

        return $this;
    }

    /**
     * Remove transaction
     *
     * @param \AppBundle\Entity\TransactionConnection $transaction
     */
    public function removeTransaction(\AppBundle\Entity\TransactionConnection $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }
}
