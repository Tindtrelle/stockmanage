<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use VMelnik\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 *
 * @ORM\Table(name="sm_user_agency")
 * @ORM\Entity
 */
class UserAgency
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="agencies")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Agency")
     * @ORM\JoinColumn(name="agency_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $agency;

    /**
     * @var integer
     * @ORM\Column(name="order_number", type="decimal", nullable=true)
     */
    private $orderNumber;

    /**
     * @var boolean
     * @ORM\Column(name="hidden", type="boolean", nullable=true)
     */
    private $hidden = false;

    /**
     * @var string
     * @ORM\Column(name="ftp_server", type="string", nullable=true)
     * @Encrypted
     *
     */
    protected $ftpServer = "";

    /**
     * @var string
     * @ORM\Column(name="ftp_user", type="string", nullable=true)
     * @Encrypted
     *
     */
    protected $ftpUser = "";

    /**
     * @var string
     * @ORM\Column(name="ftp_pass", type="string", nullable=true)
     * @Encrypted
     *
     */
    protected $ftpPass = "";

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * @param mixed $agency
     */
    public function setAgency($agency)
    {
        $this->agency = $agency;
    }

    /**
     * @return int
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param int $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return boolean
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * @param boolean $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @return string
     */
    public function getFtpServer()
    {
        return $this->ftpServer;
    }

    /**
     * @param string $ftpServer
     */
    public function setFtpServer($ftpServer)
    {
        $this->ftpServer = $ftpServer;
    }

    /**
     * @return string
     */
    public function getFtpUser()
    {
        return $this->ftpUser;
    }

    /**
     * @param string $ftpUser
     */
    public function setFtpUser($ftpUser)
    {
        $this->ftpUser = $ftpUser;
    }

    /**
     * @return string
     */
    public function getFtpPass()
    {
        return $this->ftpPass;
    }

    /**
     * @param string $ftpPass
     */
    public function setFtpPass($ftpPass)
    {
        $this->ftpPass = $ftpPass;
    }

}