<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\SerializedName;

/**
 * User
 *
 * @ORM\Table(name="sm_transaction_connection")
 * @ORM\Entity()
 */
class TransactionConnection
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Transaction", inversedBy="connection")
     * @ORM\JoinColumn(name="transaction_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $transaction;

	/**
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="transactions")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 */
    private $userId;

	/**
	 * @ORM\ManyToOne(targetEntity="Folder", inversedBy="transactions")
	 * @ORM\JoinColumn(name="folder_id", referencedColumnName="id")
	 */
    private $folderId;

	/**
	 * @ORM\ManyToOne(targetEntity="Studio", inversedBy="transactions")
	 * @ORM\JoinColumn(name="studio_id", referencedColumnName="id")
	 */
    private $studioId;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }



    /**
     * Set userId
     *
     * @param \AppBundle\Entity\User $userId
     *
     * @return TransactionConnection
     */
    public function setUserId(\AppBundle\Entity\User $userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set folderId
     *
     * @param \AppBundle\Entity\Folder $folderId
     *
     * @return TransactionConnection
     */
    public function setFolderId(\AppBundle\Entity\Folder $folderId = null)
    {
        $this->folderId = $folderId;

        return $this;
    }

    /**
     * Get folderId
     *
     * @return \AppBundle\Entity\Folder
     */
    public function getFolderId()
    {
        return $this->folderId;
    }

    /**
     * Set studioId
     *
     * @param \AppBundle\Entity\Studio $studioId
     *
     * @return TransactionConnection
     */
    public function setStudioId(\AppBundle\Entity\Studio $studioId = null)
    {
        $this->studioId = $studioId;

        return $this;
    }

    /**
     * Get studioId
     *
     * @return \AppBundle\Entity\Studio
     */
    public function getStudioId()
    {
        return $this->studioId;
    }
}