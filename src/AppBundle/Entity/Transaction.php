<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="sm_transaction")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     *
     */
    protected $name;

    /**
     * Expense, income, transfer
     * @var string
     * @ORM\Column(name="type", type="string")
     *
     */
    protected $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime")
     */
    protected $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var string
     * @ORM\Column(name="source_currency", type="string", nullable = true)
     *
     */
    protected $sourceCurrency;

    /**
     * @ORM\ManyToOne(targetEntity="TransactionEntity", cascade={"persist"})
     * @ORM\JoinColumn(name="source_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $source;

    /**
     * @var float
     * @ORM\Column(name="source_amount", type="float", nullable=true)
     */
    private $sourceAmount;

    /**
     * @var string
     * @ORM\Column(name="destination_currency", type="string", nullable = true)
     *
     */
    protected $destinationCurrency;

    /**
     * @ORM\ManyToOne(targetEntity="TransactionEntity", cascade={"persist"})
     * @ORM\JoinColumn(name="destination_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $destination;

    /**
     * @var float
     * @ORM\Column(name="destination_amount", type="float", nullable=true)
     */
    private $destinationAmount;

    /**
     * Default amount should always be save in USD
     * @var float
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="TransactionStatus")
     * @ORM\JoinColumn(name="status", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $status;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable = true)
     *
     */
    protected $description;

    /**
     * @var string
     * @ORM\Column(name="statement", type="text", nullable = true)
     *
     */
    private $statement;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $owner;
    /**
     * @var float
     * @ORM\Column(name="current_balance_source", type="float", nullable=true)
     */
    private $currentBalanceSource;
    /**
     * @var float
     * @ORM\Column(name="current_balance_destination", type="float", nullable=true)
     */
    private $currentBalanceDestination;

    /**
     * @ORM\Column(name="archive", type="boolean", options={"default":0})
     */
    private $archive = false;

    /**
     * Applies only if transaction is transfer
     * @var float
     * @ORM\Column(name="transfer_cost", type="float", nullable=true)
     */
    private $transferCost;
    /**
     * @ORM\OneToMany(targetEntity="TransactionConnection", mappedBy="transaction", cascade={"persist","remove"})
     **/
    private $connection;

    private $studios;

    private $shoots;

    private $usersSalary;



    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getSourceCurrency()
    {
        return $this->sourceCurrency;
    }

    /**
     * @param mixed $sourceCurrency
     */
    public function setSourceCurrency($sourceCurrency)
    {
        $this->sourceCurrency = $sourceCurrency;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param mixed $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return float
     */
    public function getSourceAmount()
    {
        return $this->sourceAmount;
    }

    /**
     * @param float $sourceAmount
     */
    public function setSourceAmount($sourceAmount)
    {
        $this->sourceAmount = $sourceAmount;
    }

    /**
     * @return mixed
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param mixed $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return mixed
     */
    public function getDestinationCurrency()
    {
        return $this->destinationCurrency;
    }

    /**
     * @param mixed $destinationCurrency
     */
    public function setDestinationCurrency($destinationCurrency)
    {
        $this->destinationCurrency = $destinationCurrency;
    }

    /**
     * @return float
     */
    public function getDestinationAmount()
    {
        return $this->destinationAmount;
    }

    /**
     * @param float $destinationAmount
     */
    public function setDestinationAmount($destinationAmount)
    {
        $this->destinationAmount = $destinationAmount;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getStatement()
    {
        return $this->statement;
    }

    /**
     * @param string $statement
     */
    public function setStatement($statement)
    {
        $this->statement = $statement;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getTransferCost()
    {
        return $this->transferCost;
    }

    /**
     * @param float $transferCost
     */
    public function setTransferCost($transferCost)
    {
        $this->transferCost = $transferCost;
    }

    /**
     * @param mixed $connection
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        return $this->connection;
    }
        /**
     * @param mixed $shoots
     */
    public function setShoots($shoots)
    {
        $this->shoots = $shoots;
    }/**
     * @return mixed
     */
    public function getShoots()
    {
        return $this->shoots;
    }/**
     * @param mixed $studios
     */
    public function setStudios($studios)
    {
        $this->studios = $studios;
    }/**
     * @return mixed
     */
    public function getStudios()
    {
        return $this->studios;
    }

    /**
     * @param mixed $usersSalary
     */
    public function setUsersSalary($usersSalary)
    {
        $this->usersSalary = $usersSalary;
    }

    /**
     * @return mixed
     */
    public function getUsersSalary()
    {
        return $this->usersSalary;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getCurrentBalanceSource()
    {
        return $this->currentBalanceSource;
    }

    /**
     * @param float $currentBalanceSource
     */
    public function setCurrentBalanceSource($currentBalanceSource)
    {
        $this->currentBalanceSource = $currentBalanceSource;
    }

    /**
     * @return float
     */
    public function getCurrentBalanceDestination()
    {
        return $this->currentBalanceDestination;
    }

    /**
     * @param float $currentBalanceDestination
     */
    public function setCurrentBalanceDestination($currentBalanceDestination)
    {
        $this->currentBalanceDestination = $currentBalanceDestination;
    }




    /**
     * Set archive
     *
     * @param boolean $archive
     *
     * @return Transaction
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;

        return $this;
    }

    /**
     * Get archive
     *
     * @return boolean
     */
    public function getArchive()
    {
        return $this->archive;
    }

    /**
     * Add connection
     *
     * @param \AppBundle\Entity\TransactionConnection $connection
     *
     * @return Transaction
     */
    public function addConnection(\AppBundle\Entity\TransactionConnection $connection)
    {
        $this->connection[] = $connection;

        return $this;
    }

    /**
     * Remove connection
     *
     * @param \AppBundle\Entity\TransactionConnection $connection
     */
    public function removeConnection(\AppBundle\Entity\TransactionConnection $connection)
    {
        $this->connection->removeElement($connection);
    }
}
