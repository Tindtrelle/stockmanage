<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TransactionEntityUser
 *
 * @ORM\Table(name="sm_transaction_entity_user")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\TransactionEntityUserRepository")
 */
class TransactionEntityUser
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="TransactionEntity", inversedBy="users")
     * @ORM\JoinColumn(name="transaction_entity_id", referencedColumnName="id",  onDelete="CASCADE")
     **/
    private $transactionEntity;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     **/
    private $user;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $transactionEntity
     */
    public function setTransactionEntity($transactionEntity)
    {
        $this->transactionEntity = $transactionEntity;
    }

    /**
     * @return mixed
     */
    public function getTransactionEntity()
    {
        return $this->transactionEntity;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }


}