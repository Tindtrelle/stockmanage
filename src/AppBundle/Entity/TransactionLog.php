<?php
/**
 * Created by PhpStorm.
 * User: Constantine
 * Date: 04-Jul-17
 * Time: 22:24
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction Log
 *
 * @ORM\Table(name="sm_transaction_log")
 * @ORM\Entity
 */
class TransactionLog {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Transaction",cascade={"persist"})
	 * @ORM\JoinColumn(name="transaction_id", referencedColumnName="id", onDelete="CASCADE", unique=false)
	 **/
	private $transaction;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_created", type="datetime")
	 */
	private $dateCreated;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="action", type="text", nullable=true)
	 */
	private $action;

	/**
	 * @ORM\ManyToOne(targetEntity="User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
	 **/
	private $user;

	/**
	 * @var float
	 * @ORM\Column(name="source_amount", type="float", nullable=true)
	 */
	private $sourceAmount;

	/**
	 * @var float
	 * @ORM\Column(name="destination_amount", type="float", nullable=true)
	 */
	private $destinationAmount;

	/**
	 * @var float
	 * @ORM\Column(name="transaction_amount", type="float", nullable=true)
	 */
	private $transactionAmount;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return TransactionLog
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return TransactionLog
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set sourceAmount
     *
     * @param float $sourceAmount
     *
     * @return TransactionLog
     */
    public function setSourceAmount($sourceAmount)
    {
        $this->sourceAmount = $sourceAmount;

        return $this;
    }

    /**
     * Get sourceAmount
     *
     * @return float
     */
    public function getSourceAmount()
    {
        return $this->sourceAmount;
    }

    /**
     * Set destinationAmount
     *
     * @param float $destinationAmount
     *
     * @return TransactionLog
     */
    public function setDestinationAmount($destinationAmount)
    {
        $this->destinationAmount = $destinationAmount;

        return $this;
    }

    /**
     * Get destinationAmount
     *
     * @return float
     */
    public function getDestinationAmount()
    {
        return $this->destinationAmount;
    }

    /**
     * Set transactionAmount
     *
     * @param float $transactionAmount
     *
     * @return TransactionLog
     */
    public function setTransactionAmount($transactionAmount)
    {
        $this->transactionAmount = $transactionAmount;

        return $this;
    }

    /**
     * Get transactionAmount
     *
     * @return float
     */
    public function getTransactionAmount()
    {
        return $this->transactionAmount;
    }

    /**
     * Set transaction
     *
     * @param \AppBundle\Entity\Transaction $transaction
     *
     * @return TransactionLog
     */
    public function setTransaction(\AppBundle\Entity\Transaction $transaction = null)
    {
        $this->transaction = $transaction;

        return $this;
    }

    /**
     * Get transaction
     *
     * @return \AppBundle\Entity\Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return TransactionLog
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
