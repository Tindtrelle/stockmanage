<?php

namespace AppBundle\DataFixtures;
use AppBundle\Entity\Agency;
use AppBundle\Entity\Studio;
use AppBundle\Entity\AgencyStatus;
use AppBundle\Entity\FolderStatus;
use AppBundle\Entity\FileMetadata;
use AppBundle\Entity\Keywords;
use AppBundle\Entity\File;
use AppBundle\Entity\Metadata;
use AppBundle\Entity\TransactionStatus;
use AppBundle\Entity\TransactionEntity;
use AppBundle\Entity\TransactionEntityCategory;
use AppBundle\Entity\User;
use AppBundle\Entity\UserAgency;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;


class loadData extends AbstractFixture
{

    /** {@inheritDoc} */
    public function load(ObjectManager $manager)
    {
        ini_set('xdebug.max_nesting_level', -1);
        #$files  = $manager->getRepository('AppBundle:File')->findBy(array(),array(), 1500, 570);
        $counter = 0;
        $batchSize = 20;
        $files  = $manager->getRepository('AppBundle:File')->findAll();
        $keywordRepository = $manager->getRepository('AppBundle:Keywords');
        foreach ($files as $file){
            $keywords = explode(",", $file->getMetadata()->getKeywords());
            foreach ($keywords as $keyword){
                $counter ++ ;
                $name = trim($keyword);
                if(empty($name)){
                    continue;
                }
                $keyword = $keywordRepository->findOneBy(array('name'=>$name));
                if(!isset($keyword)){
                    $keyword = new Keywords($name);
                    $manager->persist($keyword);
                    $manager->flush();
                }
                if(!empty($name)){
                    $file->addKeyword($keyword);
                    $manager->persist($file);
                    $keyword->addFile($file);
                    $manager->persist($keyword);

                }
                if (($counter % $batchSize) === 0) {
                    $manager->flush();
                }
                echo $counter . "\n";

            }
            echo "new file \n";
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }

}