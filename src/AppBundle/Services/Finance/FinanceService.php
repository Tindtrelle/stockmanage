<?php
namespace AppBundle\Services\Finance;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\TransactionEntity;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FinanceService
{
    protected $em;
    protected $container;
    
    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }
    
    /**
     * Update balance
     * @param Transaction $transaction
     * @return array
     */
    public function updateBalance(Transaction $transaction, $deleting=false) {
        $source = $transaction->getSource();
        $destination = $transaction->getDestination();
	    $user = $this->container->get("s_user")->getUser();
        // update source balance
        if($source->getCategory()->getSlug() == "account") {
            if(strtolower($source->getCurrency()) == strtolower($transaction->getSourceCurrency())) {
                $balance = $source->getBalance();
                $sourceAmount = (float) $transaction->getSourceAmount();
                if($deleting){
                    $source->setBalance($balance + $sourceAmount);
                } else{
                    $source->setBalance($balance - $sourceAmount);
                }
                // Save USD
                $amountUsd = $this->container->get('s_currency')->convertToUsd($sourceAmount,$transaction->getSourceCurrency());
                $transaction->setAmount($amountUsd);
                $transaction->setCurrentBalanceSource($source->getBalance());
            } else {
                return $this->container->get("s_controller")->createResponse(false,"Source currency is not allowed");
            }
        }
        // Update destination balance
        if($destination->getCategory()->getSlug() == "account") {
            if(strtolower($destination->getCurrency()) == strtolower($transaction->getDestinationCurrency())) {
                $balance = $destination->getBalance();
                $destAmount = (float) $transaction->getDestinationAmount();
                if($deleting){
                    $destination->setBalance($balance - $destAmount);
                } else{
                    $destination->setBalance($balance + $destAmount);
                }
                // Save USD
                $amountUsd = $this->container->get('s_currency')->convertToUsd($destAmount,$transaction->getDestinationCurrency());
                $transaction->setAmount($amountUsd);
                $transaction->setCurrentBalanceDestination($destination->getBalance());
            } else {
                return $this->container->get("s_controller")->createResponse(false,"Destination currency is not allowed");
            }
        }

	    //Log transaction
	    $this->container->get("s_transaction_log")->insertTransactionLog($user, $transaction,
		    [ "action"=>"update-balance", ]
	    );

        return $this->container->get("s_controller")->createResponse(true,"Balance updated");
    }

    public function getAdditionalDataEntity(TransactionEntity $transactionEntity) {
        $transactionId = $transactionEntity->getId();
        // Child Entities
        $childEntities = $this->em->getRepository('AppBundle:TransactionEntity')->findBy(array("parentEntity" => $transactionId), array('name' => 'ASC'));
        $transactionEntity->setChildEntities($childEntities);
        return $transactionEntity;
    }
}