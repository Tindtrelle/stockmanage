<?php
namespace AppBundle\Services\Image;
use AppBundle\Entity\File;
use AppBundle\Entity\FileUser;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImageService
{
    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;

    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function getImageThumb(File $file)
    {
        $originalPath = $file->getPath();
        $originalPathArr = explode("/",$originalPath);
        $lastIndex = count($originalPathArr)-1;
        $fileName = $originalPathArr[$lastIndex];
        $originalPathArr[$lastIndex] = "thumb/".$fileName;
        return implode("/",$originalPathArr);
    }

    public function getImageThumbKey(File $file) {
        $originalKey= $file->getAwsKey();
        $originalKeyArr = explode("/",$originalKey);
        $lastIndex = count($originalKeyArr)-1;
        $fileName = $originalKeyArr[$lastIndex];
        $originalKeyArr[$lastIndex] = "thumb/".$fileName;
        return implode("/",$originalKeyArr);
    }

    /**
     * @param File $file
     * @return int
     * Define image editors from name
     * Patern: LI20161023__mg_1234_p03_r22 ( custom for Lumina Studio )
     *
     */
    public function setImageUsersFromName(File $file) {
        $fileFullName = $file->getName();
        if(!isset($fileFullName)) {
            return 0;
        }
        $owner = $this->container->get('s_user')->getUser();
        $company = $owner->getCompany() ? $owner->getCompany() : null;

        $fileNameArr = explode(".",$fileFullName);
        $fileName = $fileNameArr[0];
        // FIRST USER ( photographer )
        $oldPhotographer = $this->em->getRepository("AppBundle:FileUser")->findOneBy(["file"=>$file,"role"=>"photographer"]);
        $photographer= $oldPhotographer ? $oldPhotographer : new FileUser();
        $p = strpos(strtolower($fileName),"p");
        if($p) { // Check if there is another p
            $p2 = strpos(strtolower($fileName),"p",$p+1);
            if($p2) { $p = $p2; }
            if(isset($fileName[$p+1]) && isset($fileName[$p+2])) {
                $internalId = $fileName[$p+1].$fileName[$p+2]; // INTERNAL ID
                $user1 = $this->em->getRepository("AppBundle:User")->findOneBy(["internalId"=>$internalId,"company"=>$company]);
                if($user1) {
                    $photographer->setUser($user1);
                } else {
                    $photographer->setUser(null);
                }
            }
        }
        $photographer->setFile($file);
        $photographer->setRole("photographer");
        $file->addUser($photographer);

        // SECOND USER ( retoucher )
        $oldRetoucher = $this->em->getRepository("AppBundle:FileUser")->findOneBy(["file"=>$file,"role"=>"retoucher"]);
        $retoucher = $oldRetoucher ? $oldRetoucher : new FileUser();
        $c = strpos(strtolower($fileName),"r");
        if($c) { // Check if there is another r
            $c2 = strpos(strtolower($fileName),"r",$c+1);
            if($c2) { $c = $c2; }
            if(isset($fileName[$c+1]) && isset($fileName[$c+2])) {
                $internalId = $fileName[$c+1].$fileName[$c+2]; // INTERNAL ID
                $user2 = $this->em->getRepository("AppBundle:User")->findOneBy(["internalId"=>$internalId,"company"=>$company]);
                if($user2) {
                    $retoucher->setUser($user2);
                } else {
                    $retoucher->setUser(null);
                }
            }
        }
        $retoucher->setFile($file);
        $retoucher->setRole("retoucher");
        $file->addUser($retoucher);

        return $file;
    }

    /**
     * @param File $file
     * @return int
     * Define image date from name
     * Pattern: LI20161023__mg_1234_p03_r22 ( custom for Lumina Studio )
     *
     */
    public function setImageDateFromName(File $file) {
        $fileName = $file->getName();
        if(!isset($fileName)) {
            return 0;
        }
        $year = substr($fileName, 2, 4);
        $month = substr($fileName, 6, 2);
        $day = substr($fileName, 8, 2);
        $datetime = \DateTime::createFromFormat("d/m/Y",$day."/".$month."/".$year);
        if($datetime) {
            $file->setInternalDateCreated($datetime);
        }

        return $file;
    }

    /**
     * File names should be unique for Lumina
     * @param $file
     * @return bool
     */
    public function isUniqueFileName($file) {
        $filename = str_replace(" ","_",$file->getClientOriginalName());
        $fileFound = $this->em->getRepository("AppBundle:File")->findOneByName($filename);
        if($fileFound) {
            return false;
        }
        return true;
    }

    /**
     * File names should be unique for Lumina
     * @param $filename
     * @return bool
     */
    public function isUniqueFileNameByName($filename) {
        $filename = str_replace(" ","_",$filename);
        $fileFound = $this->em->getRepository("AppBundle:File")->findOneByName($filename);
        if($fileFound) {
            return false;
        }
        return true;
    }

    /**
     * @param $file
     */
    public function setMetaTitle(File $file) {
        $title = $file->getMetadata()->getTitle();
        exec('iptc -m ObjectName -v "'.$title.'" '.$file->getSource());
    }

    public function setMetaDescription(File $file) {
        $description = $file->getMetadata()->getDescription();

        exec('iptc -m Caption -v "'.$description.'" '.$file->getSource());

    }

    public function setMetaKeyword(File $file) {
        exec('iptc -d Keywords:all '.$file->getSource());
        $keywords = $file->getMetadata()->getKeywords();
        $keywordsArr = explode(",",$keywords);
        foreach($keywordsArr as $keyword) {
            exec('iptc -a Keywords -v "'.$keyword.'" '.$file->getSource());
        }
    }
    
    public function setMetaData($metadata, $filePath) {
        if(isset($metadata["title"])) {
            exec('iptc -m ObjectName -v "'.$metadata["title"].'" '.$filePath);
        }
        
        if(isset($metadata["description"])) {
            exec('iptc -m Caption -v "'.$metadata["description"].'" '.$filePath);

        }
        
        if(isset($metadata["keywords"])) {
            exec('iptc -d Keywords:all '.$filePath);
            $keywordsArr = explode(",",$metadata["keywords"]);
            foreach($keywordsArr as  $keyword) {
                exec('iptc -a Keywords -v "'.$keyword.'" '.$filePath);
            }
        }
    }

    /**
     * File path
     * @param $file
     * @return array
     */
    public function getMetaData($file) {
        $metadata = array();
        $keywords = array();
        $output = array();
        exec('iptc '.$file,$output);
        foreach($output as $line) {
            if(strpos($line,"Object Name") !== false) {
                $metadata["title"] = $this->getMetaValueFromString($line,4);
            }
            if(strpos($line,"Caption/Abstract") !== false) {
                $metadata["description"] = $this->getMetaValueFromString($line,3);
            }
            if(strpos($line,"Keywords") !== false) {
                $keywords[] = $this->getMetaValueFromString($line,3);
            }
        }
        $metadata["keywords"] = implode(",",$keywords);
        return $metadata;
    }

    /**
     *
     * @param $string
     * @param $elementNumber ( number where string starts )
     * @return mixed
     */
    protected function getMetaValueFromString($string,$elementNumber) {
        $valueArr = array();
        $lineArr = explode(" ",$string);

        // Remove empty spaces
        $newArr = [];
        foreach($lineArr as $key=>$elem)
        {
            if(!empty($elem)) {
                $newArr[] = $elem;
            }
        }
        // Get value
        foreach($newArr as $key=>$elem)
        {
            if($key>$elementNumber) {
                $valueArr[] = $elem;
            }
        }

        $value = implode(" ",$valueArr);
        return $value;

    }
}