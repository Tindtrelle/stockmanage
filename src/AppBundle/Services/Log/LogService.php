<?php
namespace AppBundle\Services\Log;
use AppBundle\Entity\User;
use AppBundle\Entity\UserLog;
use Doctrine\ORM\EntityManager;

class LogService
{
    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;

    public function __construct(EntityManager $em, $container)
    {
        $this->em = $em;
        $this->container = $container;
    }
    
    public function insertUserLog(User $user, $data) {
        if(!isset($data["entityName"])) {
            return;
        }
        if(!isset($data["entityId"])) {
            return;
        }
        if(!isset($data["action"])) {
            return;
        }
        
        $log = new UserLog();
        $log->setUser($user);
        $log->setDateCreated(new \DateTime());
        $log->setEntityName($data["entityName"]);
        $log->setEntityId($data["entityId"]);
        $log->setAction($data["action"]);
        
        $this->em->persist($log);
        $this->em->flush();
    }
}