<?php
/**
 * Created by PhpStorm.
 * User: Constantine
 * Date: 04-Jul-17
 * Time: 22:41
 */

namespace AppBundle\Services\Log;

use AppBundle\Entity\User;
use AppBundle\Entity\TransactionLog;
use AppBundle\Entity\Transaction;
use Doctrine\ORM\EntityManager;

class TransactionLogService {
	/**
	 * @var EntityManager
	 */
	protected $em;

	protected $container;

	public function __construct(EntityManager $em, $container)
	{
		$this->em = $em;
		$this->container = $container;
	}

	public function insertTransactionLog(User $user, Transaction $transaction, $data) {
		if(!isset($data["action"])) {
			return;
		}

		$log = new TransactionLog();
		$log->setUser($user);
		$log->setTransaction($transaction);
		$log->setDateCreated(new \DateTime());
		$log->setAction($data["action"]);

		$log->setSourceAmount($transaction->getSourceAmount());
		$log->setDestinationAmount($transaction->getDestinationAmount());
		$log->setTransactionAmount($transaction->getAmount());

		$this->em->persist($log);
		$this->em->flush();
	}
}