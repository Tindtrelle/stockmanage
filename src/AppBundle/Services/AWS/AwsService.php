<?php
namespace AppBundle\Services\AWS;
use AppBundle\Entity\File;
use AppBundle\Entity\Metadata;
use AppBundle\Entity\Keywords;
use Aws\CommandPool;
use Aws\Exception\AwsException;
use Aws\ResultInterface;
use Aws\S3\Exception\S3Exception;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Promise\PromiseInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Aws\S3\S3Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AwsService
{

    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;

    private $s3Client;

    private $dir = "folders";

    private $bucket;

    private $tmpFolder = "uploads";
    // TMP USED FOR UPLOAD PROGRESS
    public $tmp_folder = null;
    public $upload_progress = null;
    public $commands_count = 0;
    public $current_command = 0;
    public $currentUser = null;
    public $upload_info = "";
    // TMP FOR METADATA UPDATE
    public $tmpMetaFolder = null;

    public function __construct(EntityManager $em,  $container)
    {
        $this->em = $em;
        $this->container = $container;
        $this->bucket = $this->container->getParameter("aws_bucket");
    }

    public function createAwsClient() {
        $options = [
            'region'            => 'us-west-2',
            'version'           => 'latest',
            'credentials' => [
                'key'    => $this->container->getParameter("aws_key"),
                'secret' => $this->container->getParameter("aws_secret"),
            ],
            'ssl.certificate_authority' => 'cacert.pem'
        ];
        $this->s3Client = new S3Client($options);
    }

    /**
     * @param $files
     * @param array $data
     * @param array $options
     * @return array
     */
    public function uploadMultipleFiles($files, $data = array(), $options = array()) {

        $this->currentUser = $data["currentUser"];
        if(!isset($data["folder_id"])) {
            return $this->createResponse(false,"Folder ID is missing");
        }
        $folder = $this->em->getRepository("AppBundle:Folder")->find($data["folder_id"]);
        if(!$folder) {
            return $this->createResponse(false,"Folder is missing");
        }
        $this->tmp_folder = $folder;
        if(!isset($data["folder_name"])) {
            return $this->createResponse(false,"Folder name is missing");
        }
        if(isset($data["upload_progress"])) { // Used for upload progress
            $this->upload_progress = $data["upload_progress"];
        }

        $commands = array();
        $existsStr = "";
        $fileCounter = 0;
        foreach($files as $file) {
            if(!$this->container->get("s_image")->isUniqueFileName($file)) {
                $existsStr .= $file->getClientOriginalName()." already exists<br>";
                continue;
            }
            $fileCounter++;
            $commandArr = $this->uploadFileCommand($file, $data, $options);
            foreach ($commandArr as $command) {
                $commands[] = $command;
            }
        }
        $this->commands_count = count($commands); // Used for upload progress
        // UPLOAD INFO
        $this->upload_info .= $fileCounter ." file(s) uploaded<br>";
        $this->upload_info .= $existsStr;
        
        try {
            $pool = new CommandPool($this->s3Client, $commands,[
                // Send 5 files at a time
                'concurrency' => 25,
                // Invoke this function for each successful transfer.
                'fulfilled' => function ( ResultInterface $result, $iterKey, PromiseInterface $aggregatePromise) {
                    // UPDATE UPLOAD PROGRESS
                    $this->updateUploadProgress();
                    // SAVE FILE LOCAL
                    if($result instanceof S3Exception) {
                        //var_dump($result->getMessage());
                    } else {
                        if(isset($result['ObjectURL'])) {
                            $fileS3Path = $result['ObjectURL'];
                            if(strpos($fileS3Path, '/thumb/') == false) { // DON'T SAVE THUMBS
                                $key = str_replace("https://s3-us-west-2.amazonaws.com/".$this->getBucket()."/", "", $fileS3Path);

                                $file = $this->em->getRepository("AppBundle:File")->findOneByAwsKey($key);
                                if($file) {
                                    $newFile = $file;
                                } else {
                                    $newFile = new File();
                                }
                                $pathArr = explode("/",$fileS3Path);
                                $newFile->setName($pathArr[count($pathArr)-1]);
                                $localFilePath = $this->tmpFolder."/".$newFile->getName();
                                $newFile = $this->container->get('s_image')->setImageUsersFromName($newFile);
                                $newFile = $this->container->get('s_image')->setImageDateFromName($newFile);
                                $newFile->setPath($fileS3Path);
                                $newFile->setDateCreated(new \DateTime());
                                $newFile->setFolder($this->tmp_folder);
                                $newFile->setAwsKey($key);
                                $newFile->setOwner($this->currentUser);
                                if($this->currentUser->getCompany()) {
                                    $newFile->setCompany($this->currentUser->getCompany());
                                }
                                // Metadata
                                $metadata = $this->container->get('s_image')->getMetaData($localFilePath);
                                $meta = new Metadata();
                                if(isset($metadata["title"])) {
                                    $meta->setTitle($metadata["title"]);
                                }
                                if(isset($metadata["description"])) {
                                    $meta->setDescription($metadata["description"]);
                                }
                                if(isset($metadata["keywords"])) {
                                    $meta->setKeywords($metadata["keywords"]);
                                }

                                if(isset($metadata["keywords"])) {

                                    $keywords = $metadata["keywords"];
                                    $keywordsNew = array_map('trim',explode(',', $keywords));
                                    $keywordRepository = $this->em->getRepository('AppBundle:Keywords');
                                    foreach ($keywordsNew as $keyword){
                                        $name = trim($keyword);
                                        if(empty($name)){
                                            continue;
                                        }
                                        $keyword = $keywordRepository->findOneBy(array('name'=>$name));
                                        if(!isset($keyword)){
                                            $keyword = new Keywords($name);
                                            $this->em->persist($keyword);
                                            $this->em->flush();
                                        }
                                        if(!empty($name)){
                                            $keyword->addFile($newFile);
                                            $this->em->persist($keyword);
                                        }
                                    }
                                }

                                $this->em->persist($meta);
                                $newFile->setMetadata($meta);
                                $this->em->persist($newFile);
                                $this->em->flush();
                                // Remove tmp file
                                if(file_exists($localFilePath)) {
                                    unlink($localFilePath);
                                }
                                // Create default agency status
                                $this->container->get("s_file")->setDefaultAgencyStatus($newFile);
                                // Insert log
                                $this->container->get("s_log")->insertUserLog($this->currentUser,["entityName"=>"file","entityId"=>$newFile->getId(),"action"=>"upload"]);
                            }
                        }
                    }
                },
                // Invoke this function for each failed transfer.
                'rejected' => function ( AwsException $reason, $iterKey,  PromiseInterface $aggregatePromise ) {
                     //echo "Failed: {$reason}\n";
                     //ToDo Send error email
                },
            ]);

            $promise = $pool->promise();

            try {
                $result = $promise->wait();
                return $result;
            } catch (AwsException $e) {
                //ToDo Send error email
                return $e;
            }

        } catch (AwsException $e) {
            //ToDo Send error email
            return $e;
        }
    }

    /**
     * Update uploading progress
     */
    protected function updateUploadProgress() {
        if($this->upload_progress) {
            $this->current_command++;
            $percentage = ($this->current_command*100)/$this->commands_count;
            $this->upload_progress->setValue(round($percentage,2));
            $this->em->persist($this->upload_progress);
            $this->em->flush($this->upload_progress);
        }
    }

    /**
     * Just create AWS upload commands ( DON'T UPLOAD )
     * @param UploadedFile $file
     * @param array $data
     * @param array $options
     * @return array
     */
    public function uploadFileCommand(UploadedFile $file, $data = array(), $options = array()) {
        $commands = array();
        try {
            if(!isset($data["folder_id"])) {
                return $this->createResponse(false,"Folder ID is missing");
            }
            if(!isset($data["folder_name"])) {
                return $this->createResponse(false,"Folder name is missing");
            }
            $folderName = str_replace(" ","_",$data["folder_name"])."_".$data["folder_id"];
            $filename = str_replace(" ","_",$file->getClientOriginalName());
            $key = $this->dir . "/" . $folderName . "/" . $filename;

            $commands[] = $this->s3Client->getCommand('putObject',array(
                'Bucket' => $this->bucket,
                'Key'    => $key,
                'SourceFile' => $file,
                'ACL'    => 'public-read'));

            // GET METADATA FROM ORIGINAL FILE
            // THIS IS USED ONLY TO GET METADATA FROM ORIGINAL FILE
            $originalDestination = $this->tmpFolder."/".$filename;
            $r = copy($file,$originalDestination);
            if($r) {
                $metaData = $this->container->get("s_image")->getMetaData($originalDestination);
            }
            // CREATE THUMB
            if(isset($options["resize"])) {
                $key = $this->dir . "/" . $folderName . "/thumb/" . $filename;
                $maxWidth = $options["resize"];

                $thumbFile = $this->resizeUploadedImage($file, $filename, $maxWidth);

                $commands[] = $this->s3Client->getCommand('putObject',array(
                    'Bucket' => $this->bucket,
                    'Key'    => $key,
                    'SourceFile' => $thumbFile,
                    'ACL'    => 'public-read'));

                // SET META DATA FROM ORIGINAL FILE
                if(isset($metaData)) {
                    $this->container->get("s_image")->setMetaData($metaData,$thumbFile);
                }
            }

            return $commands;
        } catch (S3Exception $e) {
//            $error = array("message"=>$e->getMessage());
            throw $e;
        }
    }

    /**
     * Upload file from upload steam
     * @param $data
     * @param $file
     * @param array $options
     * @return bool
     */
    public function uploadFile(UploadedFile $file, $data = array(), $options = array()) {
        try {
            if(!isset($data["folder_id"])) {
                return $this->createResponse(false,"Folder ID is missing");
            }
            if(!isset($data["folder_name"])) {
                return $this->createResponse(false,"Folder name is missing");
            }

            $folderName = $data["folder_name"]."_".$data["folder_id"];
            $filename = $file->getClientOriginalName();
            $key = $this->dir . "/" . $folderName . "/" . $filename;
            // Resize
            if(isset($options["resize"])) {
                $maxWidth = $options["resize"];
                $file = $this->resizeUploadedImage($file, $filename, $maxWidth);
            }

            $result = $this->s3Client->putObject(array(
                'Bucket' => $this->bucket,
                'Key'    => $key,
                'SourceFile' => $file,
                'ACL'    => 'public-read'
            ));
            $result["key"] = $key;

            // Remove tmp file if resized
            if(isset($options["resize"])) {
                unlink($this->tmpFolder."/".$filename);
            }

            return $result;
        } catch (S3Exception $e) {
            $error = array("message"=>$e->getMessage());
//            $this->emailService->send("alen.ankajcan@gmail.com","[PP PHP ERROR] - Error while user is trying upload portfolio",'<pre>' . print_r($error,1) . '</pre>');
        }

        return false;
    }

    /**
     * @param File $file
     * @return bool
     */
    public function deleteFile(File $file) {
        try {
            $result = $this->s3Client->deleteObject(array(
                'Bucket' => $this->bucket,
                'Key'    => $file->getAwsKey()
            ));

            return $result;
        } catch(S3Exception $e) {
            $error = array("message"=>$e->getMessage());
//            $this->emailService->send("alen.ankajcan@gmail.com","[PP PHP ERROR] - Error while user is trying remove portfolio",'<pre>' . print_r($error,1) . '</pre>');
        }
        return false;
    }

    /**
     * @param String $key
     * @return bool
     */
    public function deleteFileByKey($key) {
        try {
            $result = $this->s3Client->deleteObject(array(
                'Bucket' => $this->bucket,
                'Key'    => $key
            ));

            return $result;
        } catch(S3Exception $e) {
            $error = array("message"=>$e->getMessage());
//            $this->emailService->send("alen.ankajcan@gmail.com","[PP PHP ERROR] - Error while user is trying remove portfolio",'<pre>' . print_r($error,1) . '</pre>');
        }
        return false;
    }
    
    /**
     * @param UploadedFile $source
     * @param $filename
     * @param $thumbWidth
     * @return bool|string
     */
    public function resizeUploadedImage(UploadedFile $source, $filename, $thumbWidth) {
        $extension = $source->guessExtension();
        $destination = $this->tmpFolder."/".$filename;

        $size = getimagesize($source);
        $imageWidth  = $newWidth  = $size[0];
        $imageHeight = $newHeight = $size[1];

        if ($imageWidth > $thumbWidth || $imageHeight > $thumbWidth) {
            // Calculate the ratio
            $xscale = ($imageWidth/$thumbWidth);
            $yscale = ($imageHeight/$thumbWidth);
            $newWidth  = ($yscale > $xscale) ? round($imageWidth * (1/$yscale)) : round($imageWidth * (1/$xscale));
            $newHeight = ($yscale > $xscale) ? round($imageHeight * (1/$yscale)) : round($imageHeight * (1/$xscale));
        }

        $newImage = imagecreatetruecolor($newWidth, $newHeight);
        switch ($extension)
        {
            case 'jpeg':
            case 'jpg':
                $imageCreateFrom = 'imagecreatefromjpeg';
                $store = 'imagejpeg';
                break;

            case 'png':
                $imageCreateFrom = 'imagecreatefrompng';
                $store = 'imagepng';
                break;

            case 'gif':
                $imageCreateFrom = 'imagecreatefromgif';
                $store = 'imagegif';
                break;

            default:
                return false;
        }
        $container = $imageCreateFrom($source);
        imagecopyresampled($newImage, $container, 0, 0, 0, 0, $newWidth, $newHeight, $imageWidth, $imageHeight);
        $quality = $extension == "png" ? 9 : 100;
        $store($newImage, $destination, $quality);

        return $destination;
    }

    /**
     * Resize image from local source ( not from upload steam )
     * @param $filePath
     * @param $thumbWidth
     * @return bool|string
     */
    public function resizeUploadedImageFromSrc($filePath, $thumbWidth) {
        $fileData = pathinfo($filePath);
        $extension = $fileData['extension'];
        $filename = $fileData['basename'];
        $destination = $this->tmpFolder."/".$filename;

        $size = getimagesize($filePath);
        $imageWidth  = $newWidth  = $size[0];
        $imageHeight = $newHeight = $size[1];

        if ($imageWidth > $thumbWidth || $imageHeight > $thumbWidth) {
            // Calculate the ratio
            $xscale = ($imageWidth/$thumbWidth);
            $yscale = ($imageHeight/$thumbWidth);
            $newWidth  = ($yscale > $xscale) ? round($imageWidth * (1/$yscale)) : round($imageWidth * (1/$xscale));
            $newHeight = ($yscale > $xscale) ? round($imageHeight * (1/$yscale)) : round($imageHeight * (1/$xscale));
        }

        $newImage = imagecreatetruecolor($newWidth, $newHeight);
        switch ($extension)
        {
            case 'jpeg':
            case 'jpg':
                $imageCreateFrom = 'imagecreatefromjpeg';
                $store = 'imagejpeg';
                break;

            case 'png':
                $imageCreateFrom = 'imagecreatefrompng';
                $store = 'imagepng';
                break;

            case 'gif':
                $imageCreateFrom = 'imagecreatefromgif';
                $store = 'imagegif';
                break;

            default:
                return false;
        }
        $container = $imageCreateFrom($filePath);
        imagecopyresampled($newImage, $container, 0, 0, 0, 0, $newWidth, $newHeight, $imageWidth, $imageHeight);
        $quality = $extension == "png" ? 9 : 100;
        $store($newImage, $destination, $quality);
        return $destination;
    }

    /**
     * Download 1 file
     * @param File $file
     * @return string
     */
    public function downloadFile(File $file) {
        try {
            $destination = $this->tmpFolder."/".$file->getName();

            // Get the object
            $result = $this->s3Client->getObject(array(
                'Bucket' => $this->bucket,
                'Key'    => $file->getAwsKey(),
                'SaveAs' => $destination

            ));
            chmod($destination,0777);
            return $destination;
        } catch (S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    /**
     * Download more files and save to tmp folder
     * @param $files
     * @return array
     */
    public function downloadFiles($files) {
        try {
            $date = new \DateTime();
            $tmpFolderName = "Files_".$date->format('Ymd')."_".time();
            $tmpFolder = $this->tmpFolder."/".$tmpFolderName;
            mkdir($tmpFolder, 0777);
            chmod($tmpFolder,0777);

            $filesUrls = array();
            foreach($files as $file) {
                $destination = $tmpFolder."/".$file->getName();

                // Get the object
                $result = $this->s3Client->getObject(array(
                    'Bucket' => $this->bucket,
                    'Key'    => $file->getAwsKey(),
                    'SaveAs' => $destination
                ));
                chmod($destination,0777);

                $filesUrls[] = $destination;

            }
            return $tmpFolderName;
        } catch (S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    /**
     * Tmp download files for updating metadata
     * @param $files
     * @return array
     */
    public function downloadFilesForMetadata($files) {
        $tmpFolderName = 'files-'.md5(uniqid(rand(), true));
        $base = __DIR__."/../../../../web/uploads/".$tmpFolderName;
        $this->tmpMetaFolder = $base;
        mkdir($this->tmpMetaFolder, 0777);
        chmod($this->tmpMetaFolder,0777);

        foreach($files as $file) {
            $destination = $this->tmpMetaFolder."/".$file->getName();
            try {
                // Get the object
                $result = $this->s3Client->getObject(array(
                    'Bucket' => $this->bucket,
                    'Key'    => $file->getAwsKey(),
                    'SaveAs' => $destination
                ));
                chmod($destination,0777);
                $file->setSource($destination);
            } catch (S3Exception $e) {
                echo $e->getMessage() . "\n";
            }
        }

        return $files;
    }
    
    public function uploadFilesForMetadata($files) {
        
        try {
            foreach ($files as $file) {
                if($file->getSource()) {
                    $result = $this->s3Client->putObject(array(
                        'Bucket' => $this->bucket,
                        'Key' => $file->getAwsKey(),
                        'SourceFile' => $file->getSource(),
                        'ACL' => 'public-read'
                    ));
                    if($result) {
                        $file->setMetadataUpdated(false);
                        $this->em->persist($file);
                    }
                    if(file_exists($file->getSource())) {
                        unlink($file->getSource());
                    }
                }
            }
            // Remove folder
            if(file_exists($this->tmpMetaFolder)) {
                rmdir($this->tmpMetaFolder);
            }
            $this->em->flush();

        } catch (S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

//    /** NOT USED ATM */
//     * Tmp download files for updating metadata
//     * @param $files
//     * @return array
//     */
//    public function downloadFilesForFtp($files) {
//        $base = __DIR__."/../../../../web/uploads";
//        foreach($files as $file) {
//            $destination = $base."/".$file->getName();
//            try {
//                // Get the object
//                $result = $this->s3Client->getObject(array(
//                    'Bucket' => $this->bucket,
//                    'Key'    => $file->getAwsKey(),
//                    'SaveAs' => $destination
//                ));
//                chmod($destination,0777);
//                $file->setSource($destination);
//            } catch (S3Exception $e) {
//                echo $e->getMessage() . "\n";
//            }
//        }
//
//        return $files;
//    }
    
    protected function createResponse($status, $message, $data= null) {
        $response = array();
        $response['status'] = $status;
        $response['message'] = $message;
        if($data) {
            $response['data'] = $data;
        }
        return $response;
    }

    /**
     * @return string
     */
    public function getBucket()
    {
        return $this->bucket;
    }

    /**
     * @param string $bucket
     */
    public function setBucket($bucket)
    {
        $this->bucket = $bucket;
    }

    /**
     * @return string
     */
    public function getUploadInfo()
    {
        return $this->upload_info;
    }

    /**
     * @param string $upload_info
     */
    public function setUploadInfo($upload_info)
    {
        $this->upload_info = $upload_info;
    }

    /**
     * Move files to one folder
     */
    public function moveFile(File $file) {
        
        $newKey = "images/".$file->getName();
        $this->s3Client->copyObject(array(
            'Bucket'     => $this->bucket,
            'Key'        => $newKey,
            'CopySource' => "{$this->bucket}/{$file->getAwsKey()}",
        ));

        // Thumb
        $imageService = $this->container->get("s_image");
        $thumbKey = $imageService->getImageThumbKey($file);
        $newKey = "images/thumb/".$file->getName();

        $this->s3Client->copyObject(array(
            'Bucket'     => $this->bucket,
            'Key'        => $newKey,
            'CopySource' => "{$this->bucket}/{$thumbKey}",
        ));
    }

    


    
    
}