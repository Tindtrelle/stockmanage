<?php
namespace AppBundle\Services\Validation;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;

class ValidationService
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function userRegisterValidate($userData) {
        $errors = array();
        $email = $userData['email'];
        if($email=="" || !$email) {
            $errors [] = "Email field is empty";
        } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors [] = "Email is not valid";
        }
        $password = $userData['password'];
        if(!$password || $password=="") {
            $errors [] = "Password field is empty";
        }
        return $errors;
    }

    public function emailValid($email) {
        $errors = array();
        if($email=="" || !$email) {
            $errors [] = "Email field is empty";
        } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors [] = "Email is not valid";
        }
        return $errors;
    }

    public function imageSizeValid($file) {
        $fileSize = filesize($file);
        if($fileSize > 5000000 || !$fileSize) {
            return false;
        }
        return true;
    }

    public function imageTypeValid($file) {
        $allowedTypes = array("jpeg","jpg","png","gif");
        $type = $file->guessExtension();
        if(!in_array($type,$allowedTypes)) {
            return false;
        }
        return true;
    }

    /**
     *
     * Check if there is emails, phone numbers and external URLs
     * @param $string
     * @return array
     */
    public function contactInfoValid($string) {
        $warnings = array();
        // Validate email address
        if(preg_match('/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i',$string)) {
            $warnings[] = "Email address detected";
        }
        // Validate phone number
        $digitCount = preg_match_all( "/[0-9]/", $string );
        if($digitCount > 7) {
            $warnings[] = "Phone number detected";
        }
        // Validate URL
        if (preg_match('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i',$string)) {
            $warnings[] = "URL detected";
        }

        return $warnings;
    }

    /**
     *
     * Replace if there is emails, phone numbers and external URLs with HIDDEN
     * @param $string
     * @return array
     */
    public function contactInfoHide($string) {
        // Hide email address
        $string = preg_replace('/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i', ' (HIDDEN) ' ,$string);

        // Hide phone number
        $string = preg_replace( '/[0-9][\D]{0,3}[0-9][\D]{0,3}[0-9][\D]{0,3}[0-9][\D]{0,3}[0-9][\D]{0,3}[0-9][\D]{0,3}[0-9]/', ' (HIDDEN) ' , $string);

        // Hide URL
        $string = preg_replace('~(?:\b[a-z\d.-]+://[^<>\s]+|\b(?:(?:(?:[^\s!@#$%^&*()_=+[\]{}\|;:\'",.<>/?]+)\.)+(?:ac|ad|aero|ae|af|ag|ai|al|am|an|ao|aq|arpa|ar|asia|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|biz|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|cat|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|coop|com|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|info|int|in|io|iq|ir|is|it|je|jm|jobs|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mil|mk|ml|mm|mn|mobi|mo|mp|mq|mr|ms|mt|museum|mu|mv|mw|mx|my|mz|name|na|nc|net|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pro|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tel|tf|tg|th|tj|tk|tl|tm|tn|to|tp|travel|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|xn--0zwm56d|xn--11b5bs3a9aj6g|xn--80akhbyknj4f|xn--9t4b11yi5a|xn--deba0ad|xn--g6w251d|xn--hgbk6aj7f53bba|xn--hlcj6aya9esc7a|xn--jxalpdlp|xn--kgbechtv|xn--zckzah|ye|yt|yu|za|zm|zw)|(?:(?:[0-9]|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])\.){3}(?:[0-9]|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5]))(?:[;/][^#?<>\s]*)?(?:\?[^#<>\s]*)?(?:#[^<>\s]*)?(?!\w))~iS', '(HIDDEN)' ,$string);

        return $string;
    }
}