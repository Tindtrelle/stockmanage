<?php
namespace AppBundle\Services\File;
use AppBundle\Entity\Agency;
use AppBundle\Entity\File;
use AppBundle\Entity\FileAgencyStatus;
use AppBundle\Entity\Folder;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContext;

class FileService
{
    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;


    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }
    
    public function getAgencyStatus(File $file, Agency $agency) {
        $fileAgencyStatus = $this->em->getRepository("AppBundle:FileAgencyStatus")->findOneBy(array("file"=>$file,"agency"=>$agency));
        if($fileAgencyStatus) {
            return $fileAgencyStatus->getStatus()->getTitle();
        } else {
            return "Not sent";
        }
    }

    public function getLabelClassByStatus($status) {
        if(strtolower($status) == 'approved') {
            return "status-approved";
        } else if(strtolower($status) == 'rejected') {
            return "status-rejected";
        } else if(strtolower($status) == 'pending') {
            return "status-pending";
        } else {
            return "status-not-sent";
        }
    }

    /**
     * Create not-sent status for every agency
     * @param File $file
     */
    public function setDefaultAgencyStatus(File $file) {
        $agencies = $this->em->getRepository("AppBundle:Agency")->findAll();
        $agencyStatus = $this->em->getRepository("AppBundle:AgencyStatus")->findOneByName("not-sent");
        foreach($agencies as $agency) {
            $fileAgencyStatus = $this->em->getRepository("AppBundle:FileAgencyStatus")->findOneBy(array("file"=>$file,"agency"=>$agency));
            if(!$fileAgencyStatus) {
                $fileAgencyStatus = new FileAgencyStatus();
                $fileAgencyStatus->setFile($file);
                $fileAgencyStatus->setAgency($agency);
                $fileAgencyStatus->setStatus($agencyStatus);
                $this->em->persist($fileAgencyStatus);
            }
        }
        $this->em->flush();
    }
}