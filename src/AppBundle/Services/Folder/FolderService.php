<?php
namespace AppBundle\Services\Folder;
use AppBundle\Entity\Folder;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContext;

class FolderService
{
    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;


    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function getFolderAdditionalData($folder, $data = array())
    {
        if(in_array('folder-users',$data)) {
            $files = $this->em->getRepository('AppBundle:File')->findBy(array("folder" => $folder));
            $users = array();
            foreach($files as $file) {
                if ($file->getUsers()) {
                    foreach ($file->getUsers() as $fileUser) {
                        if($fileUser->getUser()) {
                            if(!$this->checkUserExists($users,$fileUser)) {
                                $users[] = $fileUser;
                            }
                        }
                    }
                }
            }
            $folder->setFileUsers($users);
        }
        return $folder;
    }

    protected function checkUserExists($users,$fileUser) {
        $userExist = false;
        foreach($users as $u) {
            if($fileUser->getUser() && $u->getUser()) {
                if($u->getUser()->getId() == $fileUser->getUser()->getId()) {
                    $userExist = true;
                    break;
                }
            }
        }
        return $userExist;
    }
    
    public function findAllCollections($params) {
        $collections = [];
        $folders = $this->em->getRepository('AppBundle:Folder')->getAllCollections($params);
        foreach($folders as $folder) {
            if(isset($folder["collection"])) {
                if(!$this->checkCollectionExists($collections,$folder["collection"])) {
                    $collections[] = $folder["collection"];
                }
            }
        }
        return $collections;
    }

    protected function checkCollectionExists($collections,$collection) {
        $exist = false;
        foreach($collections as $c) {
            if($c == $collection) {
                $exist = true;
                break;
            }
        }
        return $exist;
    }

    /**
     * @param Folder $folder
     * @return mixed
     * Define folder date from name
     * Pattern:20161023__p20_business ( custom for Lumina Studio )
     *
     */
    public function setFolderDateFromName(Folder $folder) {
        $fileName = $folder->getName();
        if(!isset($fileName)) {
            return 0;
        }
        $year = substr($fileName, 0, 4);
        $month = substr($fileName, 4, 2);
        $day = substr($fileName, 6, 2);
        $datetime = \DateTime::createFromFormat("d/m/Y",$day."/".$month."/".$year);
        if($datetime) {
            $folder->setInternalDateCreated($datetime);
        }

        return $folder;
    }

    public function getTransactions(Folder $folder) {
        $folderId = $folder->getId();
        // Transactions
        $transactions = $this->em->getRepository('AppBundle:TransactionConnection')->findBy(array("folderId" => $folderId));
        if($transactions){
            $expenses = array();
            foreach ($transactions as $transaction){
                $expenses[] = $transaction->getTransaction();
            }
            $folder->setExpenses($expenses);
        }
        return $folder;
    }

}