<?php
namespace AppBundle\Services\Pagination;
use Doctrine\ORM\EntityManager;

class PaginationService
{
    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;

    protected $limit = 100;

    protected $page;

    public function countOffset($page) {
        $offset = (intval($page) - 1) * intval($this->limit);
        return $offset;
    }

    public function __construct(EntityManager $em, $container)
    {
        $this->em = $em;
        $this->em = $container;
        $this->page = 1;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function setCurrentPage($page) {
        if ($page && $page!= "") {
            $this->page = $page;
        }
    }

    public function getCurrentPage() {
        return $this->page;
    }

    public function countTotalPages($totalEntities) {
        return ceil($totalEntities / $this->limit);
    }


}