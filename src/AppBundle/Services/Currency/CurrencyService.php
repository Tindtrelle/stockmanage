<?php
namespace AppBundle\Services\Currency;
use AppBundle\Entity\Currency;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CurrencyService
{

    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;

    private $apiKey = "3932e259b20c61ca6e6b2198d2f6d419";

    private $url = "http://apilayer.net/api/live";

    private $currencies = "USD,CAD,EUR,RSD";

    private $currencyMap = [
        "USD" => "USDUSD",
        "EUR" => "USDEUR",
        "CAD" => "USDCAD",
        "RSD" => "USDRSD",
    ];

    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;

    }

    public function updateRates() {
        try {
            $ch = curl_init();
            $url = $this->url .'?access_key='.$this->apiKey.'&currencies='.$this->currencies.'&format=1';
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = json_decode(curl_exec($ch),true);

            if(isset($output['success'])) {
                if($output['success'] && isset($output['quotes'])) {
                    $rates = $output['quotes'];
                    foreach($rates as $name => $value) {
                        $currency = $this->em->getRepository('AppBundle:Currency')->findOneByName($name);
                        if(!$currency) {
                            $currency = new Currency();
                            $currency->setName($name);
                        }
                        $currency->setValue(floatval($value));
                        $currency->setDateUpdated(new \DateTime());
                        $this->em->persist($currency);
                    }
                    $this->em->flush();
                    return $this->createResponse(true,'Currency rates update success');
                } else {
                    return $this->createResponse(false,"Error with updating currency rates",$output);
                }
            }
            curl_close($ch);
        } catch(\Exception $e) {
            $error = array("message"=>$e->getMessage(),"line"=>$e->getLine(),"file"=>$e->getFile());
            var_dump($error);
//            $this->emailService->send("alen.ankajcan@gmail.com","[PP PHP ERROR] - Error with updating currency rates",'<pre>' . print_r($error,1) . '</pre>');
//            return $this->createResponse(false,"Error with updating currency rates",$error);
        }

    }

    public function convertToUsd($amount,$currencyCode) {
        $currencyName = $this->currencyMap[strtoupper($currencyCode)];
        $currency = $this->em->getRepository('AppBundle:Currency')->findOneByName($currencyName);
        if($currency) {
            return $amount / $currency->getValue();
        }

        return 0;
    }


    protected function createResponse($status, $message, $data = null) {
        $response = array();
        $response['status'] = $status;
        $response['message'] = $message;
        if($data) {
            $response['data'] = $data;
        }
        return $response;
    }
}