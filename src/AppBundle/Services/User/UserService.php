<?php
namespace AppBundle\Services\User;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContext;

class UserService
{
    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;


    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function getUserAdditionalData($user, $data = array()) {
//        $userId = $user->getId();
//        if(in_array('profile-photo',$data)) {
//            $profilePhoto = $this->em->getRepository('ApiBundle:File')->findOneBy(array("entityId" => $userId, "entityName" => 'user',"type" => 'profile'));
//            if($profilePhoto) {
//                if($profilePhoto->getCloud()) {
//                    $user->setPhoto($profilePhoto->getPath());
//                } else {
//                    $user->setPhoto($profilePhoto->getWebPath());
//                }
//                $user->setPhotoId($profilePhoto->getId());
//            }
//        }
//        return $user;
    }

    public function getUser($data = null) {
        $loggedUserEmail = $this->container->get('security.token_storage')->getToken()->getUser();
        $user = $this->em->getRepository('AppBundle:User')->findOneByEmail($loggedUserEmail);
        if(!$user) { return false;}
        if(!$data) {
            return $user;
        }

        return false;
    }

    public function findUserByEmail($email) {
        if(empty($email)) { return false; }
        $user = $this->em->getRepository('AppBundle:User')->findOneByEmail($email);
        if(!$user) {
            return false;
        }
        return $user;
    }


    public function addRole(User $user,$role) {
        $user->addRole($role);
        $this->em->persist($user);
        $this->em->flush();
        return true;
    }

    public function hasRole(User $user,$role) {
        return in_array(strtoupper($role), $user->getRoles(), true);
    }

    public function hasInternalRole(User $user,$role) {
        $roles = $user->getInternalRoles();
        if(!isset($roles)) {
            return false;
        }
        return in_array(strtolower($role), $user->getInternalRoles(), true);
    }

}