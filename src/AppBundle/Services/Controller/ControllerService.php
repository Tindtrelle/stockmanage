<?php
namespace AppBundle\Services\Controller;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControllerService
{
    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;

    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function createResponse($status, $message, $data = null)
    {
        $response = array();
        $response['status'] = $status;
        if(is_array($message)) {
            $response['message'] = implode("; ",$message);
        } else {
            $response['message'] = $message;
        }
        if ($data) {
            $response['data'] = $data;
        }
        return $response;
    }

}