<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateRemoteMetadataCommand extends ContainerAwareCommand {

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('s3:metadata:update')
            ->setDescription('Update files metadata from s3 to local');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $awsService = $this->getContainer()->get('s_aws');
        $awsService->createAwsClient();
        $filesToUpdate = $em->getRepository("AppBundle:File")->findAll();
        $files = [];
        $counter = 0;
        foreach($filesToUpdate as $file) {
            if($counter > 9) { break;}
            if($file->getMetadata()) {
                $title = $file->getMetadata()->getTitle();
                if(!isset($title)) {
                    $files[] = $file; $counter++;
                }
            }
        }
        $files = $awsService->downloadFilesForMetadata($files);
        // UPDATE METADATA
        $filesUpdated = 0;
        foreach ($files as $file) {
            if($file->getSource()) {
                $fileMeta = $file->getMetadata();
                $metadata = $this->getContainer()->get("s_image")->getMetaData($file->getSource());
                if(isset($metadata['title'])) {
                    $fileMeta->setTitle($metadata['title']);
                } else {
                    $fileMeta->setTitle("");
                }
                if(isset($metadata['description'])) {
                    $fileMeta->setDescription($metadata['description']);
                } else {
                    $fileMeta->setDescription("");
                }
                if(isset($metadata['keywords'])) {
                    $fileMeta->setKeywords($metadata['keywords']);
                } else {
                    $fileMeta->setKeywords("");
                }
                $em->persist($fileMeta);
                $filesUpdated++;

                if(file_exists($file->getSource())) {
                    unlink($file->getSource());
                }
            }
        }
        $em->flush();
        $output->writeln($filesUpdated . " files updated");
    }
}