<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PromoteUserCommand extends ContainerAwareCommand {
    /**
     * @see Command
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('user:promote')
            ->setDescription('Promotes a user by adding a role');

        $this
            ->setDefinition(array(
                new InputArgument('email', InputArgument::REQUIRED, 'The email'),
                new InputArgument('role', InputArgument::OPTIONAL, 'The role'),
                new InputOption('super', null, InputOption::VALUE_NONE, 'Instead specifying role, use this to quickly add the super administrator role'),
            ));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userManager = $this->getContainer()->get('s_user');
        $email = $input->getArgument('email');
        $role = $input->getArgument('role');
        $super = (true === $input->getOption('super'));

        $user = $userManager->findUserByEmail($email);

        if (!$user) {
            throw new \RuntimeException('User with that email does not exists.');
        }

        if (null !== $role && $super) {
            throw new \InvalidArgumentException('You can pass either the role or the --super option (but not both simultaneously).');
        }

        if (null === $role && !$super) {
            throw new \RuntimeException('Not enough arguments.');
        }

        if ($super) {
            $role = "ROLE_ADMIN";
        }

        if($userManager->hasRole($user,$role)) {
            throw new \RuntimeException(sprintf('User "%s" already have role "%s".', $email, $role));
        }

        if ($userManager->addRole($user, $role)) {
            $output->writeln(sprintf('User "%s" has been promoted as a "%s".', $email, $role));
        } else {
            throw new \RuntimeException('Error');
        }
    }

}