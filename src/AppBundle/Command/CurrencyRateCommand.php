<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CurrencyRateCommand extends ContainerAwareCommand {

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('currency:update')
            ->setDescription('Update currency rates from Currency Layer API');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $currencyService = $this->getContainer()->get('s_currency');
        $response = $currencyService->updateRates();
        if(isset($response)) {
            $output->writeln($response);
        } else {
            $output->writeln("Error updating currency values");
        }
    }
}