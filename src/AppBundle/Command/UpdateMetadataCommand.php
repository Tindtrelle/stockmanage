<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateMetadataCommand extends ContainerAwareCommand {

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('metadata:update')
            ->setDescription('Update files metadata');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $filesToUpdate = $em->getRepository("AppBundle:File")->findBy(['metadataUpdated' => true]);
        $awsService = $this->getContainer()->get('s_aws');
        $awsService->createAwsClient();
        $files = $awsService->downloadFilesForMetadata($filesToUpdate);
        // UPDATE METADATA
        foreach ($files as $file) {
            if($file->getSource()) {
                $this->getContainer()->get("s_image")->setMetaTitle($file);
                $this->getContainer()->get("s_image")->setMetaDescription($file);
                $this->getContainer()->get("s_image")->setMetaKeyword($file);
            }
        }
        // UPLOAD BACK TO AMAZON
        $awsService->uploadFilesForMetadata($files);

    }
}