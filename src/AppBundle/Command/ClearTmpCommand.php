<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClearTmpCommand extends ContainerAwareCommand {

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('tmp:clear')
            ->setDescription('Remove all files from upload folder');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $files = glob(__DIR__."/../../../web/uploads/*"); // get all file names
        $counter = 0;
        foreach($files as $file){ // iterate files
            if(is_file($file)) {
                unlink($file); // delete file
                $counter++;
            } else if(is_dir($file)) {
                $this->rmdir_recursive($file);
                $counter++;
            }
        }

        if(isset($counter)) {
            $output->writeln($counter ." files removed ");
        } else {
            $output->writeln("No files had been removed");
        }
    }

    protected function rmdir_recursive($dir) {
        foreach(scandir($dir) as $file) {
            if ('.' === $file || '..' === $file) continue;
            if (is_dir("$dir/$file")) $this->rmdir_recursive("$dir/$file");
            else {
                unlink("$dir/$file");
            }
        }
        rmdir($dir);
    }
}