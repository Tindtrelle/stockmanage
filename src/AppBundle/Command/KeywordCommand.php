<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\Keywords;

class KeywordCommand extends ContainerAwareCommand {

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('migrate:keywords')
            ->setDescription('Update files metadata');

        $this
            ->setDefinition(array(
                new InputArgument('limit', InputArgument::REQUIRED, 'The limit'),
                new InputArgument('offset', InputArgument::REQUIRED, 'The offset'),
            ));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $counter = 0;
        $batchSize = 200;

        $keywordRepository = $manager->getRepository('AppBundle:Keywords');

        $limit = $input->getArgument('limit');
        $offset = $input->getArgument('offset');

        $files  = $manager->getRepository('AppBundle:File')->findBy(array(),array(), $limit, $offset);
        // UPDATE METADATA
        foreach ($files as $file){
            $keywords = explode(",", $file->getMetadata()->getKeywords());
            foreach ($keywords as $keyword){
                $counter ++ ;
                $name = trim($keyword);
                if(empty($name)){
                    continue;
                }
                $keyword = $keywordRepository->findOneBy(array('name'=>$name));
                /*if(!isset($keyword)){
                    $keyword = new Keywords($name);
                    $manager->persist($keyword);
                    $manager->flush();
                }*/
                if(!empty($name)){
                    $keyword->addFile($file);
                    $manager->persist($keyword);
                }
                echo $counter . "\n";

            }
            $manager->flush();
            echo "new file ".$file->getId()." \n";
        }

    }
}