####Fixtures:
Run fixtures: php app/console doctrine:fixtures:load --fixtures=src/AppBundle/DataFixtures --append

####Commands:
Promote user as admin:  php app/console user:promote --super [user_email]
Promote user as some role:  php app/console user:promote [user_email] [role]

Send swiftmailer email: php app/console swiftmailer:spool:send
Clear upload folder: php app/console tmp:clear
