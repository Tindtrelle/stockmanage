
$(document).ready(function(){
    /* Load tooltips */
    bindTooltips();

});

/**
 * Not used at the moment
 */
function loadImages() {
    var images = document.images;
    for(var i = 0; i < images.length; i++) {
        if($(images[i]).hasClass("load-async")) {
            var downloading_image = new Image();
            var imgId = $(images[i]).attr("id");
            window[imgId] = $(images[i]);
            console.log(imgId);

            downloading_image.onload = function(){
                console.log(this.src);
                window[$(this).attr("id")].attr("src",this.src)
            };

            downloading_image.src = window[imgId].attr("data-src");
            downloading_image.id = window[imgId].attr("id");
        }
    }
}

function bindTooltips() {
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'top'
    });
}

function getFormResults(form) {
    var formElements = form.elements;
    var formParams = {};
    var elem = null;
    for (var i = 0; i < formElements.length; i += 1) {
        elem = formElements[i];
        switch (elem.type) {
            case 'submit':
                break;
            case 'radio':
                if (elem.checked) {
                    formParams[elem.name] = elem.value;
                }
                break;
            case 'checkbox':
                if (elem.checked) {
                    formParams[elem.name] = elem.value;
                }
                break;
            case 'text':
                if(elem.value != ""){
                    formParams[elem.name] = elem.value;
                }
                break;
            default:
                formParams[elem.name] = elem.value
        }
    }
    return formParams;
}

/* Loading screen */
function startLoading() {
    $("#s-loading").removeClass("hidden");
}

function endLoading() {
    $("#s-loading").addClass("hidden");
    $(".loading-caption").html("");
}

/* SIGN UP */
function submitSignUpForm(event,form) {
    event.preventDefault();
    var url = "/user/signup/ajax";
    var data = getFormResults(form);
    startLoading();
    
    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "json",
        method: "POST"
    }).done(function(response) {
        endLoading();
        if(response.status) { // LOGIN & REDIRECT
            if(response.data.redirect) {
                window.location.href = window.location.origin + "/"+response.data.redirect;
            } else {
                location.reload();
            }
        } else { // ERRORS
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

/* LOGIN */
function submitLoginForm(event,form) {
    event.preventDefault();
    var url = "/user/login/ajax";
    var data = getFormResults(form);
    startLoading();

    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "json",
        method: "POST"
    }).done(function(response) {
        endLoading();
        if(response.status) { // LOGIN & REDIRECT
            if(response.data.redirect) {
                window.location.href = window.location.origin + "/"+response.data.redirect;
            } else {
                location.reload();
            }
        } else { // ERRORS
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

/* Warning/error modal */
function openWarningModal(data) {
    var title;
    var message;

    if(typeof(data) == 'undefined') {
        title = "Something went wrong";
        message = "Something happen. Please try again.";
    } else {
        if(data.status) {
            title = "Successful!";
        } else {
            title = "Something went wrong";
        }

        if(data.title) {
            title = data.title;
        }

        if(!data.message) {
            message = "Something happen. Please try again.";
        } else {
            message = data.message;
        }
    }

    $("#warning-modal #warning-message").html(message);
    $("#warning-modal #warning-title").html(title);
    $("#warning-modal").modal('show');
}

/* FOLDERS LIST */
$('ul.dropdown-menu').on('click', function(event){
    event.stopPropagation();
});

function openNewFolderModal() {
    // Reset values
    $("#create-folder-modal .modal-title").html("Create Folder");
    $(".input-f-id").val("");
    $(".input-f-name").val("");
    $(".input-f-collection").val("");
    $(".input-f-description").val("");

    $("#create-folder-modal").modal('show');
}

function openEditFolderModal(id) {
    var parent_row = "tr[data-folder-id='"+id+"']";
    var f_name = $(parent_row+" .folder-name").text();
    var f_description = $(parent_row+" .folder-description").text();
    var f_collection = $(parent_row+" .folder-collection").text();
    // Set values
    $("#create-folder-modal .modal-title").html("Update Folder")
    $(".input-f-id").val(id);
    $(".input-f-name").val(f_name);
    $(".input-f-collection").val(f_collection);
    $(".input-f-description").val(f_description);
    $("#create-folder-modal").modal('show');
}

function submitCreateNewFolder(event,form) {
    event.preventDefault();
    var url = "/folders/ajax";
    var data = getFormResults(form);
    data.action = "folder-create";
    startLoading();

    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "json",
        method: "POST"
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

function searchFolders(elem) {
    var url = "/folders";
    var data = {"name": $(elem).val()};

    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('.folder-list').html(response);
    });
}
var selected_all_folders = false;
function selectAllFolders() {
    if(selected_all_folders) {
        $("input.folder-select").prop('checked', false);
        selected_all_folders = false;
    } else {
        $("input.folder-select").prop('checked', true);
        selected_all_folders = true;
    }
}

/**
 * Delete selected folders
 * NOTE: Only empty folders will be deleted
 */
function deleteFolders() {
    var folders_to_delete = [];
    $("input.folder-select").each(function(index) {
        if($(this).is(':checked')) {
            folders_to_delete.push($(this).attr("data-folder-id"));
        }
    });
    if(folders_to_delete.length == 0) {
        return;
    }
    
    var url = "/folders/ajax";
    var form_data = new FormData();
    form_data.append("folders",folders_to_delete);
    form_data.append("action","folders-delete");
    startLoading();
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

function downloadFolderFiles() {
    var folders_to_download = [];
    $("input.folder-select").each(function(index) {
        if($(this).is(':checked')) {
            folders_to_download.push($(this).attr("data-folder-id"));
        }
    });
    // ALLOW ONLY ONE FOLDER DOWNLOAD
    if(folders_to_download.length != 1) {
        return;
    }

    window.open(window.location.origin+'/folder/download?id='+folders_to_download, '_blank');
}

/**
 * FOLDER VIEW START
 */
function openUploadFilesDialog() {
    $("#btn-files-upload").trigger("click");
}

function submitUploadFilesForm(event) {
    fileList = event.target.files;
    $("#frm-files-upload").submit();
}


/**
 * Upload files
 * @param event
 * @param form
 */
function startUploadFiles(event,form)
{
    event.stopPropagation();
    event.preventDefault();
    var url = "/folders/ajax";
    var form_data = new FormData();
    $.each(fileList, function(key, value)
    {
        form_data.append("file_"+key, value);
    });
    form_data.append("files_number",fileList.length);
    form_data.append("action","files-upload");
    var dataFields = getFormResults(form);
    form_data.append("folder_id",dataFields['folder_id']);
    form_data.append("folder_name",dataFields['folder_name']);
    var upload_progress_key = "upload_"+dataFields['user_id']+"_"+Math.floor(Date.now() / 1000);
    form_data.append("upload_progress_key",upload_progress_key);
    
    var uploadInterval = setInterval(function(){ uploadProgress(upload_progress_key); }, 1000);
    startLoading();
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            endLoading();
            clearInterval(uploadInterval);
            var response = JSON.parse(xhr.response);
            console.log(response);
            if(response.status) {
                location.reload();
            } else { // ERRORS
                openWarningModal({message:response.message, status: 0 });
            }
        }
    };
    xhr.upload.addEventListener("progress", function(evt){
        console.log(evt);
        if (evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            var total = ((percentComplete * 100)/2).toFixed(2);
            //Do something with upload progress
            $(".loading-caption").html(total+"% uploading...");

            console.log(percentComplete);
        }
    }, false);
    xhr.open("post", url);
    xhr.send(form_data);
}

/**
 * Get uploading process 
 * @param upload_progress_key
 */
function uploadProgress(upload_progress_key) {
    var url = "/folders/ajax";
    var form_data = new FormData();
    form_data.append("action","upload-progress");
    form_data.append("upload_progress_key",upload_progress_key);
    var up_xhr = new XMLHttpRequest();
    up_xhr.onreadystatechange = function(){
        if(up_xhr.readyState == 4){
            var response = JSON.parse(up_xhr.response);
            if(response.status) {
                $(".loading-caption").html("Uploading...");
                if(response.data) {
                    $(".loading-caption").html(response.data+"% uploading to Amazon...");
                }
            }
        }
    };
    up_xhr.open("post", url);
    up_xhr.send(form_data);
}

/**
 * Select / deselect files
 * @type {boolean}
 */
var selected_all = false;
function selectAllFiles() {
    if(selected_all) {
        $(".img-container").removeClass("selected");
        selected_all = false;
    } else {
        $(".img-container").addClass("selected");
        selected_all = true;
    }
    showInfoPopup();
}
/**
 * Select single files
 * @param elem
 */
function selectSingleFile(elem) {
    if($(elem).hasClass("selected")) {
        $(elem).removeClass("selected")
    } else {
        $(elem).addClass("selected")
    }
    showInfoPopup();
}
/**
 * Show top right info popup
 */
function showInfoPopup() {
    var info_popup = $(".page-notifications");
    var total_files = 0;
    var selected_files = 0;
    $(".img-container").each(function(index) {
        total_files++;
        if($(this).hasClass("selected")) {
            selected_files++;
        }
    });

    if(selected_files) {
        info_popup.removeClass("hidden");
        $("p.notification-content").html(selected_files+"/"+total_files+" images selected");
    } else {
        info_popup.addClass("hidden");
    }
}
/**
 * Download Files
 */
function downloadFiles() {
    var files_to_download = [];
    $(".img-container").each(function(index) {
       if($(this).hasClass("selected")) {
           files_to_download.push($(this).attr("data-file-id"));
       }
    });

    if(files_to_download.length == 0) {
        return;
    }

    window.open(window.location.origin+'/file/download?id='+files_to_download, '_blank');
}
/**
 * Delete files
 */
function deleteFiles() {
    var files_to_delete = [];
    $(".img-container").each(function(index) {
        if($(this).hasClass("selected")) {
            files_to_delete.push($(this).attr("data-file-id"));
        }
    });
    if(files_to_delete.length == 0) {
        return;
    }

    var url = "/folders/ajax";
    var form_data = new FormData();
    form_data.append("files",files_to_delete);
    form_data.append("action","files-delete");
    startLoading();
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}


/**
 * Search files
 */
var fileTypingTimer;
var $input = $('#input-file-search');
$input.on('keyup', function () {
    clearTimeout(fileTypingTimer);
    fileTypingTimer = setTimeout(
        searchFilesRequest,500);
});
$input.on('keydown', function () {
    clearTimeout(fileTypingTimer);
});

function searchFilesRequest() {
    var url = "/folders/view/"+$("#folder-id").val();
    var data = {"name": $("#input-file-search").val()};

    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "html",
        method: "GET"
    }).done(function(response) {
        $('.files-list').html(response);
    });
}

/**
 * FOLDER VIEW END
 */

/**
 * TEAM MANAGEMENT
 */

function openCreateMemberModal() {
    // Reset values
    $("#create-folder-modal .modal-title").html("Create Folder");
    $(".input-m-id").val("");
    $(".input-m-name").val("");
    $(".input-m-role").val("");
    $(".input-m-custom-id").val("");

    $("#update-member-modal").modal('show');
}

function openEditMemberModal(id) {
    var parent_row = "tr[data-member-id='"+id+"']";
    var m_name = $(parent_row+" .member-name").text();
    var m_custom_id = $(parent_row+" .member-custom-id").text();
    var m_role = $(parent_row+" .member-role").text();
    // Set values
    $("#update-member-modal .modal-title").html("Update Member");
    $(".input-m-id").val(id);
    $(".input-m-name").val(m_name);
    $(".input-m-role").val(m_role);
    $(".input-m-custom-id").val(m_custom_id);
    $("#update-member-modal").modal('show');
}

function submitUpdateMember(event,form) {
    event.preventDefault();
    var url = "/team/ajax";
    var data = getFormResults(form);
    data.action = "member-create";
    startLoading();

    $.ajax({
        url: window.location.origin+url,
        data: data,
        dataType: "json",
        method: "POST"
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            openWarningModal({message:response.message, status: 0 });
        }
    });
}

var selected_all_member = false;
function selectAllMembers() {
    if(selected_all_member) {
        $("input.member-select").prop('checked', false);
        selected_all_member = false;
    } else {
        $("input.member-select").prop('checked', true);
        selected_all_member = true;
    }
}

/**
 * Delete selected members
 *
 */
function deleteMembers() {
    var members_to_delete = [];
    $("input.member-select").each(function(index) {
        if($(this).is(':checked')) {
            members_to_delete.push($(this).attr("data-member-id"));
        }
    });
    if(members_to_delete.length == 0) {
        return;
    }

    var url = "/team/ajax";
    var form_data = new FormData();
    form_data.append("members",members_to_delete);
    form_data.append("action","members-delete");
    startLoading();
    $.ajax({
        url: window.location.origin+url,
        data: form_data,
        cache: false,
        dataType: 'json',
        method: 'POST',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(response) {
        endLoading();
        if(response.status) {
            location.reload();
        } else { // ERRORS
            console.log(response);
            openWarningModal({message:response.message, status: 0 });
        }
    });
}