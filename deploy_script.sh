#!/bin/bash

# -------------------
# Setup script
#
# -------------------

if [ "$1" == "prod" ]; then
    echo "Prepping for prod:"
    # make sure it's deleteable
    chmod -R 777 app/cache app/logs

    if [ ! -f "composer.phar" ]; then
        # get latest composer.phar
        curl -s http://getcomposer.org/installer | php
    fi

    # Install dependencies
    echo "Running composer install..."
    php composer.phar install --no-dev --optimize-autoloader

    echo "Clearing cache..."
    php app/console cache:clear --env=prod

    echo "Installing assets..."
    php app/console assets:install --env=prod
    php app/console assetic:dump --env=prod

    echo "Setting roles on cache..."
    #chown -R www-data:www-data *
    chmod -R 777 app/cache app/logs

else
    echo "Refreshing for dev. please wait..."
    #php app/console cache:clear
    php app/console assets:install web
    php app/console assetic:dump web
fi

